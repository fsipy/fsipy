"""
    FsiPy main file
    ===============
"""

import sys
import os
from   mpi4py          import MPI
import PyFluid         as PyFluid
import PySolid         as PySolid
import PySolidImplicit as PySolidImplicit
import PySolidRigid    as PySolidRigid
import PyCouplings     as PyCouplings
import parser          as parser
import aux
import numpy           as np


@aux.timeit
def run_simulation(filename):
    """ Reads inputs, builds problema and runs simulation.

        Inputs
        ------
            filename: str
                filename to config file (ex: "<PATH_TO_DIRECTORY>/channelflow")
        Outputs
        -------
            problem: class(Problem)
                Problem object of class defined in corresponding physics module.
    """

    # Gets my MPI rank id
    rank = MPI.COMM_WORLD.Get_rank()    
    
    # Reads inputs 
    d_params, d_mesh, d_bconds, d_parallel = parser.read_inputs(filename,rank)

    # Builds problem    
    problem = build_problem(d_params, d_mesh, d_bconds, d_parallel, rank)
    
    # Runs simulation    
    problem.run_simulation()
    
    # Take times only for the first subdomain (In theory, it has to be more o less the same for the rest) 
    if MPI.COMM_WORLD.Get_rank() == 0:
        determine_times(d_params, problem)
    
    return problem


def build_problem(d_params, d_mesh, d_bconds, d_parallel, rank):
    """ Builds fluid, solid or FSI problem.

        Inputs
        ------
            d_params: dict
                contains problem parameters.
            d_mesh: dict
                contains mesh data.
            d_parallel: dict
		constains nodal exchange arrays.
            rank: dict
                my MPI rank id.
        Outputs
        -------
            problem: class(Problem)
                Problem object of class defined in corresponding physics module.
    """

    # Builds fluid problem
    if d_params['physics'] == 'fluid':
        return PyFluid.Fluid(d_params, d_mesh, d_bconds, d_parallel, rank)
    
    # Builds solid problem
    elif d_params['physics'] == 'solid':
        if   d_params['time_treatment'] == 'explicit':
            return PySolid.Solid(d_params, d_mesh, d_bconds, d_parallel, rank)
        elif d_params['time_treatment'] == 'implicit':
            return PySolidImplicit.SolidImplicit(d_params, d_mesh, d_bconds, d_parallel, rank)

    elif d_params['physics'] == 'rigidbody':
        return PySolidRigid.SolidRigid(d_params, d_mesh, d_bconds, d_parallel, rank)

    # Builds FSI problem
    elif d_params['physics'] == 'coupled':
        return build_coupled_problem(d_params, rank)


def build_coupled_problem(d_params, rank):
    """ Builds coupled problem.

        Inputs
        ------
            d_params: dict
                contains problem parameters.
            rank: dict
                my MPI rank id.
        Outputs
        -------
            problem: class(Problem)
                Problem object of class defined in corresponding physics module.
    """

    # Loop over problems
    l_oproblems = []
    l_problem_names = []
    for iproblem in range(d_params['nproblems']):
        # Get problem filename
        filename = os.path.join(d_params['path_inputs'],
                                d_params['l_problems'][iproblem])

        # Read parameters of single problem
        d_params_one, d_mesh_one, d_bconds, d_parallel = parser.read_inputs(filename, rank)

        # Build single problem
        problem = build_problem(d_params_one, d_mesh_one, d_bconds, d_parallel, rank)

        # Append to list of problems
        l_oproblems.append(problem)
        l_problem_names.append(d_params_one['casename'])

    # Build coupled problem
    coupling_method = d_params['coupling_method']
    return eval('PyCouplings.'+coupling_method+'(d_params, l_oproblems, l_problem_names, rank)')


def determine_times(d_params, problem):

    # Different methods form different classes
    l_data         = []
    l_data_indices = []

    # Times for the different methods
    l_times = np.array(problem.l_times)
    
    # If is a coupled problem, we have to take into account all the problems involved
    # l_data:        contains the different types of methods and their respectives clases for measure the time
    # l_information: contains timings for the different types of methods and their respectives clases (see PyTiming)
    # Example of a timing method and their classes: Equation/velocity_prediction/assemble_algebraic_system
    if d_params['physics'] == 'coupled':
        coupling_method = d_params['coupling_method']        
        l_data.append(['Coupling', coupling_method, 'advance_timestep'])

        for iproblem in range(d_params['nproblems']):
            oproblem = problem._l_oproblems[iproblem]            
            l_equation_names = oproblem._l_equation_names
        
            l_data.append(['Problem', oproblem.__class__.__name__, 'advance_timestep'])
        
            for equation_name in l_equation_names:
                l_data.append(['Equation', equation_name, 'assemble_algebraic_system'])
                l_data.append(['Equation', equation_name, 'solve'])

        # Discard first time step, first advance_timestep will be apperad at the end of the advance_timestep
        first_index = problem.l_information.index(['Coupling', coupling_method, 'advance_timestep'])
                
    else:
        l_data.append(['Problem', problem.__class__.__name__, 'advance_timestep'])

        
        l_equation_names = problem._l_equation_names
        for equation_name in l_equation_names:
            l_data.append(['Equation', equation_name, 'assemble_algebraic_system'])
            l_data.append(['Equation', equation_name, 'solve'])        

        # Discard first time step, first advance_timestep will be apperad at the end of the advance_timestep
        first_index = problem.l_information.index(['Problem', problem.__class__.__name__, 'advance_timestep'])

    # Sum the times for each different method at each time step (excluding the first one)
    # l_information contains timings for the different types of methods and their respectives clases (see PyTiming)    
    for idata in range(len(l_data)):
        l_data_indices.append([])
        for jinformation in range(first_index+1,len(problem.l_information)):                        
            if problem.l_information[jinformation] == l_data[idata]:
                l_data_indices[idata].append(jinformation)

    # Print execution times
    print("\n-----------------------------------------------------------------------------------------------------------")
    print("EXECUTION TIME AVERAGES IN A TIME STEP (EXCLUDING THE FIRST ONE)")    
    print("-----------------------------------------------------------------------------------------------------------")
    print("{:<25} {:<30} {:<30} {:<8} {:<8}".format('Descripción  general', 'Descirpción particular', 'método', 'tiempo','porcentaje'))
    print("-----------------------------------------------------------------------------------------------------------")
    indices    = l_data_indices[0]
    tiempo_max = sum(l_times[indices])/len(indices)
    tiempo_max = round(tiempo_max * 1000)/1000

    for idata in range(len(l_data)):
        indices = l_data_indices[idata]

        tiempo    = sum(l_times[indices])/len(indices)
        tiempo    = round(tiempo * 1000)/1000

        porcentaje =  round(tiempo/tiempo_max * 1000)/10
                
        if l_data[idata][0] == 'Problem':         
            print("{:<25} {:<30} {:<30} {:<8} {:<8}".format('','','','',''))
            
        print("{:<25} {:<30} {:<30} {:<8} {:<8}".format(l_data[idata][0],l_data[idata][1],l_data[idata][2], str(tiempo),porcentaje ))
        if l_data[idata][0] == 'Problem':         
            print("{:<25} {:<30} {:<30} {:<8} {:<8}".format('','','','',''))
        
    print("-----------------------------------------------------------------------------------------------------------")



if __name__ == "__main__":
    """ Executes run_simulation only if Main.py run as script. """
    parser.print_header(MPI.COMM_WORLD.Get_rank())
    run_simulation(sys.argv[1])
    print(' ')



    
    
