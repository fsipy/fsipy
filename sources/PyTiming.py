import time
from  mpi4py import MPI

class Timing(object):

    l_information = []
    l_times       = []

    time_step      = 1
    def __init__(self):
        pass
    
    def __call__(self, original_method):
        def new_method(*args, **kwargs):                       
            if MPI.COMM_WORLD.Get_rank() == 0:
                object_name = args[0].__class__.__name__
                method_name = original_method.__name__                
                father_name = args[0].__class__.__bases__[0].__name__
                
                if object_name == "Equation":
                    father_name = "Equation"                               
                    object_name = args[0]._name

                initial_time = time.time()            
                result       = original_method(*args, **kwargs)
                final_time   = time.time()
               
                total_time = (final_time - initial_time)
                total_time = round(total_time * 10000)/10000
    
                self.l_information.append([father_name,object_name,method_name]) 
                self.l_times.append(total_time)    
            else:
                result = original_method(*args, **kwargs)

            return result        
        return new_method
