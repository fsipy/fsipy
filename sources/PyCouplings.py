"""
    FsiPy Coupling Module
    =====================

! ---------------------------------------------------------------------------- !
    call UPDATE_TIME()
    call BEGIN_TIMESTEP()
    do ITERATIVE_LOOP:
        BEGIN_ITERATION()
        do PROBLEM_LOOP:
            do COUPLING_LOOP:
                BEGIN_PROBLEM( ICOUPLING )
            PROBLEM.ADVANCE_TIMESTEP()
            do COUPLING_LOOP:
                BEGIN_PROBLEM( ICOUPLING )
        END_ITERATION()
    END_TIMESTEP()
    POST_PROCESS()
! ---------------------------------------------------------------------------- !

"""
import time         as time
import numpy        as np
import numpy.linalg as la
import PyKernel     as PyKernel
import scipy.sparse as ssp
from   mpi4py       import MPI
from   PyParameters  import *



class IB(PyKernel.Coupling):
    """ Immersed Finite Element Method """

    def __init__(self, d_params, l_oproblems, l_problem_names, rank):

        # Initialize Coupling object
        PyKernel.Coupling.__init__(self,
                                   d_params,
                                   l_oproblems,
                                   l_problem_names,
                                   rank)

        # Define list of couplings
        self._l_problem_codes = ['fluid0', 'solid0']
        self._l_dcouplings = [
                {'icoupling': 0,
                 'source'   : 'fluid0',
                 'target'   : 'solid0',
                 'where'    : 'whole_mesh',
                 'action'   : 'interpolate',
                 'type'     : 'dirichlet',
                 'variable' : 'velocity',
                 'equation' : l_oproblems[1]._l_equation_names[0],
                 'send'     : ('end_timestep','after'),
                 'recv'     : ('begin_timestep','before')},
                {'icoupling': 1,
                 'source'   : 'solid0',
                 'target'   : 'fluid0',
                 'where'    : [],
                 'action'   : 'spread',
                 'type'     : 'body-force',
                 'variable' : 'force',
                 'equation' : 'fractional-velocity',
                 'send'     : ('end_timestep','after'),
                 'recv'     : ('begin_timestep','before')}
                ];

        # Activates the advection of solid Lagrangian nodes
        self.get_problem_by_code('solid0').activate_solid_advection()

        # Save background and patch problem names
        self._background = self._l_problem_names[0]
        self._patch      = self._l_problem_names[1]

        # Cancel solid forces computed on Dirichlet nodes
        otarget = self.get_problem( self._patch )
        otarget._cancel_forces_dirichlet = True

    def begin_timestep(self):
        if self._ipass == 0:
            # Compute interpolation and spread matrices
            self.calculate_interp_matrix(self._background, self._patch)
            self.calculate_spread_matrix(self._background, self._patch)
            self._ipass = 1
        pass

    def begin_iteration(self):
        pass

    def begin_problem(self, icoupling):
        pass

    def end_problem(self, icoupling):
        # Load problems
        otarget = self.get_problem_from_coupling(icoupling, 'target') 
        osource = self.get_problem_from_coupling(icoupling, 'source')
        # Load send variable
        variable_send = osource._d_coupling_variables[ (icoupling, 'send') ]
        if   (icoupling == 0):
            # Interpolate velocities from fluid to solid
            variable_recv = self.interpolate(self._ndime,
                                             source_name   = self._background,
                                             target_name   = self._patch,
                                             values_source = variable_send)
        elif (icoupling == 1):
            # Update interpolation and spread matrices
            self.calculate_interp_matrix(self._background, self._patch)
            self.calculate_spread_matrix(self._background, self._patch)
            # Spread forces from solid to fluid 
            variable_recv = self.spread(self._ndime,
                                        source_name   = self._patch,
                                        target_name   = self._background,
                                        equation_name = self._l_dcouplings[0]['equation'],
                                        values_source = variable_send)
        # Save receive variable to target
        otarget._d_coupling_variables[ (icoupling, 'recv') ] = variable_recv

    def end_iteration(self):
        pass



class MIFEM(PyKernel.Coupling):
    """ Modified Immersed Finite Element Method """

    def __init__(self, d_params, l_oproblems, l_problem_names, rank):

        # Initialize Coupling object
        PyKernel.Coupling.__init__(self,
                                   d_params,
                                   l_oproblems,
                                   l_problem_names,
                                   rank)

        # Define list of couplings
        self._l_problem_codes = ['fluid0', 'solid0']
        self._l_dcouplings = [
                {'icoupling': 0,
                 'source'   : 'fluid0',
                 'target'   : 'solid0',
                 'where'    : ('code',0),
                 'action'   : 'interpolate',
                 'type'     : 'dirichlet',
                 'variable' : 'velocity',
                 'equation' : l_oproblems[1]._l_equation_names[0],
                 'send'     : ('end_timestep','after'),
                 'recv'     : ('begin_timestep','before')},
                {'icoupling': 1,
                 'source'   : 'solid0',
                 'target'   : 'fluid0',
                 'where'    : [],
                 'action'   : 'spread',
                 'type'     : 'body-force',
                 'variable' : 'force',
                 'equation' : 'fractional-velocity',
                 'send'     : ('end_timestep','after'),
                 'recv'     : ('begin_timestep','before')}
                ];

        # Save background and patch problem names
        self._background = self._l_problem_names[0]
        self._patch      = self._l_problem_names[1]

        # Cancel solid forces computed on Dirichlet nodes
        otarget = self.get_problem( self._patch )
        otarget._cancel_forces_dirichlet = True

    def begin_timestep(self):
        if self._ipass == 0:
            # Compute interpolation and spread matrices
            self.calculate_interp_matrix(self._background, self._patch)
            self.calculate_spread_matrix(self._background, self._patch)
            self._ipass = 1
        pass

    def begin_iteration(self):
        pass

    def begin_problem(self, icoupling):
        pass

    def end_problem(self, icoupling):

        # Load problems
        otarget = self.get_problem_from_coupling(icoupling, 'target') 
        osource = self.get_problem_from_coupling(icoupling, 'source')
        # Load send variable
        variable_send = osource._d_coupling_variables[ (icoupling, 'send') ]
        if   (icoupling == 0):
            # Interpolate velocities from fluid to solid
            variable_recv = self.interpolate(self._ndime,
                                             source_name   = self._background,
                                             target_name   = self._patch,
                                             values_source = variable_send)
        elif (icoupling == 1):
            # Update interpolation and spread matrices
            self.calculate_interp_matrix(self._background, self._patch)
            self.calculate_spread_matrix(self._background, self._patch)
            # Spread forces from solid to fluid 
            variable_recv = self.spread(self._ndime,
                                        source_name   = self._patch,
                                        target_name   = self._background,
                                        equation_name = self._l_dcouplings[0]['equation'],
                                        values_source = variable_send)
            # Compute level set for overlapping region
            self.calculate_level_set(self._background, self._patch)
            # Subtract fluid stresses from FSI force
            variable_recv -= otarget._force * np.tile(otarget._level_set,(self._ndime,1))
        # Save receive variable to target
        otarget._d_coupling_variables[ (icoupling, 'recv') ] = variable_recv

    def end_iteration(self):
        pass



class EMBEDDED(PyKernel.Coupling):
    """ Immersed Finite Element Method """

    def __init__(self, d_params, l_oproblems, l_problem_names, rank):

        # Initialize Coupling object
        PyKernel.Coupling.__init__(self,
                                   d_params,
                                   l_oproblems,
                                   l_problem_names,
                                   rank)

        # Define list of couplings
        self._l_problem_codes = ['fluid0','solid0']
        self._l_dcouplings = [
                {'icoupling': 0,
                 'source'   : 'solid0',
                 'target'   : 'fluid0',
                 'where'    : [],
                 'action'   : 'interpolate',
                 'type'     : 'dirichlet',
                 'variable' : 'velocity',
                 'equation' : 'fractional-velocity',
                 'send'     : ('end_timestep','after'),
                 'recv'     : ('begin_timestep','before')}
                ]

        # Activates the advection of solid Lagrangian nodes
        self.get_problem_by_code('solid0').activate_solid_advection()

        # Save background and patch problem names
        self._background = self._l_problem_names[0]
        self._patch      = self._l_problem_names[1]

        # Cancel solid forces computed on Dirichlet nodes
        # otarget = self.get_problem( self._patch )
        # otarget._cancel_forces_dirichlet = True

    def begin_timestep(self):
        if self._ipass == 0:
            # Compute interpolation and spread matrices
            self.calculate_interp_matrix(self._patch, self._background)
            self._ipass = 1
        pass

    def begin_iteration(self):
        pass

    def begin_problem(self, icoupling):
        pass

    def end_problem(self, icoupling):
        # Load problems
        otarget = self.get_problem_from_coupling(icoupling, 'target') 
        osource = self.get_problem_from_coupling(icoupling, 'source')
        # Load send variable
        variable_send = osource._d_coupling_variables[ (icoupling, 'send') ]
        if   (icoupling == 0):
            # Interpolate velocities from fluid to solid
            variable_recv = self.interpolate(self._ndime,
                                             source_name   = self._patch,
                                             target_name   = self._background,
                                             values_source = variable_send)
        # Save receive variable to target
        otarget._d_coupling_variables[ (icoupling, 'recv') ] = variable_recv

    def end_iteration(self):
        pass



class NIFEM(PyKernel.Coupling):
    """ Neumann Immersed Finite Element Method: Interpolates tractions
        from fluid to solid boundary and spreads solid forces to fluid. """

    def __init__(self, d_params, l_oproblems, l_problem_names, rank):

        # Initialize Coupling object
        PyKernel.Coupling.__init__(self,
                                   d_params,
                                   l_oproblems,
                                   l_problem_names,
                                   rank)

        # Define list of couplings
        self._l_problem_codes = ['fluid0', 'solid0']
        self._l_dcouplings = [
                {'icoupling': 0,
                 'source'   : 'fluid0',
                 'target'   : 'solid0',
                 'where'    : ('code',0),
                 'action'   : 'interpolate',
                 'type'     : 'neumann',
                 'variable' : 'force',
                 'equation' : [],
                 'send'     : ('end_timestep','after'),
                 'recv'     : ('begin_timestep','before')},
                {'icoupling': 1,
                 'source'   : 'solid0',
                 'target'   : 'fluid0',
                 'where'    : [],
                 'action'   : 'spread',
                 'type'     : 'body-force',
                 'variable' : 'force',
                 'equation' : 'fractional-velocity',
                 'send'     : ('end_timestep','after'),
                 'recv'     : ('begin_timestep','before')}
                ];

        # Save background and patch problem names
        self._background = self._l_problem_names[0]
        self._patch      = self._l_problem_names[1]

        # Cancel solid forces computed on Dirichlet nodes
        otarget = self.get_problem( self._patch )
        otarget._cancel_forces_dirichlet = True

    def begin_timestep(self):
        if self._ipass == 0:
            # Compute interpolation and spread matrices
            self.calculate_interp_matrix(self._background, self._patch)
            self.calculate_spread_matrix(self._background, self._patch)
            self._ipass = 1
        pass

    def begin_iteration(self):
        pass

    def begin_problem(self, icoupling):
        pass

    def end_problem(self, icoupling):

        # Load problems
        otarget = self.get_problem_from_coupling(icoupling, 'target') 
        osource = self.get_problem_from_coupling(icoupling, 'source')
        # Load send variable
        variable_send = osource._d_coupling_variables[ (icoupling, 'send') ]
        if   (icoupling == 0):
            # Interpolate velocities from fluid to solid
            variable_recv = self.interpolate(self._ndime,
                                             source_name   = self._background,
                                             target_name   = self._patch,
                                             values_source = variable_send)
        elif (icoupling == 1):
            # Update interpolation and spread matrices
            self.calculate_interp_matrix(self._background, self._patch)
            self.calculate_spread_matrix(self._background, self._patch)
            # Spread forces from solid to fluid 
            variable_recv = self.spread(self._ndime,
                                        source_name   = self._patch,
                                        target_name   = self._background,
                                        values_source = variable_send)
            # Compute level set for overlapping region
            self.calculate_level_set(self._background, self._patch)
            # Subtract fluid stresses from FSI force
            variable_recv -= otarget._force * np.tile(otarget._level_set,(self._ndime,1))
        # Save receive variable to target
        otarget._d_coupling_variables[ (icoupling, 'recv') ] = variable_recv

    def end_iteration(self):
        pass



class EMPTY(PyKernel.Coupling):
    """ Empty coupling to test interpolation schemes. """

    def __init__(self, d_params, l_oproblems, l_problem_names, rank):

        # Initialize Coupling object
        PyKernel.Coupling.__init__(self,
                                   d_params,
                                   l_oproblems,
                                   l_problem_names,
                                   rank)

        # Define list of couplings
        self._l_problem_codes = ['fluid0', 'solid0']
        self._l_dcouplings = [
                {'icoupling': 0,
                 'source'   : 'fluid0',
                 'target'   : 'solid0',
                 'where'    : 'whole_mesh',
                 'action'   : 'interpolate',
                 'type'     : 'dirichlet',
                 'variable' : 'velocity',
                 'equation' : l_oproblems[1]._l_equation_names[0],
                 'send'     : ('end_timestep','after'),
                 'recv'     : ('begin_timestep','before')},
                {'icoupling': 1,
                 'source'   : 'solid0',
                 'target'   : 'fluid0',
                 'where'    : [],
                 'action'   : 'spread',
                 'type'     : 'body-force',
                 'variable' : 'force',
                 'equation' : 'fractional-velocity',
                 'send'     : ('end_timestep','after'),
                 'recv'     : ('begin_timestep','before')}
                ];

        # Save background and patch problem names
        self._background = self._l_problem_names[0]
        self._patch      = self._l_problem_names[1]

    def begin_timestep(self):
        if self._ipass == 0:
            # Compute interpolation and spread matrices
            self.calculate_interp_matrix(self._background, self._patch)
            self.calculate_spread_matrix(self._background, self._patch)
            self._ipass = 1
        pass

    def begin_iteration(self):
        pass

    def begin_problem(self, icoupling):
        pass

    def end_problem(self, icoupling):
        pass

    def end_iteration(self):
        pass
