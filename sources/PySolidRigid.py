"""
    FsiPy Rigid Solid Class
    ==================
"""

# External libraries
import numpy         as np
from   mpi4py        import MPI

# Internal libraries
import PyKernel      as PyKernel
import FortranSolid  as FortranSolid
from   PyParameters  import *


class SolidRigid(PyKernel.Problem):
    """ Solid class in Python. """

    # Initialize object 
    def __init__(self, d_params, d_mesh, d_bconds, d_parallel, rank):
        """ Initializes Problem object. """
        
        # 1. Fortran physics module name (white string for no Fortran module )
        d_params['module_name'] = " "

        # 2.1. Names of variables
        self._l_variables_names = ['acceleration','velocity','displacement']        
        # 2.2. Dimensionality of variables ['vector'|'scalar']
        self._l_variables_dims  = ['vector', 'vector', 'vector']

        # 3.1 Names of equations
        self._l_equation_names         = ['newton_euler']
        # 3.2 Primary variables involved in each equation        
        self._l_variables_in_equations = ['displacement']
        # 3.3 Flags for dynamic assembly of LHS in each equation        
        self._l_flags_dynamic_lhs      = [False]

        # 4. Number of past timesteps to be saved
        d_params['npast'] = 3
        
        # 5. Variables involved in boundary conditions
        self._d_dofs_in_bconds    = {'nodes'    : 'displacement'}

        # 6. Problem tasks  
        self._l_tasks_begin_timestep  = ['update_time'];
        self._l_tasks_begin_equation  = [];
        self._l_tasks_begin_iteration = [];
        self._l_tasks_do_iteration    = ['solve_algebraic_system'];
        self._l_tasks_end_iteration   = [];
        self._l_tasks_end_equation    = [];
        self._l_tasks_end_timestep    = ['postprocess'];
        
        # 6. Initialize Finite Element Problem object
        PyKernel.Problem.__init__(self,
                                  d_params,
                                  d_mesh,
                                  d_bconds,
                                  d_parallel,
                                  rank)
        print (self._ndime)
        print (self._ndofs)
        print (self._nnode)   
        print (self._nelem)

    

    def solve_algebraic_system(self, iequation):
        pass
       


    def initialize_material_properties(self):
        """ Initializes material properties. 

            Outputs
            -------
                self._rho : numpy array of dims (nelem,ngaus)
                    solid mass density at each gauss point.
                self._young : numpy array of dims (nelem,ngaus)
                    solid Young modulus at each gauss point.
        """        



    def compute_timestep_size(self):
        """ Calculate critical timestep.

            Outputs
            -------
                self._d_params['dt'] : float
                    time-step size.
                self._d_params['cfl'] : float
                    CFL number ( = dt / dt_critical )
                self._d_params['dt_critical'] : float
                    critical timestep size.
        """
        # Save to dict
        self._d_params['dt']            = 1.0


        
    def activate_solid_advection(self):
        """ Activates flag for advection of solid nodes. """

        self._flag_advected_nodes = True
