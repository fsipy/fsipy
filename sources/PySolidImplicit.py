"""
    FsiPy Solid Implicit Module
    ===========================

|--------------------------------------------------------------------| 
|                     Execution flow-chart                           | 
|--------------------------------------------------------------------| 
    for time_loop
    |   begin_timestep()
    |   for equation_loop
    |   |   begin_equation()
    |   |   for iter_loop
    |   |   |   begin_iteration()
    |   |   |   do_iteration()
    |   |   |   end_iteration()
    |   |   end iter_loop
    |   |   end_equation()
    |   end equation_loop
    |   end_timestep()
    end time_loop
|--------------------------------------------------------------------| 
"""

# External libraries
import numpy                as np
import numpy.linalg         as la
from   mpi4py               import MPI

# Internal libraries
import PyKernel             as PyKernel
import FortranSolidImplicit as FortranSolidImplicit
from   PyParameters  import *


class SolidImplicit(PyKernel.Problem):
    """ Implicit solid class in Python. """

    # Initialize object 
    def __init__(self, d_params, d_mesh, d_bconds, d_parallel, rank):
        """ Initializes Problem object. """

        # Physics module name
        d_params['module_name'] = 'FortranSolidImplicit'

        # Number of past timesteps to be saved
        d_params['npast'] = 4

        # Names of variables
        self._l_variables_names = ['displacement','velocity','acceleration']

        # Dimensionality of variables ['vector'|'scalar']
        self._l_variables_dims = ['vector', 'vector', 'vector']

        # Type of variables ['primary'|'secondary']
        self._l_variables_types = ['primary', 'secondary', 'secondary']

        # Names of equations
        self._l_equation_names = ['implicit_solid']

        # Primary variables involved in each equation
        self._l_variables_in_equations = ['displacement']

        # Variables involved in boundary conditions
        # Allow us to impose values directly on a different variable thtan the primary one:
        self._d_dofs_in_bconds        = {self._l_equation_names[0]: self._l_variables_names[2]}        

        self._l_tasks_begin_timestep  = ['update_time',
                                         'update_variables',
                                         'update_boundary_conditions'];
        self._l_tasks_begin_equation  = ['update_variables',
                                         'impose_boundary_conditions_on_variables'];
        self._l_tasks_begin_iteration = ['update_variables'];
        self._l_tasks_do_iteration    = ['assemble_algebraic_system',
                                         'impose_zero_boundary_conditions_on_algebraic_system',
                                         'solve_algebraic_system',
                                         'update_variables',
                                         'calculate_reaction',
                                         'calculate_forces_from_stresses'];
        self._l_tasks_end_iteration   = ['compute_iteration_error',
                                         'update_variables'];
        self._l_tasks_end_equation    = [];
        self._l_tasks_end_timestep    = ['update_variables',
                                         'postprocess'];

        self._l_flags_dynamic_lhs      = []
        self._l_flags_dynamic_lumping  = []

        # Flags for dynamic assembly of LHS in each equation
        self._l_flags_dynamic_lhs      = [True]

        # Flags for dynamic lumping of LHS in correponding each equation
        self._l_flags_for_lumping = [d_params['flag_lump_mass']]

        # Initialize Finite Element Problem object
        PyKernel.Problem.__init__(self,
                                  d_params,
                                  d_mesh,
                                  d_bconds,
                                  d_parallel,
                                  rank)



    def initialize_fortran_module(self):
        """ Initializes corresponding Fortran Module. """

        module_name = self._d_params['module_name']
        str_expr = module_name+'.'+module_name.lower() \
                 + '.initialize('                      \
                 + 'self._o_mesh._l_elems, '           \
                 + 'self._o_mesh._gauss_weights, '     \
                 + 'self._o_mesh._gp_jacobians, '      \
                 + 'self._o_mesh._gp_inv_jacobians, '  \
                 + 'self._o_mesh._gp_det_jacobians, '  \
                 + 'self._o_mesh._gp_coords, '         \
                 + 'self._o_mesh._gp_shape, '          \
                 + 'self._o_mesh._gp_dshape, '         \
                 + 'self._o_mesh._gp_volume, '         \
                 + 'self._rho, '                       \
                 + 'self._young, '                     \
                 + 'self._d_params[\'material\'], '    \
                 + 'self._d_params[\'dt\'], '          \
                 + 'self._d_params[\'alpha\'], '       \
                 + 'self._d_params[\'poisson\'], '     \
                 + 'self._d_params[\'beta\'], '        \
                 + 'self._d_params[\'gamma\'], '       \
                 + 'self._o_mesh._nnode, '             \
                 + 'self._ndofs, '                     \
                 + 'self._npast, '                     \
                 + 'self._ndime, '                     \
                 + 'self._o_mesh._nelem, '             \
                 + 'self._o_mesh._nnode_elem, '        \
                 + 'self._o_mesh._ngaus, '             \
                 + 'self._o_mesh._nbasis) '
        exec(str_expr)



    def initialize_material_properties(self):
        """ Initializes material properties. 

            Outputs
            -------
                self._rho : numpy array of dims (nelem,ngaus)
                    solid mass density at each gauss point.
                self._young : numpy array of dims (nelem,ngaus)
                    solid Young modulus at each gauss point.
        """

        # Save mass density and kinematic viscosity at each gauss point
        self._rho   = np.zeros((self._nelem,self._o_mesh._ngaus),float)
        self._young = np.zeros((self._nelem,self._o_mesh._ngaus),float)
        for ielem in range(self._nelem):
            for igaus in range(self._o_mesh._ngaus):
                self._rho[ielem,igaus]    = self._d_params['rho']
                self._young[ielem,igaus]  = self._d_params['young']



    def compute_timestep_size(self):
        """ Calculate critical timestep.

            Outputs
            -------
                self._d_params['dt'] : float
                    time-step size.
                self._d_params['cfl'] : float
                    CFL number ( = dt / dt_critical )
                self._d_params['dt_critical'] : float
                    critical timestep size.
        """

        # For now only static timestep sizes    
        if self._tcount > 0: return

        # Compute global critical timestep and element intrinsic times
        dt_critical = 1e13
        hmin        = 1.0e13
        for ielem in range(self._nelem):
            l_inodes   = self._o_mesh._l_elems[ielem,:]
            h          = self._o_mesh._l_elem_lengths[ielem]
            wavespeed  = np.sqrt( self._d_params['young']/self._d_params['rho'] )
            dt_element = h / wavespeed

            # Global critical timestep
            dt_critical = min(dt_critical, dt_element)
            hmin        = min([hmin, h])

        # Timestep from critical
        if self._d_params['time_coupling'] == 'from_critical':
            dt  = self._d_params['cfl'] * dt_critical
            cfl = self._d_params['cfl']
        # Fixed timestep
        else:
            dt  = self._d_params['dt']
            cfl = self._d_params['dt'] / dt_critical

        # Save to parameters' dictionary
        self._d_params['dt']            = dt
        self._d_params['cfl']           = cfl
        self._d_params['dt_critical']   = dt_critical
        self._d_params['min_elem_size'] = hmin



    def update_variables(self):
        """ Does variable updates according to stage given by flag. """

        # Load time-scheme parameters
        beta   =  self._d_params['beta']
        gamma  =  self._d_params['gamma']
        dt     =  self._d_params['dt']
        l_cou  =  self._d_coupling_dofs[self._l_equation_names[0]]
        l_ncou = ~self._d_coupling_dofs[self._l_equation_names[0]]

        ## Begin timestep
        if self._flag_in_begstep:
            # Update auxiliary iteration variables
            self.update_aux_iterative_variables()


        ## Begin equation
        if self._flag_in_begeqn:
            # Compute trial displacement and velocity for beta-Newmark scheme
            self._trial_displacement = self._d_variables['displacement'][:,:,time_n] \
                                     + self._d_variables['velocity'][:,:,time_n] * dt \
                                     + self._d_variables['acceleration'][:,:,time_n] * (0.5-beta)*dt**2
            self._trial_velocity     = self._d_variables['velocity'][:,:,time_n] \
                                     + self._d_variables['acceleration'][:,:,time_n] * (1.0-gamma)*dt
            # Update current iteration displacement to trial displacement
            self._d_variables['displacement'][:,:,iter_k] = self._trial_displacement.copy()
            self._unkno                                   = self._trial_displacement.copy()


        ## Begin iteration 
        if self._flag_in_begiter:
            self._unkno = self._d_variables['displacement'][:,:,iter_k].copy()
            # Velocity and acceleration for non-coupled nodes: 
            self._d_variables['acceleration'][l_ncou,iter_k] = 1.0/(beta*dt**2) \
                    * ( self._d_variables['displacement'][l_ncou,iter_k]        \
                    -   self._trial_displacement[l_ncou] )
            self._d_variables['velocity'][l_ncou,iter_k] = self._trial_velocity[l_ncou] \
                    + gamma*dt * self._d_variables['acceleration'][l_ncou,iter_k]

            # Velocity and acceleration for coupled nodes: 
            self._d_variables['acceleration'][l_cou,iter_k] = 1.0/(gamma*dt)           \
                    * (                self._d_variables['velocity'][l_cou,iter_k]     \
                    -                  self._d_variables['velocity'][l_cou,time_n]     \
                    - (1.0-gamma)*dt * self._d_variables['acceleration'][l_cou,time_n] )

        ## In iteration
        if self._flag_in_iter:
            # Obtain displacement increment from solution of algebraic system
            self._ddisplacement         = self._d_variables['displacement'][:,:,iter_k].copy()
            self._ddisplacement[l_cou]  = self._d_variables['velocity'][l_cou,iter_k] * dt
#            self._ddisplacement[l_cou]  = self._d_variables['velocity'][l_cou,time_n] * dt \
#                    + dt**2*(0.5-beta)  * self._d_variables['acceleration'][l_cou,time_n] \
#                    + dt**2             * self._d_variables['acceleration'][l_cou,iter_k]
            # Save displacement from last iteration
            self._d_variables['displacement'][:,:,iter_k] = self._unkno.copy()
            # Add increment to unknown
            self._unkno += self._ddisplacement


        ## End iteration 
        if self._flag_in_enditer:
            # Save unknown to current iteration displacement (after evaluating error)
            self._d_variables['displacement'][:,:,iter_k] = self._unkno.copy()
            # Update total lagrangian coordinates
            self._o_mesh._l_node_coords_ale = self._o_mesh._l_node_coords \
                                            + self._d_variables['displacement'][:,:,iter_k].T

        ## End timestep
        if self._flag_in_endstep:
            # Update auxiliary iteration and timestep variables
            self.update_aux_iterative_variables()
            self.update_aux_time_variables()
            self.update_time_variables()



    def compute_iteration_error(self, iequation):
        """ Compute error for iteration as:

            Error = ||u(n+1,k+1) - u(n+1,k)|| / ||u(n+1,k+1)||

            where k: iteration index
                  n: time index

        """
        norm = la.norm(self._unkno)
        diff = la.norm(self._unkno - self._d_variables['displacement'][:,:,iter_k])
        if norm < 1e-13:
            self._error = diff
        else:
            self._error = diff / norm
        return self._error



    def print_parameters(self):
        """ Prints problem parameters. """

        if MPI.COMM_WORLD.Get_rank() == 0:        
            print('\nProblem Parameters:')
            print('-------------------\n')
            print('    Physics:             ',self._d_params['physics'])
            print('    Time-treatment:      ',self._d_params['time_treatment'])
            print('    Material model:      ',self._d_params['material'])
            print('    Mass density:        ',self._d_params['rho'])
            print('    Young modulus:       ',self._d_params['young'])
            print('    Poisson ratio:       ',self._d_params['poisson'])
            print('    beta-Newmark scheme:')
            print('        Beta:            ',self._d_params['beta'])
            print('        Gamma:           ',self._d_params['gamma'])
            print('        Timestep:        ',self._d_params['dt'])
            print('    CFL Number:          ',self._d_params['cfl'])
            print('    Rayleigh alpha coef: ',self._d_params['alpha'])
            print('    Number of elements:  ',self._nelem)
            print('    Number of nodes:     ',self._nnode)
            print('    Volume Quadrature:   ',self._d_params['volume_quadrature'])
            print('    Surface Quadrature:  ',self._d_params['surface_quadrature'])
            print(' ')
