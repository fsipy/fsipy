module FortranKernel
! ============================================================================ !
!                           FsiPy Fortran Kernel                               !
! ============================================================================ !
    use types
    implicit none
    !
    ! Equation class in Fortran
    !
    type, abstract, public :: Equation
        !
        ! Attributes (none)
        !
    contains
        !
        ! Explicit procedures
        !
        procedure, pass(this), public :: assemble_algebraic_system
        procedure, pass(this), public :: assemble_lhs_and_rhs
        procedure, pass(this), public :: assemble_rhs
        procedure, pass(this), public :: assemble_force
        !
        ! Deferred procedures
        !
        procedure(compute_gauss_lhs_abs),     pass(this), deferred, public :: compute_gauss_lhs
        procedure(compute_gauss_rhs_abs),     pass(this), deferred, public :: compute_gauss_rhs
        procedure(compute_gauss_lhs_rhs_abs), pass(this), deferred, public :: compute_gauss_lhs_rhs
        procedure(compute_gauss_force_abs),   pass(this), deferred, public :: compute_gauss_force
        procedure(compute_natural_bcs_abs),   pass(this), deferred, public :: compute_natural_bcs
    end type

    interface
        !
        ! Abstract subroutines to be explicited in instances of class
        !
        subroutine compute_gauss_lhs_abs(this,           &
                                         gplhs,          &
                                         elvar,          &
                                         elevelset,      &
                                         nnode_in,       &
                                         ndofs_total_in, &
                                         ndofs_in,       &
                                         npast_in,       &
                                         ielem,          &
                                         igaus)
            import                       :: Equation
            class(Equation), intent(in)  :: this
            real(8),         intent(out) :: gplhs(ndofs_in, nnode_in, ndofs_in, nnode_in)
            real(8),         intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
            real(8),         intent(in)  :: elevelset(nnode_in)
            integer(4),      intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
            integer(4),      intent(in)  :: ielem, igaus
        end subroutine
        subroutine compute_gauss_rhs_abs(this,           &
                                         gprhs,          &
                                         elvar,          &
                                         elsource,       &
                                         elevelset,      &
                                         nnode_in,       &
                                         ndofs_total_in, &
                                         ndofs_in,       &
                                         npast_in,       &
                                         ielem,          &
                                         igaus)
            import                       :: Equation
            class(Equation), intent(in)  :: this
            real(8),         intent(out) :: gprhs(ndofs_in, nnode_in)
            real(8),         intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
            real(8),         intent(in)  :: elsource(ndofs_total_in, nnode_in)
            real(8),         intent(in)  :: elevelset(nnode_in)
            integer(4),      intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
            integer(4),      intent(in)  :: ielem, igaus
        end subroutine
        subroutine compute_gauss_lhs_rhs_abs(this,           &
                                             gplhs,          &
                                             gprhs,          &
                                             elvar,          &
                                             elsource,       &
                                             elevelset,      &
                                             nnode_in,       &
                                             ndofs_total_in, &
                                             ndofs_in,       &
                                             npast_in,       &
                                             ielem,          &
                                             igaus)
            import                       :: Equation
            class(Equation), intent(in)  :: this
            real(8),         intent(out) :: gplhs(ndofs_in, nnode_in, ndofs_in, nnode_in)
            real(8),         intent(out) :: gprhs(ndofs_in, nnode_in)
            real(8),         intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
            real(8),         intent(in)  :: elsource(ndofs_total_in, nnode_in)
            real(8),         intent(in)  :: elevelset(nnode_in)
            integer(4),      intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
            integer(4),      intent(in)  :: ielem, igaus
        end subroutine
        subroutine compute_gauss_force_abs(this,           &
                                           gpforce,        &
                                           elvar,          &
                                           elsource,       &
                                           nnode_in,       &
                                           ndofs_total_in, &
                                           ndofs_in,       &
                                           npast_in,       &
                                           ielem,          &
                                           igaus)
            import                       :: Equation
            class(Equation), intent(in)  :: this
            real(8),         intent(out) :: gpforce(ndofs_in, nnode_in)
            real(8),         intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
            real(8),         intent(in)  :: elsource(ndofs_total_in, nnode_in)
            integer(4),      intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
            integer(4),      intent(in)  :: ielem, igaus
        end subroutine
        subroutine compute_natural_bcs_abs(this,           &
                                           bcnat,          &
                                           source,         &
                                           nnode_in,       &
                                           ndofs_total_in, &
                                           ndofs_in)
            import                       :: Equation
            class(Equation), intent(in)  :: this
            real(8),         intent(out) :: bcnat(ndofs_in, nnode_in)
            real(8),         intent(in)  :: source(ndofs_total_in, nnode_in)
            integer(4),      intent(in)  :: nnode_in, ndofs_total_in, ndofs_in
        end subroutine
        pure function int_det( M ) result ( det )
            real(8), intent(in) :: M(:,:)
            real(8)             :: det
        end function
        pure function int_inv( M ) result ( R )
            real(8), intent(in) :: M(:,:)
            real(8)             :: R(size(M,1),size(M,1))
            real(8)             :: detinv 
        end function
    end interface
    interface outx
        module procedure outx11, outx22
    end interface outx
    !
    ! Define procedures
    !
    procedure(int_det), pointer :: matdet 
    procedure(int_inv), pointer :: matinv
    !
    ! Global variables received from Python Kernel
    !
    public
    integer(4)              :: ndime
    integer(4)              :: nnode
    integer(4)              :: nelem
    integer(4)              :: nnode_elem
    integer(4)              :: ndofs_total
    integer(4)              :: ngaus
    integer(4)              :: nbasis
    integer(4)              :: npast
    integer(4), parameter   :: iter_k   = 1
    integer(4), parameter   :: iter_aux = 2
    integer(4), parameter   :: time_n   = 3
    integer(4), parameter   :: time_aux = 4
    real(8)                 :: dt
    integer(4), allocatable :: l_elems(:,:)
    real(8),    allocatable :: gauss_weights(:)
    real(8),    allocatable :: gp_jacobians(:,:,:,:)
    real(8),    allocatable :: gp_inv_jacobians(:,:,:,:)
    real(8),    allocatable :: gp_det_jacobians(:,:)
    real(8),    allocatable :: gp_coords(:,:,:)
    real(8),    allocatable :: gp_shape(:,:)
    real(8),    allocatable :: gp_dshape(:,:,:)
    real(8),    allocatable :: gp_volume(:,:)
    character(len=7)        :: material
    !
    ! Global variables calculated in Fortran Kernel
    !
    real(8),    allocatable :: gp_dshape_glo(:,:,:,:)
    real(8),    allocatable :: gp_shape_shape(:,:,:)
    real(8),    allocatable :: gp_shape_dshape(:,:,:,:,:)
    real(8),    allocatable :: gp_dshape_shape(:,:,:,:,:)
    real(8),    allocatable :: gp_dshape_dshape(:,:,:,:)
    real(8),    pointer     :: identity(:,:)
    real(8),    parameter   :: zero = 0.0_dp
    real(8),    parameter   :: one  = 1.0_dp
    real(8),    parameter   :: I22(2,2) = reshape( (/one,zero,zero,one/), (/2,2/) )
    real(8),    parameter   :: I33(3,3) = reshape( (/one,zero,zero,zero,one,zero,zero,zero,one/), (/3,3/) )

contains


subroutine initialize_kernel(l_elems_in,   &
                      gauss_weights_in,    &
                      gp_jacobians_in,     &
                      gp_inv_jacobians_in, &
                      gp_det_jacobians_in, &
                      gp_coords_in,        &
                      gp_shape_in,         &
                      gp_dshape_in,        &
                      gp_volume_in,        &
                      material_in,         &
                      dt_in,               &
                      nnode_in,            &
                      ndofs_total_in,      &
                      npast_in,            &
                      ndime_in,            &
                      nelem_in,            &
                      nnode_elem_in,       &
                      ngaus_in,            &
                      nbasis_in)
    !
    ! Initializes kernel global variables used by all modules
    !
    ! --------------------------------------------------------------------------
    integer(4),       intent(in) :: l_elems_in(nelem_in,nnode_elem_in)
    real(8),          intent(in) :: gauss_weights_in(ngaus_in)
    real(8),          intent(in) :: gp_jacobians_in(nelem_in,ngaus_in,ndime_in,ndime_in)
    real(8),          intent(in) :: gp_inv_jacobians_in(nelem_in,ngaus_in,ndime_in,ndime_in)
    real(8),          intent(in) :: gp_det_jacobians_in(nelem_in,ngaus_in)
    real(8),          intent(in) :: gp_coords_in(nelem_in,ngaus_in,ndime_in)
    real(8),          intent(in) :: gp_shape_in(ngaus_in,nbasis_in)
    real(8),          intent(in) :: gp_dshape_in(ngaus_in,nbasis_in,ndime_in)
    real(8),          intent(in) :: gp_volume_in(nelem_in,ngaus_in)
    character(len=7), intent(in) :: material_in
    real(8),          intent(in) :: dt_in
    integer(4),       intent(in) :: nnode_in
    integer(4),       intent(in) :: ndofs_total_in
    integer(4),       intent(in) :: npast_in
    integer(4),       intent(in) :: ndime_in
    integer(4),       intent(in) :: nelem_in
    integer(4),       intent(in) :: nnode_elem_in
    integer(4),       intent(in) :: ngaus_in
    integer(4),       intent(in) :: nbasis_in

    ! Save FortranKernel's global parameters
    dt          = dt_in
    nnode       = nnode_in
    ndofs_total = ndofs_total_in
    npast       = npast_in
    ndime       = ndime_in
    nnode       = nnode_in
    nelem       = nelem_in
    nnode_elem  = nnode_elem_in
    ngaus       = ngaus_in
    nbasis      = nbasis_in
    material    = material_in

    ! Allocate FortranKernel's global arrays
    allocate( l_elems(nelem_in,nnode_elem_in)                       )
    allocate( gauss_weights(ngaus_in)                               )
    allocate( gp_jacobians(nelem_in,ngaus_in,ndime_in,ndime_in)     )
    allocate( gp_inv_jacobians(nelem_in,ngaus_in,ndime_in,ndime_in) )
    allocate( gp_det_jacobians(nelem_in,ngaus_in)                   )
    allocate( gp_coords(nelem_in,ngaus_in,ndime_in)                 )
    allocate( gp_shape(ngaus_in,nbasis_in)                          )
    allocate( gp_dshape(ngaus_in,nbasis_in,ndime_in)                )
    allocate( gp_volume(nelem_in,ngaus_in)                          )
    allocate( gp_dshape_glo(nelem,ngaus,nnode_elem,ndime)           )
    allocate( gp_shape_shape(ngaus,nnode_elem,nnode_elem)           )

    ! Save FortranKernel's global arrays
    l_elems          = l_elems_in + 1     ! Convert indexing to Fortran format (i=1,2,..)
    gauss_weights    = gauss_weights_in
    gp_jacobians     = gp_jacobians_in
    gp_inv_jacobians = gp_inv_jacobians_in
    gp_det_jacobians = gp_det_jacobians_in
    gp_coords        = gp_coords_in
    gp_shape         = gp_shape_in
    gp_dshape        = gp_dshape_in
    gp_volume        = gp_volume_in

    ! Assign dimension pointers for simple matrix operations
    call assign_dimension_pointers(ndime)
    ! Calculate FortranKernel's elemental matrices
    call compute_global_shape_function_derivatives()
    call compute_element_mass_matrix()
end subroutine


subroutine assemble_algebraic_system(this,              &
                                     lhs,               &
                                     idlhs,             &
                                     rhs,               &
                                     variables,         &
                                     source,            &
                                     levelset,          &
                                     flag_assemble_lhs, &
                                     tcount,            &
                                     ndofs,             &
                                     lhs_lump)
    !
    ! Assembles Equation's RHS and (optionally) LHS
    !
    class(Equation), intent(in)  :: this
    real(8),         intent(out) :: lhs(nelem*nnode_elem**2*ndofs**2)
    integer(4),      intent(out) :: idlhs(nelem*nnode_elem**2*ndofs**2, 2)
    real(8),         intent(out) :: rhs(ndofs*nnode)
    real(8),         intent(in)  :: variables(ndofs_total, nnode, npast)
    real(8),         intent(in)  :: source(ndofs_total, nnode)
    real(8),         intent(in)  :: levelset(nnode)    
    logical,         intent(in)  :: flag_assemble_lhs
    integer(4),      intent(in)  :: tcount 
    integer(4),      intent(in)  :: ndofs
    real(8),         intent(out) :: lhs_lump(ndofs*nnode)    

    if (flag_assemble_lhs) then
        ! Assembles both LHS and RHS
        call this % assemble_lhs_and_rhs(lhs,       &
                                         idlhs,     &
                                         rhs,       &
                                         variables, &
                                         source,    &
                                         levelset,  &
                                         ndofs,     &
                                         lhs_lump)        
    else
        ! Assembles RHS only
        call this % assemble_rhs(rhs,       &
                                 variables, &
                                 source,    &
                                 levelset,  &
                                 ndofs)
    end if
end subroutine


subroutine assemble_lhs_and_rhs(this,      &
                                lhs,       &
                                idlhs,     &
                                rhs,       &
                                variables, &
                                source,    &
                                levelset,  &
                                ndofs,     &
                                lhs_lump)
    !
    ! Assembles Equation's LHS and RHS 
    !
    class(Equation), intent(in)  :: this
    real(8),         intent(out) :: lhs      (nelem*nnode_elem**2*ndofs**2)
    integer(4),      intent(out) :: idlhs    (nelem*nnode_elem**2*ndofs**2, 2)
    real(8),         intent(out) :: rhs      (ndofs*nnode)
    real(8),         intent(in)  :: variables(ndofs_total, nnode, npast)
    real(8),         intent(in)  :: source   (ndofs_total, nnode)
    real(8),         intent(in)  :: levelset (nnode)
    integer(4),      intent(in)  :: ndofs
    real(8),         intent(out) :: lhs_lump (ndofs*nnode)    
    real(8)                      :: ellhs    (ndofs,       nnode_elem, ndofs, nnode_elem)
    real(8)                      :: elrhs    (ndofs,       nnode_elem)
    real(8)                      :: elvar    (ndofs_total, nnode_elem, npast)
    real(8)                      :: elsource (ndofs_total, nnode_elem)
    real(8)                      :: elevelset(nnode_elem)
    real(8)                      :: gplhs    (ndofs,       nnode_elem, ndofs, nnode_elem)
    real(8)                      :: gprhs    (ndofs,       nnode_elem)
    real(8)                      :: bcnat    (ndofs,       nnode)
    integer(4)                   :: ielem, igaus, idof, jdof, icount
    integer(4)                   :: inode, jnode, inode_glo, jnode_glo, irow_glo, jcol_glo
    real(8) :: tmp

    ! Initialize LHS and RHS
    idlhs  = 0
    lhs    = 0.0_dp
    rhs    = 0.0_dp
    icount = 0

    ! Loop over elements
    do ielem = 1,nelem
        !
        ! Compute element LHS and RHS 
        !
        elvar     = variables(:, l_elems(ielem, :), :)
        elsource  =    source(:, l_elems(ielem, :))
        elevelset =  levelset(   l_elems(ielem, :))
        ellhs = 0.0_dp
        elrhs = 0.0_dp
        ! Loop over gauss points
        do igaus = 1,ngaus
            ! Compute LHS and RHS at gauss point
            call this % compute_gauss_lhs_rhs(gplhs,       &
                                              gprhs,       &
                                              elvar,       &
                                              elsource,    &
                                              elevelset,   &
                                              nnode_elem,  &
                                              ndofs_total, &
                                              ndofs,       &
                                              npast,       &
                                              ielem,       &
                                              igaus)
            ! Loop over nodes of element
            do inode = 1,nnode_elem
                ! Loop over degrees of freedom
                do idof = 1,ndofs
                    tmp = elrhs(idof,inode)
                    ! Assemble gauss point contribution to element RHS
                    elrhs(idof,inode) = elrhs(idof,inode) &
                                      + gprhs(idof,inode) &
                                      * gp_volume(ielem,igaus)
                    ! Loop over nodes of element
                    do jnode = 1,nnode_elem
                        ! Loop over degrees of freedom
                        do jdof = 1,ndofs
                            ! Assemble gauss point contribution to element LHS
                            ellhs(idof,inode,jdof,jnode) = ellhs(idof,inode,jdof,jnode) &
                                                         + gplhs(idof,inode,jdof,jnode) &
                                                         * gp_volume(ielem,igaus)
                        end do
                    end do
                end do
            end do
        end do
        !
        ! Assemble element contribution to global LHS and RHS
        !
        ! Loop over element nodes
        do inode = 1,nnode_elem
            ! Global node id
            inode_glo = l_elems(ielem,inode)
            ! Loop over degrees of freedom
            do idof = 1,ndofs
                ! Update row id for ssp matrix using Py indices starting at 0
                irow_glo = ndofs*(inode_glo-1) + idof-1
                ! Assemble RHS contribution
                rhs(irow_glo+1) = rhs(irow_glo+1) + elrhs(idof,inode)
                ! Loop over element nodes
                do jnode = 1,nnode_elem
                    do jdof = 1,ndofs
                        ! Global node id
                        jnode_glo = l_elems(ielem,jnode)
                        ! Update column id for ssp matrix using Py indices starting at 0
                        jcol_glo = ndofs*(jnode_glo-1) + jdof-1
                        ! update sparse matrix row id (it's also possible to assign
                        ! a unique id so that we directly sum as: lhs(irow)+=elhs(..))
                        icount          = icount + 1
                        idlhs(icount,:) = (/ irow_glo , jcol_glo /)
                        lhs  (icount)   = ellhs(idof, inode, jdof, jnode)
                        ! Calculate lump mass matrix
                        ! It is important to sum +1 because global indices are 
                        ! calculated for pyhton not for fortran 
                        lhs_lump(irow_glo+1) = lhs_lump(irow_glo+1) + ellhs(idof, inode, jdof, jnode)
                    end do
                end do
            end do
        end do
    end do
    !
    ! Assemble natural boundary conditions
    !
    call this % compute_natural_bcs(bcnat, source, nnode, ndofs_total, ndofs)
    do inode=1,nnode
        do idof=1,ndofs
            irow_glo      = ndofs*(inode-1) + idof
            rhs(irow_glo) = rhs(irow_glo) + bcnat(idof,inode)
        end do
    end do

end subroutine


subroutine assemble_rhs(this,        &
                        rhs,         &
                        variables,   &
                        source,      &
                        levelset,    &
                        ndofs)
    !
    ! Assembles Equation's RHS only
    !
    class(Equation), intent(in)  :: this
    real(8),         intent(out) :: rhs(ndofs*nnode)
    real(8),         intent(in)  :: variables(ndofs_total, nnode, npast)
    real(8),         intent(in)  :: source(ndofs_total, nnode)
    real(8),         intent(in)  :: levelset(nnode)
    integer(4),      intent(in)  :: ndofs
    real(8)                      :: elrhs(ndofs, nnode_elem)
    real(8)                      :: elvar(ndofs_total, nnode_elem, npast)
    real(8)                      :: elsource(ndofs_total, nnode_elem)
    real(8)                      :: elevelset(nnode_elem)
    real(8)                      :: gprhs(ndofs, nnode_elem)
    real(8)                      :: bcnat(ndofs, nnode)
    integer(4)                   :: ielem, igaus, idof
    integer(4)                   :: inode, inode_glo, irow_glo

    ! Initializes RHS
    rhs = 0.0_dp

    ! Loop over elements
    do ielem = 1,nelem
        !
        ! Compute element RHS 
        !
        elvar     = variables(:, l_elems(ielem, :), :)
        elsource  =    source(:, l_elems(ielem, :))
        elevelset =  levelset(   l_elems(ielem, :))
        elrhs = 0.0_dp
        ! Loop over gauss points
        do igaus = 1,ngaus
            ! Compute RHS at gauss point
            call this % compute_gauss_rhs(gprhs,       &
                                          elvar,       &
                                          elsource,    &
                                          elevelset,   &
                                          nnode_elem,  &
                                          ndofs_total, &
                                          ndofs,       &
                                          npast,       &
                                          ielem,       &
                                          igaus)
            ! Loop over nodes of element
            do inode = 1,nnode_elem
                ! Loop over degrees of freedom
                do idof = 1,ndofs
                    ! Calculate contribution to element RHS
                    elrhs(idof,inode) = elrhs(idof,inode) &
                                      + gprhs(idof,inode) & 
                                      * gp_volume(ielem,igaus)
                end do
            end do
        end do
        !
        ! Assemble element contribution to global RHS
        !
        ! Loop over element nodes
        do inode = 1,nnode_elem
            inode_glo = l_elems(ielem,inode)
            ! Loop over degrees of freedom
            do idof = 1,ndofs
                ! Update row id for ssp matrix using Py indices starting at 0
                irow_glo = ndofs*(inode_glo-1) + idof
                ! Assemble RHS contribution
                rhs(irow_glo) = rhs(irow_glo) + elrhs(idof,inode)
            end do
        end do
    end do
    !
    ! Assemble natural boundary conditions
    !
    call this % compute_natural_bcs(bcnat, source, nnode, ndofs_total, ndofs)
    do inode=1,nnode
        do idof=1,ndofs
            irow_glo      = ndofs*(inode-1) + idof
            rhs(irow_glo) = rhs(irow_glo) + bcnat(idof,inode)
        end do
    end do

end subroutine


subroutine assemble_force(this,         &
                          force,        &
                          variables,    &
                          source,       &
                          ndofs)
    !
    ! Assembles Equation's force from stress tensor only
    !
    class(Equation), intent(in)  :: this
    real(8),         intent(out) :: force(ndofs*nnode)
    real(8),         intent(in)  :: variables(ndofs_total, nnode, npast)
    real(8),         intent(in)  :: source(ndofs_total, nnode)
    integer(4),      intent(in)  :: ndofs
    real(8)                      :: elforce(ndofs, nnode_elem)
    real(8)                      :: elvar(ndofs_total, nnode_elem, npast)
    real(8)                      :: elsource(ndofs_total, nnode_elem)
    real(8)                      :: gpforce(ndofs, nnode_elem)
    integer(4)                   :: ielem, igaus, idof
    integer(4)                   :: inode, inode_glo, irow_glo

    ! Initializes force
    force = 0.0_dp

    ! Loop over elements
    do ielem = 1,nelem
        !
        ! Compute element force 
        !
        elvar    = variables(:, l_elems(ielem, :), :)
        elsource =    source(:, l_elems(ielem, :))
        elforce  = 0.0_dp
        ! Loop over gauss points
        do igaus = 1,ngaus
            ! Compute force at gauss point
            call this % compute_gauss_force(gpforce,     &
                                            elvar,        &
                                            elsource,     &
                                            nnode_elem,   &
                                            ndofs_total,  &
                                            ndofs,        &
                                            npast,        &
                                            ielem,        &
                                            igaus)
            ! Loop over nodes of element
            do inode = 1,nnode_elem
                ! Loop over degrees of freedom
                do idof = 1,ndofs
                    ! Calculate contribution to element force 
                    elforce(idof,inode) = elforce(idof,inode) &
                                        + gpforce(idof,inode) & 
                                        * gp_volume(ielem,igaus)
                end do
            end do
        end do
        !
        ! Assemble element contribution to global force 
        !
        ! Loop over element nodes
        do inode = 1,nnode_elem
            inode_glo = l_elems(ielem,inode)
            ! Loop over degrees of freedom
            do idof = 1,ndofs
                ! Update row id for ssp matrix using Py indices starting at 0
                irow_glo = ndofs*(inode_glo-1) + idof
                ! Assemble force contribution
                force(irow_glo) = force(irow_glo) + elforce(idof,inode)
            end do
        end do
    end do

end subroutine


subroutine compute_global_mass_matrix(mass_matrix, idmass)
    !
    ! Assembles global mass matrix 
    !
    real(8),    intent(out) :: mass_matrix(nelem*nnode_elem**2)
    integer(4), intent(out) :: idmass(nelem*nnode_elem**2, 2)
    integer(4)              :: ielem, igaus, inode, jnode
    integer(4)              :: inode_glo, jnode_glo, icount
    real(8)                 :: elmass(nnode_elem,nnode_elem)
  
    ! Initialization
    mass_matrix(:) = 0.0_dp
    idmass         = 0
    icount         = 0
  
    ! Loop over elements
    do ielem = 1,nelem
        !
        ! Compute element mass matrix
        !
        elmass = 0.0_dp
        do igaus = 1,ngaus
            do inode = 1,nnode_elem
                do jnode = 1,nnode_elem
                    elmass(inode,jnode) = elmass(inode,jnode) &
                                        + gp_shape_shape(igaus,inode,jnode) &
                                        * gp_volume(ielem,igaus)
                end do
            end do
        end do
        !
        ! Assemble element mass matrix to global mass matrix
        !
        do inode = 1,nnode_elem
            inode_glo = l_elems(ielem,inode) - 1
            do jnode = 1,nnode_elem
                jnode_glo = l_elems(ielem,jnode) - 1
                icount              = icount + 1
                idmass(icount,:)    = (/ inode_glo , jnode_glo /)
                mass_matrix(icount) = elmass(inode,jnode)
            end do
        end do
    end do
end subroutine


subroutine compute_global_laplacian_matrix(lapla, idmat)
    !
    ! Assembles global laplacian matrix
    !
    real(8),    intent(out) :: lapla(nelem*nnode_elem**2)
    integer(4), intent(out) :: idmat(nelem*nnode_elem**2, 2)
    real(8)                 :: elmat(nnode_elem, nnode_elem)
    real(8)                 :: gpmat(nnode_elem, nnode_elem)
    integer(4)              :: ielem, igaus, icount
    integer(4)              :: inode, jnode, inode_glo, jnode_glo

    ! Initialize arrays
    idmat  = 0
    lapla  = 0.0_dp
    icount = 0

    ! Loop over elements
    do ielem = 1,nelem
        !
        ! Compute element laplacian
        !
        elmat = 0.0_dp
        ! Loop over gauss points
        do igaus = 1,ngaus
            ! Compute dNdN at gauss point
            gpmat = gp_dshape_dshape(ielem,igaus,:,:)
            ! Loop over nodes of element
            do inode = 1,nnode_elem
                ! Loop over nodes of element
                do jnode = 1,nnode_elem
                    ! Assemble gauss point contribution to element laplacian
                    elmat(inode,jnode) = elmat(inode,jnode) &
                                       + gpmat(inode,jnode) &
                                       * gp_volume(ielem,igaus)
                end do
            end do
        end do
        !
        ! Assemble element contribution to global laplacian
        !
         ! Loop over element nodes
        do inode = 1,nnode_elem
            ! Global node id
            inode_glo = l_elems(ielem,inode)-1
            ! Loop over element nodes
            do jnode = 1,nnode_elem
                ! Global node id
                jnode_glo = l_elems(ielem,jnode)-1
                ! update sparse matrix row id (it's also possible to assign
                ! a unique id so that we directly sum as: lapla(irow)+=elmat(..))
                icount          = icount + 1
                idmat(icount,:) = (/ inode_glo , jnode_glo /)
                lapla(icount)   = elmat(inode, jnode)
            end do
        end do
    end do

end subroutine


subroutine compute_levelset_rhs(rhs, source)
    !
    ! Assembles levelset RHS
    !
    real(8),    intent(out) :: rhs(nnode)
    real(8),    intent(in)  :: source(ndime*nnode)
    real(8)                 :: elrhs(nnode_elem)
    real(8)                 :: gprhs(nnode_elem)
    real(8)                 :: elsource(ndime,nnode_elem)
    integer(4)              :: ielem, igaus, inode, jnode, inode_glo, idime, jdime, irow_glo

    rhs = 0.0_dp

    ! Loop over elements
    do ielem = 1,nelem
        ! Load source
        do inode=1,nnode_elem
            do idime=1,ndime
                inode_glo = l_elems(ielem,inode)
                irow_glo = (inode_glo-1)*ndime + idime
                elsource(idime,inode) = source(irow_glo)
            end do
        end do
        !
        ! Compute element contribution 
        !
        elrhs = 0.0_dp
        ! Loop over gauss points
        do igaus = 1,ngaus
            gprhs = 0.0_dp
            ! Loop over nodes of element
            do inode = 1,nnode_elem
                ! Loop over nodes of element
                do jnode = 1,nnode_elem
                    ! Loop over degrees of freedom
                    do jdime = 1,ndime
                        !
                        ! Compute gauss point contribution
                        !
                        gprhs(inode) = gprhs(inode) &
                                     + gp_shape_dshape(ielem,igaus,inode,jnode,jdime) &
                                     * elsource(jdime,jnode)
                    end do
                end dO
                !
                ! Add gauss point contrbution to element vector
                !
                elrhs(inode) = elrhs(inode) &
                             + gprhs(inode) & 
                             * gp_volume(ielem,igaus)
            end do
        end do
        !
        ! Assemble element contribution 
        !
        ! Loop over element nodes
        do inode = 1,nnode_elem
            inode_glo = l_elems(ielem,inode)
            ! Assemble force contribution
            rhs(inode_glo) = rhs(inode_glo) + elrhs(inode)
        end do
    end do

end subroutine


subroutine compute_global_shape_function_derivatives()
    !
    ! Computes cartesian derivatives of shape functions 
    !
    integer(4) :: ielem, igaus, inode, idime, jdime
    gp_dshape_glo = 0.0_dp
    do ielem=1,nelem
        do igaus=1,ngaus
            do inode=1,nnode_elem
                do idime=1,ndime
                    do jdime=1,ndime
                      gp_dshape_glo(ielem,igaus,inode,idime) =      &
                          gp_dshape_glo(ielem,igaus,inode,idime)    &
                        + gp_inv_jacobians(ielem,igaus,idime,jdime) &
                        * gp_dshape(igaus,inode,jdime)
                    end do
                end do
            end do
        end do
    end do
end subroutine


subroutine compute_element_mass_matrix()
    !
    ! Computes mass matrix for all elements 
    !
    integer(4) :: igaus, inode, jnode
    gp_shape_shape = 0.0_dp
    do igaus=1,ngaus
        do inode=1,nnode_elem
            do jnode=1,nnode_elem
              gp_shape_shape(igaus,inode,jnode) = gp_shape(igaus,inode) &
                                                * gp_shape(igaus,jnode)
            end do
        end do
    end do
end subroutine


subroutine compute_laplacian_matrix()
    !
    ! Computes laplacian matrix for all elements 
    !
    integer(4) :: ielem, igaus, inode, jnode, idime, jdime
    gp_dshape_dshape = 0.0_dp
    do ielem=1,nelem
        do igaus=1,ngaus
            do inode=1,nnode_elem
                do jnode=1,nnode_elem
                    do idime=1,ndime
                      gp_dshape_dshape(ielem,igaus,inode,jnode) =   &
                          gp_dshape_dshape(ielem,igaus,inode,jnode) &
                        - gp_dshape_glo(ielem,igaus,inode,idime)    &
                        * gp_dshape_glo(ielem,igaus,jnode,idime)
                    end do
                end do
            end do
        end do
    end do
end subroutine


subroutine compute_gradient_matrix()
    !
    ! Computes gradient matrix for all elements 
    !
    integer(4) :: ielem, igaus, inode, jnode, idime, jdime
    !
    ! Nota: Aca estoy definiendo G^ab_i = - ( dN^a/dX_i , N^b )
    !       pero en NavierStokes del viejo FsiPy el gradiente
    !       gradient_divided_by_density que uso esta definido
    !       como G^ab_i = (N^a , dN^a/dX_i), no deberian ser
    !       iguales ya que son la transpuesta con el signo
    !       cambiado? => en la practica no lo son..
    !
    gp_dshape_shape = 0.0_dp
    do ielem=1,nelem
        do igaus=1,ngaus
            do inode=1,nnode_elem
                do jnode=1,nnode_elem
                    do idime=1,ndime
                      gp_dshape_shape(ielem,igaus,inode,jnode,idime) = &
                        - gp_dshape_glo(ielem,igaus,inode,idime)       &
                        * gp_shape(igaus,jnode)
                    end do
                end do
            end do
        end do
    end do
end subroutine


subroutine compute_divergence_matrix()
    !
    ! Computes divergence matrix for all elements 
    !
    integer(4) :: ielem, igaus, inode, jnode, idime, jdime
    gp_shape_dshape = 0.0_dp
    do ielem=1,nelem
        do igaus=1,ngaus
            do inode=1,nnode_elem
                do jnode=1,nnode_elem
                    do idime=1,ndime
                      gp_shape_dshape(ielem,igaus,inode,jnode,idime) = &
                          gp_shape(igaus,inode)                        &
                        * gp_dshape_glo(ielem,igaus,jnode,idime)
                    end do
                end do
            end do
        end do
    end do
end subroutine


subroutine change_timestep(dt_in)
    !
    ! Modifies time step size
    !
    real(8), intent(in) :: dt_in
    dt = dt_in
end subroutine


subroutine assign_dimension_pointers( ndime )
    !
    ! Assigns pointers according to spacial dimensions
    !
    implicit none
    integer(4), intent(in) :: ndime

    if ( associated(identity) ) then
        deallocate( identity )
    end if
    allocate( identity(ndime,ndime) )

    if     (ndime == 2) then
        identity = I22
        matdet   => matdet22
        matinv   => matinv22
    elseif (ndime == 3) then
        identity = I33
        matdet   => matdet33
        matinv   => matinv33
    end if
end subroutine


pure function matdet22(M) result(det)
    !
    ! Performs a direct calculation of the determinant of a 2x2 matrix.
    !
    real(8), intent(in) :: M(:,:)
    real(8)             :: det
    ! Calculate the determinant of the matrix
    det = M(1,1)*M(2,2) - M(1,2)*M(2,1)
end function


pure function matdet33(M) result(det)
    !
    ! Performs a direct calculation of the determinant of a 3x3 matrix.
    !
    real(8), intent(in) :: M(:,:)
    real(8)             :: det
    ! Calculate the inverse determinant of the matrix
    det = M(1,1)*M(2,2)*M(3,3) - M(1,1)*M(2,3)*M(3,2)&
        - M(1,2)*M(2,1)*M(3,3) + M(1,2)*M(2,3)*M(3,1)&
        + M(1,3)*M(2,1)*M(3,2) - M(1,3)*M(2,2)*M(3,1)
end function


pure function matdet44(M) result(det)
    !
    ! Performs a direct calculation of the determinant of a 4x4 matrix.
    !
    real(8), intent(in) :: M(:,:)
    real(8)             :: det
    ! Calculate the inverse determinant of the matrix
    det = M(1,1)*(M(2,2)*(M(3,3)*M(4,4)-M(3,4)*M(4,3))+M(2,3)*(M(3,4)*M(4,2)-M(3,2)*M(4,4))+M(2,4)*(M(3,2)*M(4,3)-M(3,3)*M(4,2)))&
        - M(1,2)*(M(2,1)*(M(3,3)*M(4,4)-M(3,4)*M(4,3))+M(2,3)*(M(3,4)*M(4,1)-M(3,1)*M(4,4))+M(2,4)*(M(3,1)*M(4,3)-M(3,3)*M(4,1)))&
        + M(1,3)*(M(2,1)*(M(3,2)*M(4,4)-M(3,4)*M(4,2))+M(2,2)*(M(3,4)*M(4,1)-M(3,1)*M(4,4))+M(2,4)*(M(3,1)*M(4,2)-M(3,2)*M(4,1)))&
        - M(1,4)*(M(2,1)*(M(3,2)*M(4,3)-M(3,3)*M(4,2))+M(2,2)*(M(3,3)*M(4,1)-M(3,1)*M(4,3))+M(2,3)*(M(3,1)*M(4,2)-M(3,2)*M(4,1)))
end function


pure function matinv22(M) result(R)
    !
    ! Performs a direct calculation of the inverse of a 2x2 matrix.
    !
    real(8), intent(in) :: M(:,:)
    real(8)             :: R(size(M,1),size(M,1))
    real(8)             :: detinv
    ! Calculate the inverse determinant of the matrix
    detinv = 1._dp/matdet22(M)
    ! Calculate the inverse of the matrix
    R(1,1) = +detinv * M(2,2)
    R(2,1) = -detinv * M(2,1)
    R(1,2) = -detinv * M(1,2)
    R(2,2) = +detinv * M(1,1)
end function


pure function matinv33(M) result(R)
    !
    ! Performs a direct calculation of the inverse of a 3x3 matrix.
    !
    real(8), intent(in) :: M(:,:)
    real(8)             :: R(size(M,1),size(M,1))
    real(8)             :: detinv
    ! Calculate the inverse determinant of the matrix
    detinv = 1._dp/matdet33(M)
    ! Calculate the inverse of the matrix
    R(1,1) = +detinv * (M(2,2)*M(3,3) - M(2,3)*M(3,2))
    R(2,1) = -detinv * (M(2,1)*M(3,3) - M(2,3)*M(3,1))
    R(3,1) = +detinv * (M(2,1)*M(3,2) - M(2,2)*M(3,1))
    R(1,2) = -detinv * (M(1,2)*M(3,3) - M(1,3)*M(3,2))
    R(2,2) = +detinv * (M(1,1)*M(3,3) - M(1,3)*M(3,1))
    R(3,2) = -detinv * (M(1,1)*M(3,2) - M(1,2)*M(3,1))
    R(1,3) = +detinv * (M(1,2)*M(2,3) - M(1,3)*M(2,2))
    R(2,3) = -detinv * (M(1,1)*M(2,3) - M(1,3)*M(2,1))
    R(3,3) = +detinv * (M(1,1)*M(2,2) - M(1,2)*M(2,1))
end function


pure function matinv44(M) result(R)
    !
    ! Performs a direct calculation of the inverse of a 4x4 matrix.
    !
    real(8), intent(in) :: M(:,:)
    real(8)             :: R(size(M,1),size(M,1))
    real(8)             :: detinv
    ! Calculate the inverse determinant of the matrix
    detinv = 1._dp/matdet44(M)
    ! Calculate the inverse of the matrix
    R(1,1) = detinv*(M(2,2)*(M(3,3)*M(4,4)-M(3,4)*M(4,3))+M(2,3)*(M(3,4)*M(4,2)-M(3,2)*M(4,4))+M(2,4)*(M(3,2)*M(4,3)-M(3,3)*M(4,2)))
    R(2,1) = detinv*(M(2,1)*(M(3,4)*M(4,3)-M(3,3)*M(4,4))+M(2,3)*(M(3,1)*M(4,4)-M(3,4)*M(4,1))+M(2,4)*(M(3,3)*M(4,1)-M(3,1)*M(4,3)))
    R(3,1) = detinv*(M(2,1)*(M(3,2)*M(4,4)-M(3,4)*M(4,2))+M(2,2)*(M(3,4)*M(4,1)-M(3,1)*M(4,4))+M(2,4)*(M(3,1)*M(4,2)-M(3,2)*M(4,1)))
    R(4,1) = detinv*(M(2,1)*(M(3,3)*M(4,2)-M(3,2)*M(4,3))+M(2,2)*(M(3,1)*M(4,3)-M(3,3)*M(4,1))+M(2,3)*(M(3,2)*M(4,1)-M(3,1)*M(4,2)))
    R(1,2) = detinv*(M(1,2)*(M(3,4)*M(4,3)-M(3,3)*M(4,4))+M(1,3)*(M(3,2)*M(4,4)-M(3,4)*M(4,2))+M(1,4)*(M(3,3)*M(4,2)-M(3,2)*M(4,3)))
    R(2,2) = detinv*(M(1,1)*(M(3,3)*M(4,4)-M(3,4)*M(4,3))+M(1,3)*(M(3,4)*M(4,1)-M(3,1)*M(4,4))+M(1,4)*(M(3,1)*M(4,3)-M(3,3)*M(4,1)))
    R(3,2) = detinv*(M(1,1)*(M(3,4)*M(4,2)-M(3,2)*M(4,4))+M(1,2)*(M(3,1)*M(4,4)-M(3,4)*M(4,1))+M(1,4)*(M(3,2)*M(4,1)-M(3,1)*M(4,2)))
    R(4,2) = detinv*(M(1,1)*(M(3,2)*M(4,3)-M(3,3)*M(4,2))+M(1,2)*(M(3,3)*M(4,1)-M(3,1)*M(4,3))+M(1,3)*(M(3,1)*M(4,2)-M(3,2)*M(4,1)))
    R(1,3) = detinv*(M(1,2)*(M(2,3)*M(4,4)-M(2,4)*M(4,3))+M(1,3)*(M(2,4)*M(4,2)-M(2,2)*M(4,4))+M(1,4)*(M(2,2)*M(4,3)-M(2,3)*M(4,2)))
    R(2,3) = detinv*(M(1,1)*(M(2,4)*M(4,3)-M(2,3)*M(4,4))+M(1,3)*(M(2,1)*M(4,4)-M(2,4)*M(4,1))+M(1,4)*(M(2,3)*M(4,1)-M(2,1)*M(4,3)))
    R(3,3) = detinv*(M(1,1)*(M(2,2)*M(4,4)-M(2,4)*M(4,2))+M(1,2)*(M(2,4)*M(4,1)-M(2,1)*M(4,4))+M(1,4)*(M(2,1)*M(4,2)-M(2,2)*M(4,1)))
    R(4,3) = detinv*(M(1,1)*(M(2,3)*M(4,2)-M(2,2)*M(4,3))+M(1,2)*(M(2,1)*M(4,3)-M(2,3)*M(4,1))+M(1,3)*(M(2,2)*M(4,1)-M(2,1)*M(4,2)))
    R(1,4) = detinv*(M(1,2)*(M(2,4)*M(3,3)-M(2,3)*M(3,4))+M(1,3)*(M(2,2)*M(3,4)-M(2,4)*M(3,2))+M(1,4)*(M(2,3)*M(3,2)-M(2,2)*M(3,3)))
    R(2,4) = detinv*(M(1,1)*(M(2,3)*M(3,4)-M(2,4)*M(3,3))+M(1,3)*(M(2,4)*M(3,1)-M(2,1)*M(3,4))+M(1,4)*(M(2,1)*M(3,3)-M(2,3)*M(3,1)))
    R(3,4) = detinv*(M(1,1)*(M(2,4)*M(3,2)-M(2,2)*M(3,4))+M(1,2)*(M(2,1)*M(3,4)-M(2,4)*M(3,1))+M(1,4)*(M(2,2)*M(3,1)-M(2,1)*M(3,2)))
    R(4,4) = detinv*(M(1,1)*(M(2,2)*M(3,3)-M(2,3)*M(3,2))+M(1,2)*(M(2,3)*M(3,1)-M(2,1)*M(3,3))+M(1,3)*(M(2,1)*M(3,2)-M(2,2)*M(3,1)))
end function


pure function trace(M) result(r)
    !
    ! Computes trace of matrix
    !
    implicit none
    real(8),  dimension(:,:),     intent(in) :: M
    real(8)                                  :: r
    integer(4)                               :: ii
    r = 0.0_dp
    do ii = 1, size(M,1)
        r = r + M(ii,ii)
    enddo
end function trace


pure function outx11(va,vb) result(R)
    !
    ! Computes outer product between vectors
    !
    implicit none
    real(8),    dimension(:),   intent(in)       :: va
    real(8),    dimension(:),   intent(in)       :: vb
    real(8),    dimension(size(va,1),size(vb,1)) :: R
    integer(4)                                   :: i, j
    do j = 1, size(vb,1)
        do i = 1, size(va,1)
            R(i,j) = va(i)*vb(j)
        enddo
    enddo
end function outx11


pure function outx22(A,B) result(r)
    !
    ! Computes outer product between matrices
    !
    implicit none
    real(8),    dimension(:,:),   intent(in) :: A
    real(8),    dimension(:,:),   intent(in) :: B
    real(8)                                  :: r
    integer(4)                               :: i, j
    r = 0.0_dp
    do j = 1, size(A,1)
        do i = 1, size(A,2)
            r = r + A(i,j)*B(i,j)
        enddo
    enddo
end function outx22


end module
