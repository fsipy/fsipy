module FortranSolidImplicit
! ============================================================================ !
!                    FsiPy Fortran Implicit Solid Module                       !
! ============================================================================ !
    use types
    use FortranKernel

    implicit none

    type, extends(Equation), public :: ImplicitSolid
    contains
        procedure, pass(this), public :: compute_gauss_lhs_rhs => compute_gauss_lhs_rhs_implicit_solid
        procedure, pass(this), public :: compute_gauss_lhs     => compute_gauss_lhs_implicit_solid
        procedure, pass(this), public :: compute_gauss_rhs     => compute_gauss_rhs_implicit_solid
        procedure, pass(this), public :: compute_gauss_force   => compute_gauss_force_implicit_solid
        procedure, pass(this), public :: compute_natural_bcs   => compute_natural_bcs_implicit_solid
    end type
    !
    ! Global variables
    !
    public
    real(8), allocatable :: gp_rho(:,:)
    real(8), allocatable :: gp_young(:,:)
    real(8)              :: alpha
    real(8)              :: poisson
    real(8)              :: beta 
    real(8)              :: gama 

contains

subroutine dealloc()
    deallocate( l_elems )
    deallocate( gauss_weights )
    deallocate( gp_jacobians )
    deallocate( gp_inv_jacobians )
    deallocate( gp_det_jacobians )
    deallocate( gp_coords )
    deallocate( gp_shape )
    deallocate( gp_dshape )
    deallocate( gp_volume )
    deallocate( gp_dshape_glo )
    deallocate( gp_shape_shape )
    deallocate( gp_shape_dshape )
    deallocate( gp_dshape_shape )
    deallocate( gp_dshape_dshape )
    deallocate( gp_rho )
    deallocate( gp_young )
end subroutine


subroutine initialize(l_elems_in,          &
                      gauss_weights_in,    &
                      gp_jacobians_in,     &
                      gp_inv_jacobians_in, &
                      gp_det_jacobians_in, &
                      gp_coords_in,        &
                      gp_shape_in,         &
                      gp_dshape_in,        &
                      gp_volume_in,        &
                      gp_rho_in,           &
                      gp_young_in,         &
                      material_in,         &
                      dt_in,               &
                      alpha_in,            &
                      poisson_in,          &
                      beta_in,             &
                      gama_in,             &
                      nnode_in,            &
                      ndofs_total_in,      &
                      npast_in,            &
                      ndime_in,            &
                      nelem_in,            &
                      nnode_elem_in,       &
                      ngaus_in,            &
                      nbasis_in)
    integer(4),       intent(in) :: l_elems_in(nelem_in,nnode_elem_in)
    real(8),          intent(in) :: gauss_weights_in(ngaus_in)
    real(8),          intent(in) :: gp_jacobians_in(nelem_in,ngaus_in,ndime_in,ndime_in)
    real(8),          intent(in) :: gp_inv_jacobians_in(nelem_in,ngaus_in,ndime_in,ndime_in)
    real(8),          intent(in) :: gp_det_jacobians_in(nelem_in,ngaus_in)
    real(8),          intent(in) :: gp_coords_in(nelem_in,ngaus_in,ndime_in)
    real(8),          intent(in) :: gp_shape_in(ngaus_in,nbasis_in)
    real(8),          intent(in) :: gp_dshape_in(ngaus_in,nbasis_in,ndime_in)
    real(8),          intent(in) :: gp_volume_in(nelem_in,ngaus_in)
    real(8),          intent(in) :: gp_rho_in(nelem_in,ngaus_in)
    real(8),          intent(in) :: gp_young_in(nelem_in,ngaus_in)
    character(len=7), intent(in) :: material_in
    real(8),          intent(in) :: dt_in
    real(8),          intent(in) :: alpha_in
    real(8),          intent(in) :: poisson_in
    real(8),          intent(in) :: beta_in
    real(8),          intent(in) :: gama_in
    integer(4) :: nnode_in
    integer(4) :: ndofs_total_in
    integer(4) :: npast_in
    integer(4) :: ndime_in
    integer(4) :: nelem_in
    integer(4) :: nnode_elem_in
    integer(4) :: ngaus_in
    integer(4) :: nbasis_in

    call initialize_kernel(l_elems_in,          &
                           gauss_weights_in,    &
                           gp_jacobians_in,     &
                           gp_inv_jacobians_in, &
                           gp_det_jacobians_in, &
                           gp_coords_in,        &
                           gp_shape_in,         &
                           gp_dshape_in,        &
                           gp_volume_in,        &
                           material_in,         &
                           dt_in,               &
                           nnode_in,            &
                           ndofs_total_in,      &
                           npast_in,            &
                           ndime_in,            &
                           nelem_in,            &
                           nnode_elem_in,       &
                           ngaus_in,            &
                           nbasis_in)

    ! Allocate global arrays required in FortranSolidImplicit
    allocate( gp_shape_dshape(nelem,ngaus,nnode_elem,nnode_elem,ndime) )
    allocate( gp_dshape_shape(nelem,ngaus,nnode_elem,nnode_elem,ndime) )
    allocate( gp_dshape_dshape(nelem,ngaus,nnode_elem,nnode_elem)      )
    allocate( gp_young(nelem,ngaus)                                    )
    allocate( gp_rho(nelem,ngaus)                                      )

    ! Save FortranSolidImplicit's global parameters -> **DEFINE PASSING**
    gp_young = gp_young_in
    gp_rho   = gp_rho_in
    alpha    = alpha_in
    poisson  = poisson_in
    beta     = beta_in
    gama     = gama_in

    ! Calculate elemental matrices required in FortranSolidImplicit
    call compute_laplacian_matrix()
    call compute_gradient_matrix()
    call compute_divergence_matrix()
end subroutine


subroutine assemble_algebraic_system_from_python(lhs,               &
                                                 idlhs,             &
                                                 rhs,               &
                                                 equation_name,     &
                                                 variables,         &
                                                 source,            &
                                                 levelset,          &
                                                 flag_assemble_lhs, &
                                                 tcount,            &
                                                 ndofs_in,          &
                                                 nrows_in,          &
                                                 nnode_in,          &
                                                 ndofs_total_in,    &
                                                 npast_in,          &
                                                 lhs_lump)
    real(8),      intent(out) :: lhs(nrows_in)
    integer(4),   intent(out) :: idlhs(nrows_in, 4)
    real(8),      intent(out) :: rhs(ndofs_in*nnode_in)
    character(*), intent(in)  :: equation_name
    real(8),      intent(in)  :: variables(ndofs_total_in, nnode_in, npast_in)
    real(8),      intent(in)  :: source(ndofs_total_in, nnode_in)
    real(8),      intent(in)  :: levelset(nnode_in)
    logical,      intent(in)  :: flag_assemble_lhs
    integer(4),   intent(in)  :: tcount 
    integer(4),   intent(in)  :: nrows_in
    integer(4),   intent(in)  :: nnode_in
    integer(4),   intent(in)  :: ndofs_total_in
    integer(4),   intent(in)  :: ndofs_in
    integer(4),   intent(in)  :: npast_in
    real(8),      intent(out) :: lhs_lump(ndofs_in*nnode_in)    
    type(ImplicitSolid)       :: oimplicit_solid

    call oimplicit_solid % assemble_algebraic_system(lhs,               &
                                                     idlhs,             &
                                                     rhs,               &
                                                     variables,         &
                                                     source,            &
                                                     levelset,          &
                                                     flag_assemble_lhs, &
                                                     tcount,            &
                                                     ndofs_in,          &
                                                     lhs_lump)
end subroutine


subroutine assemble_force_from_python(force,          &
                                      equation_name,  &
                                      variables,      &
                                      source,         &
                                      ndofs_in,       &
                                      nrows_in,       &
                                      nnode_in,       &
                                      ndofs_total_in, &
                                      npast_in)
    real(8),      intent(out) :: force(ndofs_in*nnode_in)
    character(*), intent(in)  :: equation_name
    real(8),      intent(in)  :: variables(ndofs_total_in, nnode_in, npast_in)
    real(8),      intent(in)  :: source(ndofs_total_in, nnode_in)
    integer(4),   intent(in)  :: nrows_in
    integer(4),   intent(in)  :: nnode_in
    integer(4),   intent(in)  :: ndofs_total_in
    integer(4),   intent(in)  :: ndofs_in
    integer(4),   intent(in)  :: npast_in
    type(ImplicitSolid)       :: oimplicit_solid

    call oimplicit_solid % assemble_force(force,        &
                                          variables,    &
                                          source,       &
                                          ndofs_in)
end subroutine


subroutine get_mass_matrix(mass_matrix_out, &
                           idmass_out,      &
                           nelem_in,        &
                           nnode_elem_in)
    real(8),       intent(out) :: mass_matrix_out(nelem_in*nnode_elem_in*nnode_elem_in)
    integer(4),    intent(out) :: idmass_out(nelem_in*nnode_elem_in*nnode_elem_in, 2)
    integer(4),    intent(in)  :: nelem_in
    integer(4),    intent(in)  :: nnode_elem_in
    call compute_global_mass_matrix(mass_matrix_out, idmass_out)
end subroutine


subroutine compute_gauss_lhs_implicit_solid(this,           & 
                                            gplhs,          &
                                            elvar,          &
                                            elevelset,      &
                                            nnode_in,       &
                                            ndofs_total_in, &
                                            ndofs_in,       &
                                            npast_in,       &
                                            ielem,          &
                                            igaus)
  class(ImplicitSolid), intent(in)  :: this
  real(8),              intent(out) :: gplhs(ndofs_in, nnode_in, ndofs_in, nnode_in)
  real(8),              intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),              intent(in)  :: elevelset(nnode_in)
  integer(4),           intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),           intent(in)  :: ielem, igaus
end subroutine


subroutine compute_gauss_rhs_implicit_solid(this,           &
                                            gprhs,          &
                                            elvar,          &
                                            elsource,       &
                                            elevelset,      &
                                            nnode_in,       &
                                            ndofs_total_in, &
                                            ndofs_in,       &
                                            npast_in,       &
                                            ielem,          &
                                            igaus)
  class(ImplicitSolid), intent(in)  :: this
  real(8),              intent(out) :: gprhs(ndofs_in, nnode_in)
  real(8),              intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),              intent(in)  :: elsource(ndofs_total_in, nnode_in)
  real(8),              intent(in)  :: elevelset(nnode_in)
  integer(4),           intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),           intent(in)  :: ielem, igaus
end subroutine


subroutine compute_gauss_lhs_rhs_implicit_solid(this,           &
                                                gplhs,          &
                                                gprhs,          &
                                                elvar,          &
                                                elsource,       &
                                                elevelset,      &
                                                nnode_in,       &
                                                ndofs_total_in, &
                                                ndofs_in,       &
                                                npast_in,       &
                                                ielem,          &
                                                igaus)
  class(ImplicitSolid),                 intent(in)  :: this
  real(8),                              intent(out) :: gplhs(ndofs_in, nnode_in, ndofs_in, nnode_in)
  real(8),                              intent(out) :: gprhs(ndofs_in, nnode_in)
  real(8),                              intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                              intent(in)  :: elsource(ndofs_total_in, nnode_in)
  real(8),                              intent(in)  :: elevelset(nnode_in)
  integer(4),                           intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),                           intent(in)  :: ielem, igaus
  real(8), dimension(ndofs_in,nnode_in)             :: gpfint, gpfext, gpfine, gprayl
  real(8), dimension(ndime,nnode_in,npast)          :: eldispl, elveloc, elaccel
  real(8), dimension(ndime,nnode_in)                :: elmomso
  real(8), dimension(nnode_in,ndime)                :: dNdx
  real(8), dimension(ndime,ndime)                   :: H, HT, F, invF, E, P
  real(8), dimension(ndime,ndime,ndime,ndime)       :: dPdF
  real(8), dimension(nnode_in,nnode_in)             :: mass
  real(8)                                           :: detF, young, lmb, mu, W
  integer(4)                                        :: inode, jnode, knode
  integer(4)                                        :: idime, jdime, kdime, ldime

  ! -------------------------------------------------------------------------- !
  ! Load variables & parameters
  ! -------------------------------------------------------------------------- !

  ! Nodal values of variables
  eldispl = elvar(1:ndime,        :,:)
  elveloc = elvar(ndime+1:2*ndime,:,:)
  elaccel = elvar(2*ndime+1:,     :,:)
  ! Element nodal values of momentum source
  elmomso = elsource(1:ndime,:)
  ! Compute Lame coefficients
  young   = gp_young(ielem,igaus)
  lmb     = young * poisson / ( (1.0_dp + poisson) * (1.0 - 2.0*poisson) )
  mu      = 0.5_dp * young / (1.0_dp + poisson)

  ! -------------------------------------------------------------------------- !
  ! Kinematic operators
  ! -------------------------------------------------------------------------- !

  ! Cartesian derivatives (dN/dX)
  dNdx = gp_dshape_glo(ielem,igaus,:,:)
  ! Displacement gradient
  H = matmul(eldispl(:,:,iter_k), dNdx(:,:))
  ! Transpose of displacement gradient
  HT = transpose(H)
  ! Deformation gradient
  F = Identity + H
  ! Determinant of deformation gradient
  detF = matdet(F)
  ! Inverse of deformation gradient
  invF = matinv(F)
  ! Calculate Green-Lagrange strain
  E = 0.5_dp * (H + HT + matmul(HT,H))

  ! -------------------------------------------------------------------------- !
  ! Displacement field 
  ! -------------------------------------------------------------------------- !

  ! Compute nominal PK1 and dPK1/dF
  call eval_stress_model_numerical(material, ndime, lmb, mu, F, P, dPdF, W)

  ! Compute RHS contribution
  gpfint = 0.0_dp
  gpfext = 0.0_dp
  gpfine = 0.0_dp
  do inode=1,nnode_elem
    do idime=1,ndime
      do jdime=1,ndime
        gpfint(idime,inode) = gpfint(idime,inode) &
                            + dNdx(inode,jdime) &
                            * P(jdime,idime)
      end do
      gpfext(idime,inode) = gpfext(idime,inode) &
                          + gp_rho(ielem,igaus) &
                          * gp_shape(igaus,inode) &
                          * elmomso(idime,inode)
    end do
  end do

  ! Compute LHS contribution
  gplhs = 0.0_dp
  do inode=1,nnode_elem
    do idime=1,ndime
      do jnode=1,nnode_elem
        do kdime=1,ndime
          do jdime=1,ndime
            do ldime=1,ndime
              gplhs(idime,inode,kdime,jnode) = gplhs(idime,inode,kdime,jnode) &
                                             + dPdF(idime,jdime,kdime,ldime) &
                                             * dNdx(inode,jdime) &
                                             * dNdx(jnode,ldime)
            end do
          end do
        end do
      end do
    end do
  end do

  ! -------------------------------------------------------------------------- !
  ! Inertial contribution 
  ! -------------------------------------------------------------------------- !

  ! Compute mass matrix 
  mass = 0.0_dp
  do inode=1,nnode_elem
    do jnode=1,nnode_elem
      mass(inode,jnode) = gp_rho(ielem,igaus) &
                        * gp_shape_shape(igaus,inode,jnode)
    end do
  end do

  ! Compute intertial forces
  gpfine = 0.0_dp
  gprayl = 0.0_dp
  do inode=1,nnode_elem
    do idime=1,ndime
      do jnode=1,nnode_elem
        ! Compute intertial force
        gpfine(idime,inode) = gpfine(idime,inode) &
                            + mass(inode,jnode) &
                            * elaccel(idime,jnode,iter_k)
        ! Compute Rayleigh damping
        gprayl(idime,inode) = gprayl(idime,inode) &
                            + alpha &
                            * gp_shape_shape(igaus,inode,jnode) &
                            * elveloc(idime,jnode,iter_k)
      end do
    end do
  end do

  ! Add inertial contribution to RHS
  gprhs = gpfext - gpfint - gpfine - gprayl

  ! Add inertial contribution to LHS according to Beta-Newmark scheme
  do idime =1,ndime
    do inode = 1,nnode_elem
      do jnode = 1,nnode_elem
        gplhs(idime,inode,idime,jnode) = gplhs(idime,inode,idime,jnode) + (1.0_dp/(beta*dt**2)) * mass(inode,jnode)
      end do
    end do
  end do

end subroutine


subroutine compute_gauss_force_implicit_solid(this,           &
                                              gpforce,        &
                                              elvar,          &
                                              elsource,       &
                                              nnode_in,       &
                                              ndofs_total_in, &
                                              ndofs_in,       &
                                              npast_in,       &
                                              ielem,          &
                                              igaus)
  class(ImplicitSolid),                 intent(in)  :: this
  real(8),                              intent(out) :: gpforce(ndofs_in, nnode_in)
  real(8),                              intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                              intent(in)  :: elsource(ndofs_total_in, nnode_in)
  integer(4),                           intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),                           intent(in)  :: ielem, igaus
  real(8), dimension(ndofs_in,nnode_in)             :: gpfint, gpfext, gpfine
  real(8), dimension(ndime,nnode_in,npast)          :: eldispl, elveloc, elaccel
  real(8), dimension(ndime,nnode_in)                :: elmomso
  real(8), dimension(nnode_in,ndime)                :: dNdx
  real(8), dimension(ndime,ndime)                   :: H, HT, F, invF, E, P
  real(8), dimension(ndime,ndime,ndime,ndime)       :: dPdF
  real(8), dimension(nnode_in,nnode_in)             :: mass
  real(8)                                           :: detF, young, lmb, mu, W
  integer(4)                                        :: inode, jnode, knode
  integer(4)                                        :: idime, jdime, kdime

  ! -------------------------------------------------------------------------- !
  ! Load variables & parameters
  ! -------------------------------------------------------------------------- !

  ! Nodal values of variables
  eldispl = elvar(1:ndime,        :,:)
  elveloc = elvar(ndime+1:2*ndime,:,:)
  elaccel = elvar(2*ndime+1:,     :,:)
  ! Element nodal values of momentum source
  elmomso = elsource(1:ndime,:)
  ! Compute Lame coefficients
  young   = gp_young(ielem,igaus)
  lmb     = young * poisson / ( (1.0_dp + poisson) * (1.0 - 2.0*poisson) )
  mu      = 0.5_dp * young / (1.0_dp + poisson)

  ! -------------------------------------------------------------------------- !
  ! Kinematic operators
  ! -------------------------------------------------------------------------- !

  ! Cartesian derivatives (dN/dX)
  dNdx = gp_dshape_glo(ielem,igaus,:,:)
  ! Displacement gradient
  H = matmul(eldispl(:,:,iter_k), dNdx(:,:))
  ! Transpose of displacement gradient
  HT = transpose(H)
  ! Deformation gradient
  F = Identity + H
  ! Determinant of deformation gradient
  detF = matdet(F)
  ! Inverse of deformation gradient
  invF = matinv(F)
  ! Calculate Green-Lagrange strain
  E = 0.5_dp * (H + HT + matmul(HT,H))

  ! -------------------------------------------------------------------------- !
  ! Displacement field 
  ! -------------------------------------------------------------------------- !

  ! Compute nominal PK1 and dPK1/dF
  call eval_stress_model_numerical(material, ndime, lmb, mu, F, P, dPdF, W)

  ! Compute RHS contribution
  gpfint = 0.0_dp
  gpfext = 0.0_dp
  gpfine = 0.0_dp
  do inode=1,nnode_elem
    do idime=1,ndime
      do jdime=1,ndime
        gpfint(idime,inode) = gpfint(idime,inode) &
                            + dNdx(inode,jdime) &
                            * P(jdime,idime)
      end do
      gpfext(idime,inode) = gpfext(idime,inode) &
                          + gp_rho(ielem,igaus) &
                          * gp_shape(igaus,inode) &
                          * elmomso(idime,inode)
    end do
  end do

  ! -------------------------------------------------------------------------- !
  ! Inertial contribution 
  ! -------------------------------------------------------------------------- !

  ! ! Compute mass matrix 
  ! mass = 0.0_dp
  ! do inode=1,nnode_elem
  !   do jnode=1,nnode_elem
  !     mass(inode,jnode) = gp_rho(ielem,igaus) &
  !                       * gp_shape_shape(igaus,inode,jnode)
  !   end do
  ! end do

  ! ! Compute intertial force
  ! gpfine = 0.0_dp
  ! do inode=1,nnode_elem
  !   do idime=1,ndime
  !     do jnode=1,nnode_elem
  !       gpfine(idime,inode) = gpfine(idime,inode) &
  !                           + mass(inode,jnode) &
  !                           * elaccel(idime,jnode,iter_k)
  !     end do
  !   end do
  ! end do

  ! Add inertial contribution to RHS
  gpforce = gpfext - gpfint !- gpfine

end subroutine


subroutine compute_natural_bcs_implicit_solid(this,           &
                                              bcnat,          &
                                              source,         &
                                              nnode_in,       &
                                              ndofs_total_in, &
                                              ndofs_in)
    class(ImplicitSolid), intent(in)  :: this
    real(8),              intent(out) :: bcnat(ndofs_in, nnode_in)
    real(8),              intent(in)  :: source(ndofs_total_in, nnode_in)
    integer(4),           intent(in)  :: nnode_in, ndofs_total_in, ndofs_in
    bcnat(:,:) = 0.0_dp
end subroutine


subroutine eval_pk2_isolin(E, young, ndime_in, PK2)
  real(8),    intent(in)  :: E(ndime_in, ndime_in)
  real(8),    intent(in)  :: young
  integer(4), intent(in)  :: ndime_in
  real(8),    intent(out) :: PK2(ndime_in, ndime_in)
  real(8)                 :: lame1, lame2, trace_E
  integer(4)              :: idime
 
  ! Calculate Lame coefficients
  lame1 = poisson * young / ((1.0_dp+poisson)*(1.0_dp-2.0_dp*poisson))
  lame2 = 0.5_dp  * young /  (1.0_dp+poisson)

!  ! If 2D & in plane stress conditions => modify 1st Lame coefficient
!  if (ndime_in==2) then
!      lame1 = 2.0 * lame1 * lame2 / (lame1 + 2.0*lame2)
!  end if

  ! Calculate trace of Green-Lagrange strain
  trace_E = sum( (/ (E(idime,idime), idime=1, ndime_in) /) )

  ! Calculate PK2
  PK2 = lame1*trace_E*Identity + 2.0_dp*lame2*E
end subroutine 


subroutine eval_pk2_neohooke(F, FT, young, ndime_in, PK2)
  real(8),    intent(in)  :: F(ndime_in, ndime_in)
  real(8),    intent(in)  :: FT(ndime_in, ndime_in)
  real(8),    intent(in)  :: young
  integer(4), intent(in)  :: ndime_in
  real(8),    intent(out) :: PK2(ndime_in, ndime_in)
  real(8)                 :: Cinv(ndime_in, ndime_in)
  real(8)                 :: lame1, lame2, J

  ! Calculate the inversed of the right C-G tensor
  Cinv = matinv(matmul(FT,F))

  ! Calculate determinant of deformation gradient
  J = matdet(F)

  ! Calculate the Lame coefficients
  lame1 = poisson * young / ((1.0_dp+poisson)*(1.0_dp-2.0_dp*poisson))
  lame2 = 0.5_dp  * young /  (1.0_dp+poisson)

!  ! If 2D & in plane stress conditions => modify 1st Lame coefficient
!  if (ndime_in==2) then
!      lame1 = 2.0 * lame1 * lame2 / (lame1 + 2.0*lame2)
!  end if

  ! Calculate PK2
  PK2 = lame1/2.*(J**2-1.)*Cinv + lame2*(Identity-Cinv)
end subroutine 


subroutine eval_stress_model_numerical(material,nd,prop1,prop2,F,P,dPdF,W)
    ! --------------------------------------
    implicit none
    ! --------------------------------------
    character(len=7),                intent(in)  :: material
    integer(4),                      intent(in)  :: nd
    real(8),                         intent(in)  :: prop1
    real(8),                         intent(in)  :: prop2
    real(8),                         intent(in)  :: F(nd,nd)
    real(8),                         intent(out) :: P(nd,nd)
    real(8),                         intent(out) :: dPdF(nd,nd,nd,nd)
    real(8),                         intent(out) :: W
    real(8), dimension(nd,nd), target            :: F_per, F_rep
    real(8), dimension(nd,nd)                    :: P_per, P_rep
    real(8)                                      :: W_per
    integer(4)                                   :: i, j
    real(8), parameter                           :: eps = 1.0e-8_dp
    ! --------------------------------------

    ! Compute PK1 and Energy
    W = 0.0_dp
    P = get_stress_PK1(nd,prop1,prop2,F,W)
    ! Compute Numerical first elasticity tensor
    dPdF = 0.0_dp
    do i = 1, nd
        do j = 1, nd
            ! Compute forward perturbated deformation gradient
            F_per = F(:,:) + eps * outx(identity(:,i),identity(:,j))
            P_per(:,:) = get_stress_PK1(nd,prop1,prop2,F_per,W_per)
            ! Compute backward perturbated deformation gradient and Kirchoff
            F_rep = F(:,:) - eps * outx(identity(:,i),identity(:,j))
            P_rep(:,:) = get_stress_PK1(nd,prop1,prop2,F_rep,W_per)
            ! Compute algorithmic tangent approximation
            dPdF(:,:,i,j) = (P_per(:,:) - P_rep(:,:)) / (2.0_dp * eps)
        enddo
    enddo

    contains 

        function get_stress_PK1(nd,lmb,mu,F,W) result(P)
            ! Finite strain Sant Venant isotropic law
            ! --------------------------------------
            implicit none
            ! --------------------------------------
            integer(4),                      intent(in)  :: nd
            real(8),                         intent(in)  :: lmb 
            real(8),                         intent(in)  :: mu
            real(8),                         intent(in)  :: F(nd,nd)
            real(8), optional,            intent(inout)  :: W
            real(8), dimension(nd,nd)                    :: P
            real(8), dimension(nd,nd)                    :: C, E, S
            real(8)                                      :: E_tr
            integer(4)                                   :: ii, jj, kk
            ! --------------------------------------

            ! Green Lagrange strain tensor
            C = 0.0_dp
            do kk = 1, nd
                do jj = 1, nd
                    do ii = 1, nd
                        C(ii,jj) = C(ii,jj) + F(kk,ii) * F(kk,jj)
                    enddo
                enddo
            enddo
            E = 0.5_dp * (C - identity)
            E_tr = trace(E)

            ! Second Piola-Kirchoff stress tensor
            S = lmb * E_tr * identity + 2.0_dp * mu * E

            ! First Piola-Kirchoff stress
            P = 0.0_dp
            do kk = 1, nd
                do jj = 1, nd
                    do ii = 1, nd
                        P(ii,jj) = P(ii,jj) + F(ii,kk) * S(kk,jj)
                    enddo
                enddo
            enddo

            ! Strain energy
            W = 0.5_dp * lmb * E_tr**2 + mu * outx(E,E)
            ! --------------------------------------
 
            ! --------------------------------------
        end function get_stress_PK1

     ! --------------------------------------
end subroutine eval_stress_model_numerical


subroutine modify_timestep(dt_in)
    real(8), intent(in) :: dt_in
    call change_timestep(dt_in)
end subroutine


end module
