""" Post-processing module """

import numpy as np
import os
import glob
import pandas
import sys
import getopt
import vtk
from   mpi4py              import MPI

points_as_double = True
time_roundoff = 12

def save2vtk(outpath, casename, coords, elems, eltype, d_vars, varnames, tcount, time):
    
    # append datasets to single unstructured grid
    af = vtk.vtkAppendFilter()

    # read mesh
    ug = read_geometry(coords, elems, eltype)

    # number of nodes and dimensions of problem
    (nnode, ndime) = coords.shape

    # dict to classify fields according to rank of array
    field_types = {2:'vector',1:'scalar'}

    # loop over selected variables
    for varname in varnames:
        array      = d_vars[varname]
        rank       = np.ndim(array)
        field_type = field_types[rank]

        # is vector field
        if (field_type == 'vector') and (ndime < 3):
            array = np.vstack((array,np.zeros((3-ndime,nnode),float)))

        # convert np array to vtk
        vtkarray = numpyarray2vtkarray(array, varname, 'double')
        ug.GetPointData().AddArray( vtkarray )

        if field_type=='vector':
            ug.GetPointData().SetActiveVectors( varname )
 
    # add data to unstructured grid & update
    af.AddInputData(ug)
    af.Update()

    # write to .vtu file
    wr = vtk.vtkXMLUnstructuredGridWriter()
    isubdomain = MPI.COMM_WORLD.Get_rank()
    wr.SetFileName( os.path.join(outpath, f"{casename}_{isubdomain}_{tcount:08d}.vtu" ) )
    wr.SetInputData( af.GetOutput() )
    wr.SetDataModeToAppended()
    wr.EncodeAppendedDataOff()
    wr.Write()

    # Save timestep value
    if isubdomain == 0:
        if tcount == 0:
            with open(os.path.join(outpath,f'{casename}.res'),'w') as f:
                f.write( 'Timestep Time\n' )
        with open(os.path.join(outpath,f'{casename}.res'),'a') as f:
            f.write( '%08d %1.8e\n' % (tcount,time) )
 


def read_geometry(coords, elems, eltype):

    (nelem, nnode_elem) = elems.shape
    (nnode, ndime) = coords.shape

    if ndime < 3: coords = np.hstack((coords,np.zeros((nnode,1),float)))

    eltype_fsipy2vtk = {2:{'Name': vtk.VTK_TRIANGLE, 'Vertices':3},
                        3:{'Name': vtk.VTK_QUAD,     'Vertices':4}}

    pts = vtk.vtkPoints()

    if points_as_double:
        pts.SetDataTypeToDouble()

    pts.SetNumberOfPoints(nnode)
    for ptid, xyz in enumerate(coords):
        pts.SetPoint(ptid, xyz)

    if isinstance(eltype,(int,np.integer)):
        eltype = [eltype]*nelem

    element_types = [eltype_fsipy2vtk[t]['Name'] for t in eltype]
    element_nvert = [eltype_fsipy2vtk[t]['Vertices'] for t in eltype]

    cells = vtk.vtkCellArray()
    for cellid in range(nelem):
        cellpts = elems[cellid,:]
        nnodes = element_nvert[cellid]
        cells.InsertNextCell(nnodes)
        for i in range(nnodes):
            cells.InsertCellPoint(cellpts[i])

    ug = vtk.vtkUnstructuredGrid()
    ug.SetPoints(pts)
    ug.SetCells(element_types, cells)

    return ug



def numpyarray2vtkarray(data, name, arraytype):
    #data is 1d (npoints) or 2d (npoints x n) numpy.ndarray
    #name to name the array

    data = data.T

    if( data.shape[0] == 0 ):
        raise ValueError("numpyarray2vtkarray: empty data array")

    if arraytype == 'float':
        a = vtk.vtkFloatArray()
    elif arraytype == 'double':
        a = vtk.vtkDoubleArray()
    elif arraytype == 'short':
        a = vtk.vtkShortArray()
    elif arraytype == 'int64':
        a = vtk.vtkTypeInt64Array()
    else:
        raise ValueError("numpyarray2vtkarray: unknown array type")
    
    if len(data.shape)==1: #1d
        a.SetNumberOfComponents(1)
    else:
        a.SetNumberOfComponents(data.shape[1])

    a.SetNumberOfTuples(data.shape[0])
    a.SetName(name)

    if len(data.shape)==1:
        for i, values in enumerate(data):
            a.SetTuple(i, [values])
    else:
        for i, values in enumerate(data):
            a.SetTuple(i, values)

    return a



def extract_vars_vtu(vtufilename):
    
    vars = []
    extract = False
    pd = False
    cd = False
    with open(vtufilename, 'rb') as f:
        while 1>0:
            line = f.readline()

            if b"<PointData" in line:
                extract = True
            elif b"<CellData" in line:
                extract = True

            if pd and cd:
                break

            if extract:
                vars.append(line)

            if b"</PointData>" in line:
                pd = True
                extract = False
            elif b"</CellData>" in line:
                cd = True
                extract = False

    return vars    



def build_pvd_file(fpath):

    outpath  = os.path.dirname( fpath )
    casename = os.path.splitext( os.path.basename( fpath ) )[0]

    isubdomain = MPI.COMM_WORLD.Get_rank()

    basename = casename+'_'+str(isubdomain)

    print('\nSaving "'+basename+'" vtu files to pvd..')

    # write .pvd file with timestep data    
    with open(os.path.join(outpath,f'{basename}.pvd'),'w') as ff:

        ff.write('<VTKFile type="Collection" version="0.1" byte_order="LittleEndian">\n')
        ff.write('<Collection>\n')


        # read problem timesteps and write to dictionary
        with open(os.path.join(outpath,f'{casename}.res'),'r') as ft:
            lines = ft.readlines()
            if lines[0][0]=='T': lines=lines[1:]
        d_tsteps = {}
        for line in lines:
            line   = line.split()
            tcount = int(line[0])
            time   = float(line[1])
            d_tsteps[tcount] = time


        # read .vtu files
        lfiles = glob.glob(os.path.join(outpath,basename+"*.vtu"))
        ndump = np.ceil(float(len(lfiles))/10.)

        for ifile, filenamevtu in enumerate(lfiles):
            if np.mod(ifile,ndump)==0: print('    isubdomain '+str(isubdomain)+': '+str(ifile/ndump*10)+'%')

            # write timestep data
            tcount   = int(filenamevtu[-12:-4])
            time     = d_tsteps[tcount]
            filename = f"{basename}_{tcount:08d}.pvtu"
            ff.write(f'   <DataSet timestep="{time}" file="{filename}"/>\n')

            # write pvtu file
            with open(os.path.join(outpath,filename),'w') as ff_pvtu:
                ff_pvtu.write( '<?xml version="1.0"?>\n' )
                ff_pvtu.write( '<VTKFile type="PUnstructuredGrid" version="0.1" byte_order="LittleEndian" header_type="UInt32" compressor="vtkZLibDataCompressor">\n') 
                ff_pvtu.write( '<PUnstructuredGrid GhostLevel="0">\n' )
    
                # these vars must be in te same order as in vtu
                allvars = extract_vars_vtu( os.path.join(outpath,f"{basename}_{tcount:08d}.vtu") )
                for line in allvars:
                    ff_pvtu.write(line.decode('utf-8').replace('PointData','PPointData').replace('CellData','PCellData'))
                    
                ff_pvtu.write( '<PPoints>\n')
    
                if points_as_double:
                    ff_pvtu.write( '    <PDataArray type="Float64" Name="Points" NumberOfComponents="3"/>\n')
                else:
                    ff_pvtu.write( '    <PDataArray type="Float32" Name="Points" NumberOfComponents="3"/>\n')
    
                ff_pvtu.write( '</PPoints>\n')
                ff_pvtu.write( f'<Piece Source="{basename}_{tcount:08d}.vtu"/>\n')
                ff_pvtu.write( '</PUnstructuredGrid>\n')
                ff_pvtu.write( '</VTKFile>\n')

        ff.write('</Collection>\n')
        ff.write('</VTKFile>\n')


        print('    100.0%\nsubdomain '+str(isubdomain)+' completed!\n')



if __name__ == "__main__":
    """ Main code executed when pprocess.py is called directly as script. """
    
    # read path
    opts, args = getopt.getopt(sys.argv[1:],"hp:",["fpath="])
    fpath = args[0]
    
    # build .pvd file indexed by real time
    build_pvd_file(fpath)
