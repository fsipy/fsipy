"""
    FsiPy Python Coupling Kernel
    ============================
"""

# External libraries
import os
import sys
import time
import copy
import vtk 
import math
import decimal
import numpy                as np
import numpy.linalg         as la
import scipy                as sp
import scipy.sparse         as ssp
import scipy.sparse.linalg  as ssla
import time                 as ti
import pickle               as pkl
from   abc                  import ABC, abstractmethod
from   mpi4py               import MPI

# Internal libraries
import ddmpy
import aux                  as aux
import pprocess             as pp
import FortranFluid         as FortranFluid
import FortranSolid         as FortranSolid
import FortranSolidImplicit as FortranSolidImplicit
from   PyParameters         import *
from   PyTiming             import *

# Settings
np.seterr(divide='ignore', invalid='ignore')


# ============================================================================ #
#                             Coupling Class                                   #
# ============================================================================ #
class Coupling(Timing):
    """ Problem coupling class. """

    # Initialization
    def __init__(self, d_params, l_oproblems, l_problem_names, rank):

        # Attributes
        self._d_params        = d_params
        self._l_dcouplings    = {}
        self._l_oproblems     = l_oproblems
        self._nproblems       = len(l_oproblems)
        self._l_problem_names = l_problem_names
        self._interp_method   = d_params['interp_method']
        self._nlevels_interp  = d_params['nlevels_interp']
        self._interp_order    = d_params['interp_order']
        self._support_size    = d_params['support_size']        
        self._tcount          = 0
        self._time            = self._d_params['tinitial']
        self._tfinal          = self._d_params['tfinal']
        self._ntmax           = self._d_params['ntmax']
        self._ndime           = self._d_params['ndime']
        self._d_params['dt0'] = self._d_params['dt']
        self._niter           = 0
        self._error           = 1.0e13
        self._ipass           = 0


        # OJO - ATENCION: ESTO ESTA AQUI PUESTO EN CODIGO, DEBE ESTAR PUESTO COMO PARAMETRO  
        self._bigger_patch   = True


    @Timing()
    def run_simulation(self):
        """ Main routine: runs simulation. """

        # Compute timestep size 
        self.compute_timestep_size()
        # Set initial conditions, assemble algebraic systems and impose BCs
        self.initialize_problems()
        # Restart
        self.restart()
        # Post-process initial conditions
        self.post_process( first_output=True )
        # Time loop
        while (self._tcount < self._ntmax) and (self._time < self._tfinal):
            self.advance_timestep()
        self.deallocate()


    @Timing()
    def advance_timestep(self):
        # Initialize iterative counter and error
        self._niter = 0
        self._error = 1.0e13

        # Update time counters
        self.update_time()

        # Begin coupled problem timestep
        self.begin_timestep()

        # Outer iterative loop
        while self.eval_iteration_condition():
            self._niter += 1
#            print('        Coupling Iteration: ',self._niter)

            # Begin coupling iteration
            self.begin_iteration()

            # Loop over sub-problems
            for oproblem in self._l_oproblems:

                # Loop over these couplings
                for icoupling in oproblem._l_icouplings_as_target:
                    self.begin_problem( icoupling )

                # Advance timestep for this problem
                oproblem.advance_timestep()

                # Loop over these couplings
                for icoupling in oproblem._l_icouplings_as_source:
                    self.end_problem( icoupling )

            # End iteration
            self.end_iteration()

        # End timestep
        self.end_timestep()
        # Postprocess coupled problem
        self.post_process()



    def eval_iteration_condition(self):
        return (self._niter < self._d_params['nitermax']) and \
               (self._error > self._d_params['tolerance'])



    def initialize_problems(self):
        """ Sets initial conditions, assembles algebraic systemsand imposes BCs
            for each Equation of each Problem in Coupling.
        """

        # Initialize separate problems
        for iproblem, oproblem in enumerate(self._l_oproblems):
            problem_name = self._l_problem_names[iproblem]
            oproblem.activate_in_coupling( self._l_dcouplings )
            oproblem.set_dof_fixities()
            oproblem.set_initial_conditions()
            oproblem.update_boundary_conditions()

            for iequation in range(oproblem._nequations):
                if (problem_name != "rigidsolid"):
                    oproblem.assemble_algebraic_system(iequation)
            if (problem_name == self._background):
                oproblem.set_number_of_neighbors( self._d_params['nlevels_interp'] )
                oproblem.find_node_neighbors_of_elements()

        # Provide each problem with information on coupling
        for icoupling, dcoupling in enumerate(self._l_dcouplings):
            osource = self.get_problem_from_coupling(icoupling, 'source')
            otarget = self.get_problem_from_coupling(icoupling, 'target')
            osource.setup_source( dcoupling )
            otarget.setup_target( dcoupling )

        # Update BCs on separate problems 
        for oproblem in self._l_oproblems:
            for iequation in range(oproblem._nequations):
                if (problem_name != "rigidsolid"):                
                    oproblem.impose_boundary_conditions_on_algebraic_system(iequation)


    def get_problem_by_code(self, problem_code):
        """ Return Problem object for a given problem code as defined in coupling.
        """
        return self._l_oproblems[ self._l_problem_codes.index(problem_code) ]



    def get_problem_from_coupling(self, icoupling, role):
        """ Return Problem object for a given role ("source" or "target")
            in a given coupling.
        """
        problem_code = self._l_dcouplings[icoupling][role]
        return self.get_problem_by_code( problem_code )



    def update_time(self):
        """ Updates timestep counter and real time. """

        # Update time counters for each individual Problem
        for oproblem in self._l_oproblems:
            oproblem.update_time( flag_external_call=True )

        # Update timestep
        self._tcount += 1
        self._time   += self._d_params['dt']

        # Define max timestep and printing cadency
        if self._tcount == 1:
            self._ntmax = int(np.floor(min([self._ntmax,self._d_params['tfinal']/self._d_params['dt']])))
            self._d_params['ntmax'] = self._ntmax
            self._nprint = int(self._ntmax/100)
        # Print timestep
        if (self._ntmax <= 101) or (np.mod(self._tcount, self._nprint) == 0):
            if MPI.COMM_WORLD.Get_rank() == 0:
                #print('Timestep ',self._tcount,' Completed ',round(self._tcount/self._ntmax*100,1),'%')
                print("{:<18} {:<10} {:<8}".format('Beginning timestep',
                                                               str(self._tcount),
                                                               '('+str(round(self._tcount/(self._ntmax)*100,1))+'%)'))



    def compute_timestep_size(self):
        """ Computes timestep sizes for problems in coupling.
        """

        # Compute differential timesteps
        if self._d_params['flag_diff_timesteps']:
            if self._nproblems > 2:
                print('Error! Differential timestepping not implemented for nproblems>2')
                exit()
            else:
                l_dts = [self._l_oproblems[i]._d_params['dt'] for i in [0,1]]
                dtmin0         = min(l_dts)
                dtmax          = max(l_dts)
                imin           = l_dts.index(dtmin0)
                imax           = l_dts.index(dtmax)
                ntime_rounded0 = dtmax / dtmin0
                ntime_rounded  = np.ceil(ntime_rounded0)
                ntime_coupling = int(ntime_rounded)
                dtmin          = dtmax / ntime_rounded
                if np.abs(ntime_rounded - ntime_rounded0) > 1e-2:
                    print('\nMax timestep is not a multiple of min timestep!')
                    print('=> min timestep modified from '+str(dtmin0)+' to '+str(dtmin))
                    print('   max timestep   = '+str(dtmax))
                    print('   ntime_coupling = '+str(ntime_coupling))
                self._l_oproblems[imin]._d_params['dt'] = dtmin
                self._d_params['dt'] = dtmax

        # Consider min timestep as global timestep
        else:
            ntime_coupling = 1
            dtmin = 1.0e10
            for oproblem in self._l_oproblems:
                dtmin = min([dtmin, oproblem._d_params['dt']])
            self._d_params['dt'] = dtmin

        self._d_params['ntime_coupling'] = ntime_coupling



    def update_time_variables(self):
        for i, oproblem in enumerate(self._l_oproblems):
            oproblem.update_time_variables()



    def post_process(self, first_output=False):
        """ Post-processes all coupled problems. """

        # Avoid 1st data dump if loading from restart
        if first_output and self._d_params['flag_load_restart']: return

        for i, oproblem in enumerate(self._l_oproblems):
            oproblem.post_process( flag_external_call=True )

        self.save_timestep_data()

        self.save_state()



    def save_timestep_data(self):
        """ Saves Coupling timestep data to file:

            Timestep | Time | Number of Coupling Iterations | Error

        """
        pathin   = self._d_params['path_inputs'] 
        casename = self._d_params['casename']
        pathouts = self._d_params['path_outs']
        filename = os.path.join(pathin, pathouts, casename+'.res')

        # Write header
        if self._tcount == 0:
            with open(filename, 'w') as f:
                f.write( 'Timestep Time           Niter Error\n' )

        # Write timestep data
        with open(filename, 'a') as f:
            f.write( '%08d %1.8e %05d %1.8e\n' \
                 % (self._tcount,self._time,self._niter,self._error) )



    def find_wet_elements(self, source_name, l_target_points=[]):
        """ Finds wet elements for given list of target points.

            Inputs:
            -------
                source_name: str 
                    source problem name
                l_target_points: np-array of dims (nnode,ndime)
                    list of points in space that define wet elements.
            Outputs:
            --------
                l_wet_elems: list
                    list of wet elements for given list of points in space.
        """

        osource = self.get_problem(source_name) 
        return osource.find_wet_elements( l_target_points )



    def calculate_interp_matrix(self, source_name, target_name, flag_return_spread=False):
        """ Assembles interpolation and spreading matrices. """

        # loads source/target problems
        osource = self.get_problem(source_name)
        otarget = self.get_problem(target_name)

        # get number of levels of neighbooring nodes for high-order interpolation 
        nlevels = 0
        is_fem     = (self._interp_method.upper()[0:3] == 'FEM')
        is_kriging = (self._interp_method.upper()[0:5] == 'KRIGI')
        is_rkpm    = (self._interp_method.upper()[0:4] == 'RKPM')
        if is_kriging or is_rkpm: nlevels = self._nlevels_interp

        # loads target points (all nodes of target mesh)
        l_target_points = otarget._o_mesh._l_node_coords_ale

        # initializes arrays
        coords_target = np.zeros(3,float)
        nrows = otarget._nnode
        ncols = osource._nnode
        rows = []
        cols = []
        interp_matrix = []
        l_wet_elems   = []
        l_found_nodes = []

        # Set Gmsh model to current
        sorter = np.argsort(osource._o_mesh._l_ielems_gmsh)

        # Build list of subdoms per node
        comm = MPI.COMM_WORLD
        l_subdoms_per_node = int(1e9)*np.ones(otarget._nnode, int)

        # loop nodes of target
        for inode_target in range(otarget._nnode):

            # extracts coordinates of node
            coords_target[0:self._ndime] = l_target_points[inode_target,:]
            # returns list of element(s) containing point (with Gmsh global element indxing)
            ielem_gmsh = osource._o_mesh.getElementByCoordinates(coords_target[0],
                                                                 coords_target[1],
                                                                 coords_target[2],
                                                                 dim=osource._ndime)

            # if cannot find element => continue loop
            if ielem_gmsh == None: continue

            # Input rank into list of subdoms per node
            l_subdoms_per_node[inode_target] = comm.Get_rank()

            # convert indices to FsiPy element indexing (starts at 0 for each elem type)
            ielem = int(ielem_gmsh - osource._o_mesh._l_ielems_gmsh[0])

            # appends element(s) to global list
            l_found_nodes.append(inode_target)
            l_wet_elems.append(ielem)

        # Keep only min subdomn
        if (comm.Get_size() > 1):
            buff = np.zeros(otarget._nnode, int)
            comm.Allreduce(l_subdoms_per_node, buff, op=MPI.MIN)
            l_subdoms_per_node = buff

        if any(l_subdoms_per_node > 1e8) and (self._bigger_patch == False):
            print('Error! Could not find wet element for node ',inode_target)
            exit()

        # Loop over found nodes
        for i, inode_target in enumerate(l_found_nodes):

            if (l_subdoms_per_node[inode_target] != comm.Get_rank()): continue

            ielem = l_wet_elems[i]
            # finds up to "nevel"th neighbooring nodes
            coords_target[0:self._ndime] = l_target_points[inode_target,:]
            # # use already computed nearest neighboors
            # l_inodes_source = osource._l_neighbors[ielem]
            # compute nearest neighboors at each time-step
            l_inodes_source = osource._o_mesh._l_elems[ielem,:]
            if nlevels > 0:
                l_inodes_tmp    = []
                for ilevel in range(nlevels):
                    for i in l_inodes_source:
                         l_inodes_tmp.extend(osource._o_mesh._l_inodes4nodes[i])
                    l_inodes_source = l_inodes_tmp.copy()
                l_inodes_source = list(set(l_inodes_source))
            # computes interp weights using specific method
            l_interp_weights = self.calculate_interp_weights(source_name,
                                                             target_name,
                                                             ielem,
                                                             l_inodes_source, 
                                                             coords_target)
            npoints_source = len(l_interp_weights)
            # appends to interp matrix
            rows.extend([inode_target]*npoints_source)
            cols.extend(l_inodes_source)
            interp_matrix.extend(l_interp_weights)

        # converts interp matrix to sparse format
        self._interp_matrix = ssp.csr_matrix((interp_matrix, (rows,cols)), shape=(nrows,ncols))
        self._rows = rows
        self._cols = cols



    def calculate_spread_matrix(self, source_name, target_name):
        """ Computes spreading matrix from interpolation matrix.

            Note: here "source" refers to the source in the interpolation
                  operation which during the spreading operation actually is
                  the target. The converse applies to the "target", it is the
                  source during the spreading operation.

            Compute spreading matrix as:

                    S = ml^-1 * J^t
            where:
                    ml^-1 : inverse of lumped mass matrix of source
                    J^t   : transpose of interpolation matrix
            
            we're assuming that the array on which the spread operator will be
            applied has already been integrated (multiplied by the mass matrix)
            in the target. This is, if "F" are the nodal coefficients of the
            forces in the target problem, and "f" are the nodal coeffs. in the
            source problem, then:
            
                    f = S * M * F
                      = ml^-1 * J^t * M * F
                      = S' * F
            
            where S' would be the spread operator as defined in Griffiths & Luo
            2017 and which conserves energy in the Lagrangian-Eulerian interaction:
            
                    (F,U)_X = (f,u)_x
        """

        # loads source/target problems
        osource = self.get_problem(source_name)
        otarget = self.get_problem(target_name)

        # Compute spreading matrix
        self._spread_matrix = self._interp_matrix.T



    def calculate_level_set(self, background_name, patch_name, flag_rowsum=False):
        """ Calculates level-set that indicates overlapping region, with
            values ranging between 0 (non-overlapping) and 1 (overlapping).
        """

        # loads source/target problems
        oback  = self.get_problem( background_name )
        opatch = self.get_problem( patch_name )

        # Compute level-set using old method: sum rows of interp matrix
        if flag_rowsum:
            oback._level_set  = self._interp_matrix.sum(axis=0).A1
            oback._level_set /= np.amax(oback._level_set)

        # Compute level-set from Poisson equation
        else:
            # Assemble laplacian matrix
            if not hasattr(oback, '_lapla'):
                oback.assemble_laplacian_matrix()

            # Assemble RHS of Poisson equation
            rhs = self.assemble_RHS_of_poisson_equation(oback, opatch)

            ## Solve Poisson equation
            oback._level_set = ssp.linalg.spsolve(oback._lapla, rhs)

            ## Normalize level set
            max_amplitude = np.amax( oback._level_set )
            if max_amplitude > 1e-13:
                oback._level_set /= max_amplitude



    def assemble_RHS_of_poisson_equation(self, oback, opatch):
        """ TODO: Set shared phys bounds for Poisson equation. """

        ## Compute source vectorial function
        source = opatch.calculate_levelset_source(self._interp_matrix, self._rows, self._cols)

        ## Compute divergence of source function
        rhs = oback.calculate_levelset_rhs(source, self._interp_method)

        ## Set Dirichlet BCs on RHS
        # Set fluid boundaries to 0
        for inode in oback._o_mesh._l_inodes_boundary:
            rhs[inode] = 0.0

        # Set boundaries shared with solid to 0.5
        #rhs = self.impose_value_at_shared_boundaries(ofluid, osolid, rhs, 0.5)

        return rhs


    def check_force_conservation(self, osource, otarget, spread_matrix):
        """ Checks force conservation in Lagrangian-Eulerian interaction. """

        # In target:
        f  = np.ones(otarget._nnode,float)
        f_tot = np.sum( f )

        # In source:
        fp      = spread_matrix.dot( f )
        fp_tot  = self.integrate_variable_in_volume(osource, fp)
        mfp_tot = np.sum( osource._mass_matrix @ fp )

        # Compare
        print('\n-----------------------------------------')
        print('int(f)   / int(F) =',fp_tot  / f_tot)
        print('sum(m*f) / int(F) =',mfp_tot / f_tot)
        print('-----------------------------------------\n')



    def integrate_variable_in_volume(self, oproblem, f):
        F = 0.0
        for ielem in range(oproblem._nelem):
            for igaus in range(oproblem._o_mesh._ngaus):
                for inode in range(oproblem._o_mesh._nnode_elem):
                    inode_glo = oproblem._o_mesh._l_elems[ielem,inode]
                    F += f[inode_glo] \
                       * oproblem._o_mesh._gp_shape[igaus,inode] \
                       * oproblem._o_mesh._gp_volume[ielem,igaus]
        return F

    

    def calculate_interp_weights(self,
                                 source_name,
                                 target_name,
                                 ielem_source,
                                 l_inodes_source,
                                 coords_target):
        """ Computes interpolation weights for transmission matrices. """


        # Load coordinates
        ndime         = self._ndime
        coords_target = coords_target[0:ndime]
        o_mesh        = self.get_problem(source_name)._o_mesh
        l_coords      = o_mesh._l_node_coords[l_inodes_source,:]
        interp_method = self._interp_method.upper()

        # Finite Element Interpolation
        if (interp_method[0:3] == 'FEM'):
            l_interp_weights = o_mesh.calculate_interp_weights_fem(ndime,
                                                                   ielem_source,
                                                                   coords_target)
        # Smooth Dirac Delta Interpolation
        elif (interp_method[0:5] == 'DELTA'):
            l_interp_weights = self.calculate_interp_weights_delta(ndime,
                                                                   coords_target,
                                                                   l_coords)
        # Kriging Interpolation
        elif (interp_method[0:5] == 'KRIGI'):
            l_interp_weights = self.calculate_interp_weights_kriging(ndime,
                                                                     coords_target,
                                                                     l_coords)
        # RKPM Interpolation
        elif (interp_method[0:4] == 'RKPM'):
            l_interp_weights = self.calculate_interp_weights_rkpm(ndime,
                                                                  coords_target,
                                                                  l_coords)
        return l_interp_weights



    def calculate_interp_weights_delta(self,
                                       source_name,
                                       target_name,
                                       ielem_source,
                                       l_inodes_source,
                                       coords_target):
        """ Computes interpolation weights for Smooth Delta Dirac Interpolation. """

        print('Implement delta interpolation!')
        return None



    def calculate_interp_weights_kriging(self,
                                         ndime,
                                         coords_target,
                                         l_coords):
        """ Computes interpolation weights for Kriging interpolation.
        """

        # 1. Determine interpolation according to the number of data.
        ndata = len(l_coords)
        nequations = ndata + 1
        if (ndime == 2):
            if (ndata >= 3) and (self._interp_order > 0):
                nequations = ndata+ndime+1 # Almost linear
            if (ndata > 6) and (self._interp_order > 1):
                nequations = ndata+ndime+4 # Almost quadratic
        elif (ndime == 3):
            if (ndata >= 4) and (self._interp_order > 0):
                nequations = ndata+ndime+1 # Almost linear
            if (ndata > 10) and (self._interp_order > 1):
                nequations = ndata+ndime+7 # Almost quadratic 

        # 2. Assembly the upper side of the covariance matrix
        covariance_matrix = np.zeros((nequations, nequations),float)
        aa = np.tile(               l_coords[np.newaxis],                (ndata,1,1) )
        bb = np.tile( np.transpose( l_coords[np.newaxis], axes=(1,0,2)), (1,ndata,1) )
        covariance_matrix[0:ndata,0:ndata] = la.norm(aa - bb, axis=2)
        covariance_matrix[np.tril_indices(ndata)] = 0.0

        # 3. Assembly the upper side of the mean value basis
        covariance_matrix[:ndata,ndata] = 1.0
        if (nequations > ndata + 1):  # Linear basis
            covariance_matrix[:ndata,ndata+1]         = l_coords[:ndata,0]
            covariance_matrix[:ndata,ndata+2]         = l_coords[:ndata,1]
            covariance_matrix[:ndata,ndata+ndime]     = l_coords[:ndata,ndime - 1]
        if (nequations == ndata + ndime + 4):  # Add two dimensional quadratic basis
            covariance_matrix[:ndata,ndata+ndime + 1] = l_coords[:ndata,0] * l_coords[:ndata,0]
            covariance_matrix[:ndata,ndata+ndime + 2] = l_coords[:ndata,1] * l_coords[:ndata,1]
            covariance_matrix[:ndata,ndata+ndime + 3] = l_coords[:ndata,0] * l_coords[:ndata,1]
        if (nequations == ndata + ndime + 7):  # Add three dimensional quadratic basis
            covariance_matrix[:ndata,ndata+ndime + 1] = l_coords[:ndata,0] * l_coords[:ndata,0]
            covariance_matrix[:ndata,ndata+ndime + 2] = l_coords[:ndata,1] * l_coords[:ndata,1]
            covariance_matrix[:ndata,ndata+ndime + 3] = l_coords[:ndata,2] * l_coords[:ndata,2]
            covariance_matrix[:ndata,ndata+ndime + 4] = l_coords[:ndata,0] * l_coords[:ndata,1]
            covariance_matrix[:ndata,ndata+ndime + 5] = l_coords[:ndata,0] * l_coords[:ndata,2]
            covariance_matrix[:ndata,ndata+ndime + 6] = l_coords[:ndata,1] * l_coords[:ndata,2]

        # 4. Assembly the symetric matrix part
        covariance_matrix = covariance_matrix + covariance_matrix.T - np.diag(covariance_matrix.diagonal())

        # 5. Assembly the right hand side of the covariance matrix
        covariance_vector = np.zeros(nequations)    
        covariance_vector[:ndata] = la.norm(l_coords[:,:] - coords_target[:], axis=1)

        # 6. Assembly the right hand side of the mean value basis
        covariance_vector[ndata]       = 1.0
        if (nequations > ndata+1): # Linear basis            
            covariance_vector[ndata+1]       = coords_target[0]
            covariance_vector[ndata+2]       = coords_target[1]
            covariance_vector[ndata+ndime]   = coords_target[ndime-1]
        if (nequations == ndata+ndime+4): # Add two dimensional quadratic basis
            covariance_vector[ndata+ndime+1] = coords_target[0]*coords_target[0]        
            covariance_vector[ndata+ndime+2] = coords_target[1]*coords_target[1]
            covariance_vector[ndata+ndime+3] = coords_target[0]*coords_target[1]
        if (nequations == ndata+ndime+7): # Add three dimensional quadratic basis       
            covariance_vector[ndata+ndime+1] = coords_target[0]*coords_target[1]        
            covariance_vector[ndata+ndime+2] = coords_target[1]*coords_target[1]        
            covariance_vector[ndata+ndime+3] = coords_target[2]*coords_target[2]        
            covariance_vector[ndata+ndime+4] = coords_target[0]*coords_target[1]        
            covariance_vector[ndata+ndime+5] = coords_target[0]*coords_target[2]        
            covariance_vector[ndata+ndime+6] = coords_target[1]*coords_target[2]        

        # 7. Solve system
        l_weights = la.solve(covariance_matrix , covariance_vector)

        return l_weights[:ndata]



    def get_weights(self,d,x):
        s=abs(x)/d
        if (s<=0.5):
            return (2/3)-4*s**2+4*s**3
        elif(s<=1):
            return (4/3)-4*s+4*s**2-(4/3)*s**3
        else:
            return 0



    def get_weights_vectorized(self,d,x):
        s = np.abs(x)/d
        l_conds = [(s <= 0.5), np.logical_and((s <= 1), (s > 0.5)), (s >  1)]
        l_funcs = [lambda s: (2/3)-4*s**2+4*s**3, lambda s: (4/3)-4*s+4*s**2-(4/3)*s**3, 0.0]
        return np.piecewise(s, l_conds, l_funcs)


 
    def calculate_interp_weights_rkpm(self,
                                      ndime,
                                      coords_target,
                                      l_coords):
        """ Computes interpolation weights for Reproducing Kernel Particle Method shape functions on point "coords_target".
            Input
                 ndime:         problem dimension
                 coords_target: coordinate where the interpolation will perform  
                 l_coords:      list of input nodal coordinates

            Output
                  l_weights:    list of weights corresponding to the nodal coordinates "l_coords" to interpolate the coordinate "coords_target" 


            IMPORTANT: Variable "self._support_size" Defines the radius support of nodal shape functions. 
                       User has to define this radius in such way that the supoprt of any nodal shape function has enough dimension 
                       in order to include the enough number of neighbors nodes.    

                       Input Variable "l_coords" has to content all the coordinate nodes which support contents the coordinate "coords_target" 
        """
        radius = self._support_size # ESTE PARAMETRO DEBE ESTAR INICIALIZADO EN LOS ARCHIVOS ENTRANTES

        order  = 1
        if (self._interp_order > 1):
            order = 2

        basis_size = 1
        if (order > 0): basis_size = basis_size + 2
        if (order > 1): basis_size = basis_size + 3

        # Determine p: nodal polinomial basis and p_i: polinomial basis for target coordinate
        p                = np.zeros((len(l_coords),basis_size))
        p_i              = np.zeros((1,basis_size))

        p[:,0]           = 1.0 # constant
        p_i[:,0]         = 1.0 # constant       
        if (order > 0):
            p[:,1]       = l_coords[:,0]       #x      
            p[:,2]       = l_coords[:,1]       #y      
            p[:,ndime]   = l_coords[:,ndime-1] #y or z?

            p_i[:,1]     = coords_target[0]       #x
            p_i[:,2]     = coords_target[1]       #y
            p_i[:,ndime] = coords_target[ndime-1] #y or z?                
        if (order > 1):
            if ndime == 2:
                p[:,3] = l_coords[:,0]*l_coords[:,0] #x^2
                p[:,4] = l_coords[:,1]*l_coords[:,1] #y^2
                p[:,5] = l_coords[:,0]*l_coords[:,1] #xy

                p_i[:,3] = coords_target[0]*coords_target[0] #x^2
                p_i[:,4] = coords_target[1]*coords_target[1] #y^2
                p_i[:,5] = coords_target[0]*coords_target[1] #xy                        
            if ndime == 3:        
                p[:,3]   = l_coords[:,0]*l_coords[:,0] #x^2
                p[:,4]   = l_coords[:,1]*l_coords[:,1] #y^2
                p[:,5]   = l_coords[:,2]*l_coords[:,2] #z^2
                p[:,6]   = l_coords[:,0]*l_coords[:,1] #xy
                p[:,7]   = l_coords[:,0]*l_coords[:,2] #xz
                p[:,8]   = l_coords[:,1]*l_coords[:,2] #yz

                p_i[:,3] = coords_target[0]*coords_target[0] #x^2
                p_i[:,4] = coords_target[1]*coords_target[1] #y^2
                p_i[:,5] = coords_target[2]*coords_target[2] #z^2
                p_i[:,6] = coords_target[0]*coords_target[1] #xy 
                p_i[:,7] = coords_target[0]*coords_target[2] #xz            
                p_i[:,8] = coords_target[1]*coords_target[2] #yz 

        # Calculate distance with respect to the target coordinate
        l_distances = la.norm(l_coords - coords_target, axis=1)

        # Calculate weights values for the target coordinate      
        w = self.get_weights_vectorized(radius,l_distances)

        # Calculate moment matrix      
        M = p.T * w @ p

        # Loop over input nodal coordinates
        # Determine the weigth for each nodal shape function
        f = np.matmul(p_i, la.inv(M))
        w = np.tile(w[np.newaxis],(basis_size,1))
        A = w * p.T
        l_weights = np.squeeze(np.matmul(f, A))

        return l_weights 
    


    def interpolate(self,
                    ndime,
                    source_name,
                    target_name,
                    values_source,
                    l_inodes_source=[],
                    l_inodes_target=[],
                    return_normalization=False):
        """ Interpolates nodal values from source problem, indicated by
            source_name in l_oproblems, to target probelm, indicated by 
            target_name in l_oproblems. Interpolation may be from the whole
            mesh or to/from a specified set of nodes given optionally
            as l_inodes_source and l_inodes_target.
        """

        # Load source/target problems
        osource = self.get_problem(source_name)
        otarget = self.get_problem(target_name)
        ncols   = osource._nnode
        nrows   = otarget._nnode

        # Activate flag for selected source/target nodes
        flag_select_source = len(l_inodes_source)>0
        flag_select_target = len(l_inodes_target)>0

        # Generate list of source/target nodes
        if not flag_select_source: l_inodes_source = np.arange(ncols)
        if not flag_select_target: l_inodes_target = np.arange(nrows)

        # Initialize output array
        nnode_target  = len(l_inodes_target)
        if ndime == 1:
            values_target = np.zeros((nnode_target), float)
        else:
            values_target = np.zeros((ndime, nnode_target), float)

        # Extract relevant transmission matrix
        interp_matrix = self._interp_matrix
        if flag_select_source: interp_matrix = interp_matrix[:,l_inodes_source]
        if flag_select_target: interp_matrix = interp_matrix[l_inodes_target,:]

        # Interpolate values from source to target
        if ndime == 1:
            values_target[l_inodes_target] = \
                    interp_matrix.dot(values_source[l_inodes_source])
        else:
            for idime in range(ndime):
                values_target[idime,l_inodes_target] = \
                        interp_matrix.dot(values_source[idime,l_inodes_source])

        # Sum target values for all MPI processes
        comm = MPI.COMM_WORLD
        if comm.Get_size() > 1:
            if ndime == 1:
                buff = np.zeros(nnode_target, float)
                comm.Allreduce(values_target, buff, op=MPI.SUM)
                values_target += buff
            elif ndime == 2:
                for idime in range(self._ndime):
                    buff = np.zeros(nnode_target, float)
                    comm.Allreduce(values_target[idime,:], buff, op=MPI.SUM)
                    values_target[idime,:] = buff

        if return_normalization:
            tol = 1e-12
            normalization = (np.abs(np.sum(interp_matrix,axis=1)) > tol)+0
            return values_target, normalization
        else:
            return values_target



    def spread(self, ndime, source_name, target_name, equation_name, values_source):
        """ Spreads nodal values from source problem, indicated by
            source_name in l_oproblems, to target problem, indicated by 
            target_name in l_oproblems.
        """

        osource = self.get_problem(source_name)
        otarget = self.get_problem(target_name)
        nnode_target = otarget._nnode
        if ndime == 1:
            values_target = np.zeros((nnode_target), float)
        else:
            values_target = np.zeros((ndime, nnode_target), float)

        if np.ndim(values_source) == 1:
            values_source = values_source.reshape((ndime,-1),order='F')

        # Discard values from free nodes        
        values_source[ osource._d_free_dofs[equation_name]] = 0.0

        if ndime == 1:
            values_target = self._spread_matrix.dot(values_source)
        else:
            for idime in range(ndime):
                values_target[idime,:] = self._spread_matrix.dot(values_source[idime,:])

        # Sum target values for all MPI processes
        values_target = otarget.par_array_exchange( values_target )

        return values_target



    def get_problem(self, problem_name):
        """ Extract problem with name "problem_name". """

        return self._l_oproblems[ self._l_problem_names.index(problem_name) ]



    def activate_iterative_coupling(self):
        self._is_iterative = True
        for oproblem in self._l_oproblems:
            oproblem.activate_iterative()



    def modify_timestep(self, dt):
        """ Modifies global timestep size of coupling and problems in coupling. """
        
        # Modify timestep of coupled problems
        for oproblem in self._l_oproblems:
            oproblem.modify_timestep( dt )

        # Modify coupling global timestep
        self._d_params['dt'] = dt



    def deallocate(self):
        """ Deallocates all variables from all Fortran instances. """
        for oproblem in self._l_oproblems:
            oproblem.deallocate()



    def qn_inverse_jacobian(self, H, p, q):
        """ Iteratively computes Quasi-Newton scheme approximation to inverse
            jacobian matrix (see Stoer 2002) where:

            (gamma,theta) = (1,0)                   => rank 2 DFP method
            (gamma,theta) = (1,1)                   => rank 2 BFGS method
            (gamma,theta) = (1,p^T*q/p^T*q-q^T*H*q) => rank 1 Broyden method

            Following Le 2001 here we use BFGS which seems to work best.
        """
        # Set parameters for BFGS method
        gamma = 1.0
        theta = 1.0
        p = p.reshape((-1,1))
        q = q.reshape((-1,1))
        # Compute repeated factors
        pq  = p.T @ q
        qHq = q.T @ H @ q
        # Compute approx. to inv. jacobian matrix for next iteration
        H = gamma*H + (1.+gamma*theta*qHq/pq)/pq * (p@p.T) \
          - gamma*(1.-theta)/qHq * (H@q) @ (q.T@H)         \
          - gamma*theta/pq * (p@q.T@H + H@q@p.T)
        return H



    def compute_indicator_field(self):
        """ Computes level set indicator field for wet nodes as normalized
            sum along columns of interpolation matrix.
        """
        indicator_field = np.squeeze( np.array( self._interp_matrix.sum(axis=0) ) )
        return indicator_field / np.amax( indicator_field )


      
    def restart(self):
        """ Restarts previously saved Coupling. """

        if self._d_params['flag_load_restart']: self.load_state()



    def load_state(self):
        """ Loads state from pickle file for restart. """

        # Get MPI rank
        comm = MPI.COMM_WORLD

        # Get filename
        if comm.Get_size() > 1:
            filename = os.path.join(self._d_params['path_inputs'], \
                                    self._d_params['path_outs'], \
                                    self._d_params['casename']+'_'+str(self._rank)+'.rst')
        else:
            filename = os.path.join(self._d_params['path_inputs'], \
                                    self._d_params['path_outs'], \
                                    self._d_params['casename']+'.rst')

        # Load pickle
        f = open(filename, 'rb')
        tmp_dict = pkl.load(f)
        f.close()

        # Remove unwanted attributes from loaded problem to avoid ovewriting
        tmp_dict.pop('_ntmax',None)
        tmp_dict.pop('_l_oproblems',None)
        tmp_dict['_d_params'] = self._d_params.copy()

        # Load problem
        self.__dict__.update(tmp_dict)




    def save_state(self):
        """ Saves current state to pickle file for later restart. """

        # Check if I must save state
        tcount    = self._tcount
        ndump     = self._d_params['ndump_restart']
        flag_save = (ndump > 0) and (np.mod(tcount,ndump) == 0) and (tcount > 0)
        if not flag_save: return

        # Get MPI rank
        comm = MPI.COMM_WORLD

        # Get filename
        if comm.Get_size() > 1:
            filename = os.path.join(self._d_params['path_outs'], \
                                    self._d_params['casename']+'_'+str(self._rank)+'.rst')
        else:
            filename = os.path.join(self._d_params['path_outs'], \
                                    self._d_params['casename']+'.rst')

        # Load pickle
        f = open(filename, 'wb')

        # Remove unnecessary attributes from saved problem 
        d_out = self.__dict__.copy()
        d_out.pop('_d_bconds',None)
        d_out.pop('_l_oproblems',None)

        # Dump problem
        pkl.dump(d_out, f, 2)
        f.close()



    @abstractmethod
    def begin_timestep(self):
        pass

    @abstractmethod
    def begin_iteration(self):
        pass

    @abstractmethod
    def end_iteration(self):
        pass

    @abstractmethod
    def end_timestep(self):
        pass
# ============================================================================ #
