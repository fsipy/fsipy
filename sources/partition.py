import numpy  as np
import gmsh
import os, sys
from   mpi4py import MPI

import warnings
warnings.filterwarnings("ignore")

############################################################
# MODULE FOR
# 1) PERFORM THE PARITION AND 
# 2) DETERMNINE THE PARALLEL ARRAYS TO INTERCHANGE DATA BETWEEN THE INTERFACE AND HALOS NODES
#
# FOR THAT
# 1) PERFOMR THE PARTITION USING GMSH
# 1.1) USE THE NEW GMSH FORMAT FOR OBTAINING THE GLOBAL NODE NUMBERING IN THE SUBDOMAIN 
# 1.2) SAVE AFTER THE MESH IN THE OLD GMSH FORMAT TO OBTAIN THE LOCAL NODE NUMBERING AND 
#      ALSO FOR PROBLEMS WITH THE BOUNDARIES RECOGNITION IN THE NEW FORMAT  
#
# 2) AS THE ELEMENTS IN THE PARTITION ARE SORTED ACCORDING THE GLOBAL MESH 
# 2.1) MATCH LOCAL (SUBDOMAIN MESH) AND GLOBAL (HOLE MEHS) NODAL INDICES
#      ACCORDING TO THEIR APPEARANCE IN A SORTED ELEMENT ARRAY
# 2.2) AND FOR HALO NODES, LOCAL INDICES ARE SORTED ACCORDING THE GLOBAL NUMERATION
#
# 3) FOR BUILDING THE PARALLEL ARRAYS
# 3.1) OBTAIN THE COMMON GLOBAL NODES TO INTERCHANGE BETWEEEN SUDDOMISN
# 3.2) SORT THIS GLOBAL NODES BEFORE TRANSFORM THEM IN ITS CORRESPONDING LOCAL
#      INDICES IN ORDER TO MAINTEIN ORDER COHERENCY   
############################################################
import numpy  as np
import gmsh
import os, sys
from   mpi4py import MPI

############################################################
def determine_mesh_nodes_and_elements(ndime):    
    l_itypes = gmsh.model.mesh.getElementTypes(ndime, -1)

    l_ielems       = []
    l_elems_inodes = []
    
    # Loop for elemental types
    for itype in l_itypes:
        # Obtain elemental indices and nodal indices for each element         
        l_ielems_tmp, l_elems_inodes_tmp  = gmsh.model.mesh.getElementsByType(itype, -1)
        
        nelems         = len(l_ielems_tmp)
        nnodes         = int(len(l_elems_inodes_tmp)/nelems)
        
        # Reshape l_elems_inodes_tmp in order each row be an elmentt and add it to l_elems_inodes
        l_elems_inodes.extend( np.reshape(l_elems_inodes_tmp,
                                          ((nelems,nnodes))).tolist())
        l_ielems.extend(l_ielems_tmp)

    l_inodes = np.unique(l_elems_inodes)
        
    return [l_ielems,
            l_elems_inodes,
            l_inodes]
############################################################

############################################################
def determine_global_halo_topology(nlevels,
                                   l_ielems,
                                   l_elems_inodes,
                                   l_ielems_I,
                                   l_elems_inodes_I,
                                   l_inodes_I):
    l_inodes_I_tot = l_inodes_I

    # Loop over levels of halos elements
    for ilevel in range(nlevels):   
        # Add a new level of halos for subdomain I
        # For that, we identify the indices of the elements in the whole domain (excluding the elements in I) that have at least one node defined in subdomain I  
        l_indices = np.where([ 
            sum(np.isin(l_elems_inodes[i],
                        l_inodes_I_tot)) > 0
            for i in range(len(l_elems_inodes)) 
        ])[0]

        l_elems_inodes_I_tot = [l_elems_inodes[i]
                                for i in l_indices]        
        l_inodes_I_tot       = np.unique(l_elems_inodes_I_tot).tolist()
        
    # Indices of the elements that belongs to I including the halos 
    l_ielems_I_tot       = [l_ielems[i] for i in l_indices]
    # Only halos
    l_ielems_I_hal       = np.setdiff1d(l_ielems_I_tot,
                                        l_ielems_I).tolist()    
    # Indices inside the l_ielems_I_tot that correspond to the halo elements
    l_indices            = np.where(np.isin(l_ielems_I_tot,
                                            l_ielems_I) == False)[0].tolist()    
    l_elems_inodes_I_hal = [l_elems_inodes_I_tot[i]
                            for i in l_indices]            
    # List of nodes that belong to the set of halo nodes of I: N^I_hal          
    l_inodes_I_hal       = np.setdiff1d(l_inodes_I_tot,
                                        l_inodes_I)    
    return [l_ielems_I_hal,
            l_elems_inodes_I_hal,
            l_inodes_I_hal]
############################################################

############################################################
def determine_local_halo_topology(nelems,
                                  l_inodes2_I_tot_glo,
                                  l_inodes2_I_tot_loc,
                                  l_ielems_I_hal_glo,
                                  l_elems_inodes_I_hal_glo,
                                  l_inodes_I_hal_glo):
    l_ielems_I_hal_loc       = [i+nelems+1
                                for i in range(nelems)]    
    l_elems_inodes_I_hal_loc = [elem_inodes.copy()
                                for elem_inodes in l_elems_inodes_I_hal_glo]
    # Loop over halo nodes
    for i in range(len(l_inodes2_I_tot_loc)):
        # Global and local indices for the current halo node
        inode_glo = l_inodes2_I_tot_glo[i]
        inode_loc = l_inodes2_I_tot_loc[i]
        
        # Loop over halo elements
        for j in range(len(l_elems_inodes_I_hal_loc)):
            # Find the global nodal index in the current halo element and change it for the local nodal index
            k_array = np.where(np.array(l_elems_inodes_I_hal_glo[j]) == inode_glo)[0]
            if len(k_array) > 0:                
                k = np.asscalar(k_array)
                l_elems_inodes_I_hal_loc[j][k] = inode_loc

    l_inodes_I_hal_loc = np.unique(l_inodes2_I_tot_loc).tolist()
    
    return l_ielems_I_hal_loc, l_elems_inodes_I_hal_loc, l_inodes_I_hal_loc
############################################################

############################################################
def determine_global2local_arrays(nnodes,
                                  l_elems_inodes_I_glo,
                                  l_elems_inodes_I_loc,
                                  l_inodes_I_hal_glo):
    # --> 1. Obtain the corresponding local and global indices for the nodes inside subdoamin I
    # Sort global nodes according to its appearing order inside the elements (THIS ORDER IT IS CRUCIAL TO BE ABLE TO PAIR GLOBAL WITH LOCAL INDICES )
    l_elems_inodes_I_glo_1d = [l_elems_inodes_I_glo[i][j]
                               for i in range(len(l_elems_inodes_I_glo))
                               for j in range(len(l_elems_inodes_I_glo[1])) ]    
    l_indices_glo           = np.unique(l_elems_inodes_I_glo_1d, return_index=True)[1]    
    l_inodes2_I_glo         = [l_elems_inodes_I_glo_1d[index]
                               for index in sorted(l_indices_glo)]    
    # Sort local nodes according to its appearing order inside the elements 
    l_elems_inodes_I_loc_1d = [l_elems_inodes_I_loc[i][j]
                               for i in range(len(l_elems_inodes_I_loc))
                               for j in range(len(l_elems_inodes_I_loc[1])) ]
    l_indices_loc           = np.unique(l_elems_inodes_I_loc_1d, return_index=True)[1]    
    l_inodes2_I_loc         = [l_elems_inodes_I_loc_1d[index]
                               for index in sorted(l_indices_loc)]    
    # --> 2. Obtain the corresponding local and global indices for the HALO nodes of subdoamin I
    # Sort global halo nodes in order     
    l_inodes2_I_hal_glo     = np.unique(l_inodes_I_hal_glo)
    # Obtain a secuence of numbers that will correspond to the local numberation of the halo nodes           
    l_inodes2_I_hal_loc     = [i+nnodes+1
                               for i in range(len(l_inodes_I_hal_glo)) ]    
    # --> 3. Add the local and global arrays
    l_inodes2_I_tot_glo = l_inodes2_I_glo.copy()  
    l_inodes2_I_tot_glo.extend(l_inodes2_I_hal_glo)

    l_inodes2_I_tot_loc = l_inodes2_I_loc.copy()  
    l_inodes2_I_tot_loc.extend(l_inodes2_I_hal_loc)
    
    return [l_inodes2_I_tot_glo,
            l_inodes2_I_tot_loc]
############################################################

############################################################
def global2local(l_inodes_glo, l_indices_glo, l_indices_loc):
    # Determine the position of a global node 
    l_positions = []
    for inode_glo in l_inodes_glo:
        position = np.where(np.isin(l_indices_glo,inode_glo))[0].tolist()
        l_positions.append(l_indices_loc[position[0]])
        
    return l_positions
############################################################

############################################################
def determine_interface_send_and_receive_array(jsubdomain,                                               
                                               l_inodes_I_glo,                           # <-- I nodes
                                               l_inodes_J_glo,                           # <-- J nodes
                                               l_inodes2_I_tot_glo, l_inodes2_I_tot_loc, # <-- arrays for match local and global indices
                                               l_ineighbors_IJ,
                                               l_inodes_interface_IJ, 
                                               l_nnodes_shared_I):                       # <-- number of subdomains taht share a node 
    # Interface nodes in common (globally sorted to assure matchig between subdomians)
    l_inodes_IJ_glo = np.sort(l_inodes_I_glo[np.in1d(l_inodes_I_glo, l_inodes_J_glo)])       
    # If there is at least one node in common
    if len(l_inodes_IJ_glo) > 0:        
        # Save the subdomain as a neighbor of i from 0
        l_ineighbors_IJ.append(jsubdomain-1)
        # Obtain local nodes indices
        l_inodes_IJ_loc = global2local(l_inodes_IJ_glo,
                                       l_inodes2_I_tot_glo,
                                       l_inodes2_I_tot_loc)
        # Gmsh start nodal indices with 1 but Python with 0
        l_inodes_IJ_loc = [i-1 for i in l_inodes_IJ_loc]
        
        # Add the number of nodes in common
        l_nnodes_shared_I[l_inodes_IJ_loc] = l_nnodes_shared_I[l_inodes_IJ_loc] \
                                           + np.ones(len(l_inodes_IJ_loc),int)
        
        l_inodes_interface_IJ.extend([l_inodes_IJ_loc])
        
    return [l_ineighbors_IJ,
            l_inodes_interface_IJ,
            l_nnodes_shared_I]
############################################################

############################################################
def determine_halo_send_and_receive_arrays(jsubdomain,                              
                                           l_inodes_I_hal_glo, l_inodes_I_glo, # <-- I nodes (including halos)
                                           l_inodes_J_glo, l_inodes_J_hal_glo, # <-- J nodes (including halos)
                                           l_inodes2_I_tot_glo, l_inodes2_I_tot_loc,  # <-- arrays for match local and global indices
                                           l_ineighbors_IJ_hal,
                                           l_inodes_halos_IJ, l_inodes_halos_JI):
    # Global halo nodes of I in common with J (globally sorted to assure matchig between subdomains)
    l_inodes_IJ_hal_glo = np.sort(l_inodes_I_hal_glo[ np.in1d(l_inodes_I_hal_glo,
                                                              l_inodes_J_glo)     ])
    # If there is at least one node in common
    if len(l_inodes_IJ_hal_glo) > 0:        
        # Global halo nodes of J in common wiyh I
        l_inodes_JI_hal_glo = np.sort(l_inodes_J_hal_glo[ np.in1d(l_inodes_J_hal_glo,
                                                                  l_inodes_I_glo)     ])                    
        # Save the subdomain as a neighbor of i        
        l_ineighbors_IJ_hal.append(jsubdomain-1)
        
        # Obtain local nodes indices to receive
        l_inodes_IJ_hal_loc = global2local(l_inodes_IJ_hal_glo,
                                           l_inodes2_I_tot_glo,
                                           l_inodes2_I_tot_loc)        
        # Obtain local nodes indices to send (globally sorted to assure matchig between subdomains)
        l_inodes_JI_hal_loc = global2local(l_inodes_JI_hal_glo,
                                           l_inodes2_I_tot_glo,
                                           l_inodes2_I_tot_loc)

        # Gmsh start nodal indices with 1 but Python with 0
        l_inodes_IJ_hal_loc = [i-1 for i in l_inodes_IJ_hal_loc]        
        l_inodes_halos_IJ.extend([l_inodes_IJ_hal_loc])
        
        # Gmsh start nodal indices with 1 but Python with 0
        l_inodes_JI_hal_loc = [i-1 for i in l_inodes_JI_hal_loc]                
        l_inodes_halos_JI.extend([l_inodes_JI_hal_loc]) 
        
    return [l_ineighbors_IJ_hal,
            l_inodes_halos_IJ,
	    l_inodes_halos_JI]
############################################################

############################################################
def perfom_partition(nsubdomains, isubdomain, ndime):    
    # Perform the partition and save each subdomains mesh in a independent file (only one subdomain has to do it)
    
    if isubdomain == 1:
        # Create folder "partitions" if doesn't exist where paritions will be stored
        isExist = os.path.exists("partitions")
        if not isExist:
            # Create a new directory because it does not exist
            os.makedirs("partitions")

    # Wait until all subdomains have access to the folder "partitions" 
    MPI.COMM_WORLD.Barrier()
            
    if isubdomain == 1:
        # Create partitions only if didn't exist previously a folder "partitions"
        if not isExist:
            print ("Creating a new partiion")
            # Create partitions
            gmsh.model.mesh.partition(nsubdomains)    
            # It is neceesary to use new format to obtain correct node numbering for interchage data betweeen subdomains
            gmsh.option.setNumber("Mesh.PartitionSplitMeshFiles", 1)	           
            gmsh.write("partitions/partition.msh")    
            gmsh.option.setNumber("Mesh.PartitionSplitMeshFiles", 0)
        else:
            print ("Reading previous partition --> delete folder /partitions/ in order to create new partition")
            
            
    # Wait until all subdomains have access to the their respective partition information
    MPI.COMM_WORLD.Barrier()        
        
    # Delete mesh and entities
    gmsh.clear()    
    # Open file with the mesh information of current subdomain I in an independent model
    
    gmsh.open("partitions/partition_"+str(isubdomain)+".msh")    
    # Current subdomain mesh (global indices)     
    [l_ielems_I_glo,
     l_elems_inodes_I_glo,
     l_inodes_I_glo]       = determine_mesh_nodes_and_elements(ndime)
    # It is neceesary to save the mesh of the subdomain with the old format to obtain correct topology partition 
    gmsh.write("partitions/partition_"+str(isubdomain)+".msh2")

    # Wait until all subdomains have their respestives partition meshes saved  
    MPI.COMM_WORLD.Barrier()
    
    # Delete mesh and entities
    gmsh.clear()    
    # Open file with the mesh information of current subdomain I in an independent model    
    gmsh.open("partitions/partition_"+str(isubdomain)+".msh2")
        
    # Current subdomain mesh (local indices)     
    [l_ielems_I_loc,
     l_elems_inodes_I_loc,
     l_inodes_I_loc]        = determine_mesh_nodes_and_elements(ndime)

    return [l_ielems_I_glo, l_elems_inodes_I_glo, l_inodes_I_glo, 
            l_ielems_I_loc, l_elems_inodes_I_loc, l_inodes_I_loc]
############################################################

############################################################    
def build_parallel_data(ndime):

    # MPI global inforation
    nsubdomains = MPI.COMM_WORLD.Get_size()
    isubdomain  = MPI.COMM_WORLD.Get_rank() + 1

    d_inodes_interface_IJ = {}
    l_nnodes_shared_I     = np.array([])

    if nsubdomains > 1:
        # Total mesh 
        l_ielems, l_elems_inodes, l_inodes = determine_mesh_nodes_and_elements(ndime)

        #####################################
        # 1. Perform partition 
        [l_ielems_I_glo,
         l_elems_inodes_I_glo,
         l_inodes_I_glo,
         l_ielems_I_loc,
         l_elems_inodes_I_loc,
         l_inodes_I_loc]       = perfom_partition(nsubdomains, isubdomain, ndime)
        #####################################
        # Determine global halo topology
        [l_ielems_I_hal_glo,       
         l_elems_inodes_I_hal_glo,  
         l_inodes_I_hal_glo]       = determine_global_halo_topology(1,
                                                                    l_ielems,
                                                                    l_elems_inodes,
                                                                    l_ielems_I_glo,
                                                                    l_elems_inodes_I_glo,
                                                                    l_inodes_I_glo)
        #####################################
        # Determine global and local node correpondency for current subdomain (including the halos)
        nnodes = len(l_inodes_I_glo)
        [l_inodes2_I_tot_glo,
         l_inodes2_I_tot_loc] = determine_global2local_arrays(nnodes,
                                                              l_elems_inodes_I_glo,
                                                              l_elems_inodes_I_loc,
                                                              l_inodes_I_hal_glo)
        #####################################
        # Determine local halo topology from the global numeration
        nelems = len(l_ielems_I_glo)
        [l_ielems_I_hal_loc,
         l_elems_inodes_I_hal_loc,
         l_inodes_I_hal_loc]       = determine_local_halo_topology(nelems,
                                                                   l_inodes2_I_tot_glo,
                                                                   l_inodes2_I_tot_loc,
                                                                   l_ielems_I_hal_glo,
                                                                   l_elems_inodes_I_hal_glo,
                                                                   l_inodes_I_hal_glo)
        #####################################

        #####################################
        # Initializations
        # List of neighbor subdomains of isubdomain 
        l_ineighbors_IJ      = []
        l_ineighbors_IJ_hal  = []
        # List of common nodes of isubdomain for each neighbor subdomain (with local indices)     
        l_inodes_interface_IJ = []
        l_inodes_halos_IJ     = []
        l_inodes_halos_JI     = []
        # Number of subdomains share for a node  (DO NOT USE +1 beacause in pyhton indices start from 0 )  
        l_nnodes_shared_I     = np.ones(len(l_inodes_I_loc),int)
        #####################################

        #####################################
        # Loop over subdomains 
        for jsubdomain in range(1,nsubdomains+1):
            if (isubdomain != jsubdomain):        
                # Open file with the mesh information of a different subdomain J in an independent model    
                gmsh.open("partitions/partition_"+str(jsubdomain)+".msh")
                # Subdomain mesh (global indices) of J
                [l_ielems_J_glo,         
                 l_elems_inodes_J_glo,
                 l_inodes_J_glo]       = determine_mesh_nodes_and_elements(ndime)
                # Determine global halo topology of J            
                [l_ielems_J_hal_glo,
                 l_elems_inodes_J_hal_glo,
                 l_inodes_J_hal_glo]       = determine_global_halo_topology(1,
                                                                            l_ielems,
                                                                            l_elems_inodes,
                                                                            l_ielems_J_glo,
                                                                            l_elems_inodes_J_glo,
                                                                            l_inodes_J_glo)
                #####################################
                # BUILD INTERFACE PARALLEL ARRAY FOR J
                [l_ineighbors_IJ,
                 l_inodes_interface_IJ,
                 l_nnodes_shared_I,] =   determine_interface_send_and_receive_array(jsubdomain,
                                                                                    l_inodes_I_glo,                           # <-- I nodes
                                                                                    l_inodes_J_glo,                           # <-- J nodes
                                                                                    l_inodes2_I_tot_glo, l_inodes2_I_tot_loc, # <-- arrays for match local and global indices
                                                                                    l_ineighbors_IJ,
                                                                                    l_inodes_interface_IJ,
                                                                                    l_nnodes_shared_I)                        # <-- number of subdomains taht share a node
                #####################################
                # BUILD HALOS PARALLEL ARRAYS FOR J
                [l_ineighbors_IJ_hal,
                 l_inodes_halos_IJ,
                 l_inodes_halos_JI] = determine_halo_send_and_receive_arrays(jsubdomain,  
                                                                             l_inodes_I_hal_glo,  l_inodes_I_glo,         # <-- I nodes (including halos)
                                                                             l_inodes_J_glo,      l_inodes_J_hal_glo,     # <-- J nodes (including halos)
                                                                             l_inodes2_I_tot_glo, l_inodes2_I_tot_loc,    # <-- arrays for match local and global indices
                                                                             l_ineighbors_IJ_hal, 
                                                                             l_inodes_halos_IJ, l_inodes_halos_JI)
                #####################################
                # Remove model with the mesh information of subdomain J
                gmsh.model.remove()

        d_inodes_interface_I  = {i:j     for i, j    in zip(l_ineighbors_IJ,     l_inodes_interface_IJ)}
        d_inodes_halos_I      = {i:[j,k] for i, j, k in zip(l_ineighbors_IJ_hal, l_inodes_halos_IJ, l_inodes_halos_JI)}        
        #####################################

    d_parallel = {'nodal_interface_exchange': d_inodes_interface_I,
                  'nodal_halo_exchange'     : d_inodes_halos_I,
                  'shared_nodes_number'     : l_nnodes_shared_I}
        
    return d_parallel
############################################################
