""" FsiPy Test-Suite """

## Import modules
from   mpi4py import MPI
import pytest
import os
import sys
import numpy  as np
import pickle as pkl
import shutil

sys.path.append(os.path.abspath('../'))
from   Main   import run_simulation

## Set path and define MPI communicator
this_dir = os.path.dirname(os.path.abspath(__file__))
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# ---------------------------------------------------------------------------- #
#   Sequential tests                                                           #
# ---------------------------------------------------------------------------- #
@pytest.mark.skipif(size != 1, reason='test is sequential')
def test_csm3():
    casename       = 'csm3'
    tolerance      = 1e-9
    isequal, error = run_test( casename, tolerance )
    assert isequal, error

@pytest.mark.skipif(size != 1, reason='test is sequential')
def test_csm3implicit():
    casename       = 'csm3implicit'
    tolerance      = 1e-9
    isequal, error = run_test( casename, tolerance )
    assert isequal, error

@pytest.mark.skipif(size != 1, reason='test is sequential')
def test_cfd3():
    casename       = 'cfd3'
    tolerance      = 1e-9
    isequal, error = run_test( casename, tolerance )
    assert isequal, error

@pytest.mark.skipif(size != 1, reason='test is sequential')
def test_cfd3_restart():
    casename       = 'cfd3_restart'
    tolerance      = 1e-9
    isequal, error = run_test( casename, tolerance )
    assert isequal, error

@pytest.mark.skipif(size != 1, reason='test is sequential')
def test_fsi3():
    casename       = 'fsi3_ib'
    tolerance      = 1e-9
    isequal, error = run_test( casename, tolerance )
    assert isequal, error

@pytest.mark.skipif(size != 1, reason='test is sequential')
def test_fsi3_ib_kriging():
    casename       = 'fsi3_ib_kriging'
    tolerance      = 1e-9
    isequal, error = run_test( casename, tolerance )
    assert isequal, error

@pytest.mark.skipif(size != 1, reason='test is sequential')
def test_fsi3_mifem():
    casename       = 'fsi3_mifem'
    tolerance      = 1e-9
    isequal, error = run_test( casename, tolerance )
    assert isequal, error

@pytest.mark.skipif()
def test_fsi3_nifem():
    casename       = 'fsi3_nifem'
    tolerance      = 1e-9
    isequal, error = run_test( casename, tolerance )
    assert isequal, error

@pytest.mark.skip()
def test_fsi3_qnib():
    casename      = 'fsi3_qnib'
    tolerance     = 1e-9
    isequal, error = run_test( casename, tolerance )
    assert isequal, error


# ---------------------------------------------------------------------------- #
#   Parallel tests                                                             #
# ---------------------------------------------------------------------------- #
@pytest.mark.skipif(size != 5, reason='requires 5 MPI processes')
def test_fsi3_parallel():
    casename       = 'fsi3_ib_parallel'
    tolerance      = 1e-9
    is_parallel    = True 
    isequal, error = run_test( casename, tolerance, is_parallel )
    assert isequal, error


# ---------------------------------------------------------------------------- #
#   Test-Suite functions                                                       #
# ---------------------------------------------------------------------------- #
def compare(dvar_test, dvar_ref, tolerance):
    isequal = True
    difference = 0.0
    # Compare all variables in dictionary
    print(' ')
    maxdiff = 0.0
    for key in dvar_ref:
        var_test = dvar_test[key]
        var_ref  = dvar_ref[key]
        ndime = np.ndim(var_ref)
        if ndime==2:
            var_ref  = var_ref[:,0]
            var_test = var_test[:,0]
        if ndime==3:
            var_ref  = var_ref[:,:,0]
            var_test = var_test[:,:,0]
        diff     = np.sum(np.abs(var_ref - var_test)) / np.sum(np.abs(var_test))
        if (diff > tolerance):
            isequal = False
            print(key+' is incorrect by: '+str(diff*100.)+'%')
        else:
            print(key+' is correct')
        maxdiff = max([diff, maxdiff])
    return isequal, maxdiff


def run_test(casename, tolerance, is_parallel=False):
    if is_parallel:
        isequal, diff = run_parallel_test(casename, tolerance)
    else:
        isequal, diff = run_sequential_test(casename, tolerance)
    error_msg = "{:.2e}".format(diff)
    return isequal, error_msg 


def run_sequential_test(casename, tolerance):
    case_dir   = os.path.join(this_dir, 'TestSuite/cases', casename)
    fname_case = os.path.join(case_dir, casename)
    
    # Run case
    problem    = run_simulation( fname_case )
    # For coupled problems: compare variables of all subproblems
    if problem._d_params['physics'] == 'coupled':
        isequal = True
        maxdiff = 0.0
        for oproblem in problem._l_oproblems:
            casename  = oproblem._casename
            fname_ref = os.path.join(case_dir, 'reference', casename + '.pkl')
            print(fname_ref)
            dvar_test = oproblem._d_variables
            with open(fname_ref, 'rb') as f:
                dvar_ref = pkl.load(f)
            iseqtmp, diff = compare(dvar_test, dvar_ref, tolerance)
            if iseqtmp: print('=> '+oproblem._casename+' is correct\n')
            else:       print('=> '+oproblem._casename+' is incorrect\n')
            isequal = isequal and iseqtmp
            maxdiff = max([diff, maxdiff])
        if isequal: print('=> '+fname_case+' is correct\n')
    # For single problems: compare variables single problem 
    else:
        fname_ref = os.path.join(case_dir, 'reference', casename + '.pkl')
        print(fname_ref)
        dvar_test = problem._d_variables
        with open(fname_ref, 'rb') as f:
            dvar_ref = pkl.load(f)
        isequal, maxdiff = compare(dvar_test, dvar_ref, tolerance)
        if isequal: print('=> '+problem._casename+' is correct\n')
        else:       print('=> '+problem._casename+' is incorrect\n')
    return isequal, maxdiff


def run_parallel_test(casename, tolerance):
    case_dir   = os.path.join(this_dir, 'TestSuite/cases', casename)
    fname_case = os.path.join(case_dir, casename)

    # Copy partition information to the current folder if it is different one from the case folder
    isubdomain  = MPI.COMM_WORLD.Get_rank() + 1
    if (isubdomain == 1):
        curr_dir = os.getcwd()
        
        if (curr_dir != case_dir):
            if os.path.exists(curr_dir+'/partitions'):
                shutil.rmtree(curr_dir+'/partitions')
            
            shutil.copytree(case_dir+'/partitions', curr_dir+'/partitions')
        
    # Wait until all subdomains have access to the folder "partitions" 
    MPI.COMM_WORLD.Barrier()
    
    # Run case
    problem    = run_simulation( fname_case )
    # For coupled problems: compare variables of all subproblems
    if problem._d_params['physics'] == 'coupled':
        isequal = True
        maxdiff = 0.0
        for oproblem in problem._l_oproblems:
            casename  = oproblem._casename
            fname_ref = os.path.join(case_dir, 'reference', casename + '_' + str(rank) + '.pkl')
            print(fname_ref)
            dvar_test = oproblem._d_variables
            with open(fname_ref, 'rb') as f:
                dvar_ref = pkl.load(f)
            iseqtmp, diff = compare(dvar_test, dvar_ref, tolerance)
            if iseqtmp: print('=> '+oproblem._casename+' is correct\n')
            else:       print('=> '+oproblem._casename+' is incorrect\n')
            isequal = isequal and iseqtmp
            maxdiff = max([diff, maxdiff])
        if isequal: print('=> '+fname_case+' is correct\n')
    # For single problems: compare variables single problem 
    else:
        fname_ref = os.path.join(case_dir, 'reference', casename + '_' + str(rank) + '.pkl')
        print(fname_ref)
        dvar_test = problem._d_variables
        with open(fname_ref, 'rb') as f:
            dvar_ref = pkl.load(f)
        isequal, maxdiff = compare(dvar_test, dvar_ref, tolerance)
        if isequal: print('=> '+problem._casename+' is correct\n')
        else:       print('=> '+problem._casename+' is incorrect\n')
    return isequal, maxdiff
