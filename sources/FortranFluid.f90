module FortranFluid
! ============================================================================ !
!                        FsiPy Fortran Fluid Module                            !
! ============================================================================ !
    use types
    use FortranKernel

    implicit none

    type, extends(Equation), public :: VelocityPrediction
    contains
        procedure, pass(this), public :: compute_gauss_lhs_rhs => compute_gauss_lhs_rhs_velocity_prediction
        procedure, pass(this), public :: compute_gauss_lhs     => compute_gauss_lhs_velocity_prediction
        procedure, pass(this), public :: compute_gauss_rhs     => compute_gauss_rhs_velocity_prediction
        procedure, pass(this), public :: compute_gauss_force   => compute_gauss_force_velocity_prediction
        procedure, pass(this), public :: compute_natural_bcs   => compute_natural_bcs_velocity_prediction 
    end type

    type, extends(Equation) :: PressureSolution 
    contains
        procedure, pass(this), public :: compute_gauss_lhs_rhs => compute_gauss_lhs_rhs_pressure_solution
        procedure, pass(this), public :: compute_gauss_lhs     => compute_gauss_lhs_pressure_solution
        procedure, pass(this), public :: compute_gauss_rhs     => compute_gauss_rhs_pressure_solution
        procedure, pass(this), public :: compute_gauss_force   => compute_gauss_force_pressure_solution
        procedure, pass(this), public :: compute_natural_bcs   => compute_natural_bcs_pressure_solution 
    end type

    type, extends(Equation) :: VelocityCorrection 
    contains
        procedure, pass(this), public :: compute_gauss_lhs_rhs => compute_gauss_lhs_rhs_velocity_correction
        procedure, pass(this), public :: compute_gauss_lhs     => compute_gauss_lhs_velocity_correction
        procedure, pass(this), public :: compute_gauss_rhs     => compute_gauss_rhs_velocity_correction
        procedure, pass(this), public :: compute_gauss_force   => compute_gauss_force_velocity_correction
        procedure, pass(this), public :: compute_natural_bcs   => compute_natural_bcs_velocity_correction 
    end type

    type, extends(Equation) :: StabilizationSolution 
    contains
        procedure, pass(this), public :: compute_gauss_lhs_rhs => compute_gauss_lhs_rhs_stabilization_solution
        procedure, pass(this), public :: compute_gauss_lhs     => compute_gauss_lhs_stabilization_solution
        procedure, pass(this), public :: compute_gauss_rhs     => compute_gauss_rhs_stabilization_solution
        procedure, pass(this), public :: compute_gauss_force   => compute_gauss_force_stabilization_solution
        procedure, pass(this), public :: compute_natural_bcs   => compute_natural_bcs_stabilization_solution 
    end type
    !
    ! Global variables
    !
    public
    real(8)              :: gama 
    real(8)              :: theta
    real(8), allocatable :: el_tau(:)
    real(8), allocatable :: gp_skew_symmetric(:,:,:,:,:,:)
    real(8), allocatable :: gp_rho(:,:)
    real(8), allocatable :: gp_nu(:,:)

contains


subroutine dealloc()
    deallocate( l_elems )
    deallocate( gauss_weights )
    deallocate( gp_jacobians )
    deallocate( gp_inv_jacobians )
    deallocate( gp_det_jacobians )
    deallocate( gp_coords )
    deallocate( gp_shape )
    deallocate( gp_dshape )
    deallocate( gp_volume )
    deallocate( gp_dshape_glo )
    deallocate( gp_shape_shape )
    deallocate( gp_shape_dshape )
    deallocate( gp_dshape_shape )
    deallocate( gp_dshape_dshape )
    deallocate( el_tau )
    deallocate( gp_skew_symmetric )
    deallocate( gp_rho )
    deallocate( gp_nu )
end subroutine


subroutine initialize(l_elems_in,          &
                      gauss_weights_in,    &
                      gp_jacobians_in,     &
                      gp_inv_jacobians_in, &
                      gp_det_jacobians_in, &
                      gp_coords_in,        &
                      gp_shape_in,         &
                      gp_dshape_in,        &
                      gp_volume_in,        &
                      el_tau_in,           &
                      gp_rho_in,           &
                      gp_nu_in,            &
                      material_in,         &
                      dt_in,               &
                      gama_in,             &
                      theta_in,            &
                      nnode_in,            &
                      ndofs_total_in,      &
                      npast_in,            &
                      ndime_in,            &
                      nelem_in,            &
                      nnode_elem_in,       &
                      ngaus_in,            &
                      nbasis_in)
    integer(4),       intent(in) :: l_elems_in(nelem_in,nnode_elem_in)
    real(8),          intent(in) :: gauss_weights_in(ngaus_in)
    real(8),          intent(in) :: gp_jacobians_in(nelem_in,ngaus_in,ndime_in,ndime_in)
    real(8),          intent(in) :: gp_inv_jacobians_in(nelem_in,ngaus_in,ndime_in,ndime_in)
    real(8),          intent(in) :: gp_det_jacobians_in(nelem_in,ngaus_in)
    real(8),          intent(in) :: gp_coords_in(nelem_in,ngaus_in,ndime_in)
    real(8),          intent(in) :: gp_shape_in(ngaus_in,nbasis_in)
    real(8),          intent(in) :: gp_dshape_in(ngaus_in,nbasis_in,ndime_in)
    real(8),          intent(in) :: gp_volume_in(nelem_in,ngaus_in)
    real(8),          intent(in) :: el_tau_in(nelem_in)
    real(8),          intent(in) :: gp_rho_in(nelem_in,ngaus_in)
    real(8),          intent(in) :: gp_nu_in(nelem_in,ngaus_in)
    character(len=7), intent(in) :: material_in
    real(8),          intent(in) :: dt_in
    real(8),          intent(in) :: gama_in
    real(8),          intent(in) :: theta_in
    integer(4) :: nnode_in
    integer(4) :: ndofs_total_in
    integer(4) :: npast_in
    integer(4) :: ndime_in
    integer(4) :: nelem_in
    integer(4) :: nnode_elem_in
    integer(4) :: ngaus_in
    integer(4) :: nbasis_in

    call initialize_kernel(l_elems_in,          &
                           gauss_weights_in,    &
                           gp_jacobians_in,     &
                           gp_inv_jacobians_in, &
                           gp_det_jacobians_in, &
                           gp_coords_in,        &
                           gp_shape_in,         &
                           gp_dshape_in,        &
                           gp_volume_in,        &
                           material_in,         &
                           dt_in,               &
                           nnode_in,            &
                           ndofs_total_in,      &
                           npast_in,            &
                           ndime_in,            &
                           nelem_in,            &
                           nnode_elem_in,       &
                           ngaus_in,            &
                           nbasis_in)

    ! Allocate global arrays required in FortranFluid
    allocate( gp_shape_dshape(nelem,ngaus,nnode_elem,nnode_elem,ndime) )
    allocate( gp_dshape_shape(nelem,ngaus,nnode_elem,nnode_elem,ndime) )
    allocate( gp_dshape_dshape(nelem,ngaus,nnode_elem,nnode_elem)      )
    allocate( gp_skew_symmetric(nelem,ngaus,nnode_elem,nnode_elem,nnode_elem,ndime) )
    allocate( el_tau(nelem)                                            )
    allocate( gp_nu(nelem,ngaus)                                       )
    allocate( gp_rho(nelem,ngaus)                                      )

    ! Save FortranFluid's global parameters -> **DEFINE PASSING**
    gama   = gama_in
    theta  = theta_in
    el_tau = el_tau_in
    gp_nu  = gp_nu_in
    gp_rho = gp_rho_in

    ! Calculate elemental matrices required in FortranFluid
    call compute_laplacian_matrix()
    call compute_gradient_matrix()
    call compute_divergence_matrix()
    call compute_skew_symmetric_matrix()
end subroutine


subroutine assemble_algebraic_system_from_python(lhs,               &
                                                 idlhs,             &
                                                 rhs,               &
                                                 equation_name,     &
                                                 variables,         &
                                                 source,            &
                                                 levelset,          &
                                                 flag_assemble_lhs, &
                                                 tcount, &
                                                 ndofs_in,          &
                                                 nrows_in,          &
                                                 nnode_in,          &
                                                 ndofs_total_in,    &
                                                 npast_in,          &
                                                 lhs_lump)
    real(8),        intent(out) :: lhs(nrows_in)
    integer(4),     intent(out) :: idlhs(nrows_in, 4)
    real(8),        intent(out) :: rhs(ndofs_in*nnode_in)
    character(*),   intent(in)  :: equation_name
    real(8),        intent(in)  :: variables(ndofs_total_in, nnode_in, npast_in)
    real(8),        intent(in)  :: source(ndofs_total_in, nnode_in)
    real(8),        intent(in)  :: levelset(nnode_in)
    logical,        intent(in)  :: flag_assemble_lhs
    integer(4),     intent(in)  :: tcount 
    integer(4),     intent(in)  :: nrows_in
    integer(4),     intent(in)  :: nnode_in
    integer(4),     intent(in)  :: ndofs_total_in
    integer(4),     intent(in)  :: ndofs_in
    integer(4),     intent(in)  :: npast_in
    real(8),        intent(out) :: lhs_lump(ndofs_in*nnode_in)    
    type(VelocityPrediction)    :: ovelocity_prediction
    type(PressureSolution)      :: opressure_solution
    type(VelocityCorrection)    :: ovelocity_correction
    type(StabilizationSolution) :: ostabilization_solution

    if      (equation_name == 'velocity_prediction') then
        call    ovelocity_prediction % assemble_algebraic_system(lhs,               &
                                                                 idlhs,             &
                                                                 rhs,               &
                                                                 variables,         &
                                                                 source,            &
                                                                 levelset,          &
                                                                 flag_assemble_lhs, &
                                                                 tcount,            &
                                                                 ndofs_in,          &
                                                                 lhs_lump)
    else if (equation_name == 'pressure_solution') then
        call      opressure_solution % assemble_algebraic_system(lhs,               &
                                                                 idlhs,             &
                                                                 rhs,               &
                                                                 variables,         &
                                                                 source,            &
                                                                 levelset,          &
                                                                 flag_assemble_lhs, &
                                                                 tcount,            &
                                                                 ndofs_in,          &
                                                                 lhs_lump)
    else if (equation_name == 'velocity_correction') then
        call    ovelocity_correction % assemble_algebraic_system(lhs,               &
                                                                 idlhs,             &
                                                                 rhs,               &
                                                                 variables,         &
                                                                 source,            &
                                                                 levelset,          &
                                                                 flag_assemble_lhs, &
                                                                 tcount,            &
                                                                 ndofs_in,          &
                                                                 lhs_lump)
    else if (equation_name == 'stabilization_solution') then
        call ostabilization_solution % assemble_algebraic_system(lhs,               &
                                                                 idlhs,             &
                                                                 rhs,               &
                                                                 variables,         &
                                                                 source,            &
                                                                 levelset,          &
                                                                 flag_assemble_lhs, &
                                                                 tcount,            &
                                                                 ndofs_in,          &
                                                                 lhs_lump)
    end if
end subroutine


subroutine assemble_force_from_python(force,             &
                                      equation_name,     &
                                      variables,         &
                                      source,            &
                                      ndofs_in,          &
                                      nrows_in,          &
                                      nnode_in,          &
                                      ndofs_total_in,    &
                                      npast_in)
    real(8),        intent(out) :: force(ndofs_in*nnode_in)
    character(*),   intent(in)  :: equation_name
    real(8),        intent(in)  :: variables(ndofs_total_in, nnode_in, npast_in)
    real(8),        intent(in)  :: source(ndofs_total_in, nnode_in)
    integer(4),     intent(in)  :: nrows_in
    integer(4),     intent(in)  :: nnode_in
    integer(4),     intent(in)  :: ndofs_total_in
    integer(4),     intent(in)  :: ndofs_in
    integer(4),     intent(in)  :: npast_in
    type(VelocityPrediction)    :: ovelocity_prediction
    type(PressureSolution)      :: opressure_solution
    type(VelocityCorrection)    :: ovelocity_correction
    type(StabilizationSolution) :: ostabilization_solution

    if      (equation_name == 'velocity_prediction') then
        call    ovelocity_prediction % assemble_force(force,        &
                                                      variables,    &
                                                      source,       &
                                                      ndofs_in)
    else if (equation_name == 'pressure_solution') then
        call      opressure_solution % assemble_force(force,        &
                                                      variables,    &
                                                      source,       &
                                                      ndofs_in)
    else if (equation_name == 'velocity_correction') then
        call    ovelocity_correction % assemble_force(force,        &
                                                      variables,    &
                                                      source,       &
                                                      ndofs_in)
    else if (equation_name == 'stabilization_solution') then
        call ostabilization_solution % assemble_force(force,        &
                                                      variables,    &
                                                      source,       &
                                                      ndofs_in)
    end if
end subroutine


subroutine get_mass_matrix(mass_matrix_out, &
                           idmass_out,      &
                           nelem_in,        &
                           nnode_elem_in)
    real(8),       intent(out) :: mass_matrix_out(nelem_in*nnode_elem_in*nnode_elem_in)
    integer(4),    intent(out) :: idmass_out(nelem_in*nnode_elem_in*nnode_elem_in, 2)
    integer(4),    intent(in)  :: nelem_in
    integer(4),    intent(in)  :: nnode_elem_in
    call compute_global_mass_matrix(mass_matrix_out, idmass_out)
end subroutine


subroutine get_global_laplacian_matrix(lapla_out, idmat_out, nelem_in, nnode_elem_in)
    real(8),       intent(out) :: lapla_out(nelem_in*nnode_elem_in*nnode_elem_in)
    integer(4),    intent(out) :: idmat_out(nelem_in*nnode_elem_in*nnode_elem_in, 2)
    integer(4),    intent(in)  :: nelem_in
    integer(4),    intent(in)  :: nnode_elem_in
    call compute_global_laplacian_matrix(lapla_out, idmat_out)
end subroutine


subroutine get_levelset_rhs(rhs_out, source_in, nnode_in, ndime_in)
    !
    ! Computes levelset RHS 
    !
    real(8),    intent(out) :: rhs_out(nnode_in)
    real(8),    intent(in)  :: source_in(ndime_in*nnode_in)
    integer(4), intent(in)  :: nnode_in
    integer(4), intent(in)  :: ndime_in
    call compute_levelset_rhs(rhs_out, source_in)
end subroutine


subroutine compute_gauss_lhs_rhs_velocity_prediction(this,           &
                                                     gplhs,          &
                                                     gprhs,          &
                                                     elvar,          &
                                                     elsource,       &
                                                     elevelset,      &
                                                     nnode_in,       &
                                                     ndofs_total_in, &
                                                     ndofs_in,       &
                                                     npast_in,       &
                                                     ielem,          &
                                                     igaus)
  class(VelocityPrediction), intent(in)  :: this
  real(8),                   intent(out) :: gplhs(ndofs_in, nnode_in, ndofs_in, nnode_in)
  real(8),                   intent(out) :: gprhs(ndofs_in, nnode_in)
  real(8),                   intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                   intent(in)  :: elsource(ndofs_total_in, nnode_in)
  real(8),                   intent(in)  :: elevelset(nnode_in)
  integer(4),                intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),                intent(in)  :: ielem, igaus

  call this % compute_gauss_lhs(gplhs,          &
                                elvar,          &
                                elevelset,      &
                                nnode_in,       &
                                ndofs_total_in, &
                                ndofs_in,       &
                                npast_in,       &
                                ielem,          &
                                igaus)
  call this % compute_gauss_rhs(gprhs,          &
                                elvar,          &
                                elsource,       &
                                elevelset,      &
                                nnode_in,       &
                                ndofs_total_in, &
                                ndofs_in,       &
                                npast_in,       &
                                ielem,          &
                                igaus)
end subroutine


subroutine compute_gauss_lhs_rhs_pressure_solution(this,           &
                                                   gplhs,          &
                                                   gprhs,          &
                                                   elvar,          &
                                                   elsource,       &
                                                   elevelset,      &
                                                   nnode_in,       &
                                                   ndofs_total_in, &
                                                   ndofs_in,       &
                                                   npast_in,       &
                                                   ielem,          &
                                                   igaus)
  class(PressureSolution), intent(in)  :: this
  real(8),                 intent(out) :: gplhs(ndofs_in, nnode_in, ndofs_in, nnode_in)
  real(8),                 intent(out) :: gprhs(ndofs_in, nnode_in)
  real(8),                 intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                 intent(in)  :: elsource(ndofs_total_in, nnode_in)
  real(8),                 intent(in)  :: elevelset(nnode_in)
  integer(4),              intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),              intent(in)  :: ielem, igaus

  call this % compute_gauss_lhs(gplhs,          &
                                elvar,          &
                                elevelset,      &
                                nnode_in,       &
                                ndofs_total_in, &
                                ndofs_in,       &
                                npast_in,       &
                                ielem,          &
                                igaus)
  call this % compute_gauss_rhs(gprhs,          &
                                elvar,          &
                                elsource,       &
                                elevelset,      &
                                nnode_in,       &
                                ndofs_total_in, &
                                ndofs_in,       &
                                npast_in,       &
                                ielem,          &
                                igaus)
end subroutine


subroutine compute_gauss_lhs_rhs_velocity_correction(this,           &
                                                     gplhs,          &
                                                     gprhs,          &
                                                     elvar,          &
                                                     elsource,       &
                                                     elevelset,      &
                                                     nnode_in,       &
                                                     ndofs_total_in, &
                                                     ndofs_in,       &
                                                     npast_in,       &
                                                     ielem,          &
                                                     igaus)
  class(VelocityCorrection), intent(in)  :: this
  real(8),                   intent(out) :: gplhs(ndofs_in, nnode_in, ndofs_in, nnode_in)
  real(8),                   intent(out) :: gprhs(ndofs_in, nnode_in)
  real(8),                   intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                   intent(in)  :: elsource(ndofs_total_in, nnode_in)
  real(8),                   intent(in)  :: elevelset(nnode_in)
  integer(4),                intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),                intent(in)  :: ielem, igaus

  call this % compute_gauss_lhs(gplhs,          &
                                elvar,          &
                                elevelset,      &
                                nnode_in,       &
                                ndofs_total_in, &
                                ndofs_in,       &
                                npast_in,       &
                                ielem,          &
                                igaus)
  call this % compute_gauss_rhs(gprhs,          &
                                elvar,          &
                                elsource,       &
                                elevelset,      &
                                nnode_in,       &
                                ndofs_total_in, &
                                ndofs_in,       &
                                npast_in,       &
                                ielem,          &
                                igaus)
end subroutine


subroutine compute_gauss_lhs_rhs_stabilization_solution(this,           &
                                                        gplhs,          &
                                                        gprhs,          &
                                                        elvar,          &
                                                        elsource,       &
                                                        elevelset,      &
                                                        nnode_in,       &
                                                        ndofs_total_in, &
                                                        ndofs_in,       &
                                                        npast_in,       &
                                                        ielem,          &
                                                        igaus)
  class(StabilizationSolution), intent(in)  :: this
  real(8),                      intent(out) :: gplhs(ndofs_in, nnode_in, ndofs_in, nnode_in)
  real(8),                      intent(out) :: gprhs(ndofs_in, nnode_in)
  real(8),                      intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                      intent(in)  :: elsource(ndofs_total_in, nnode_in)
  real(8),                      intent(in)  :: elevelset(nnode_in)
  integer(4),                   intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),                   intent(in)  :: ielem, igaus

  call this % compute_gauss_lhs(gplhs,          &
                                elvar,          &
                                elevelset,      &
                                nnode_in,       &
                                ndofs_total_in, &
                                ndofs_in,       &
                                npast_in,       &
                                ielem,          &
                                igaus)
  call this % compute_gauss_rhs(gprhs,          &
                                elvar,          &
                                elsource,       &
                                elevelset,      &
                                nnode_in,       &
                                ndofs_total_in, &
                                ndofs_in,       &
                                npast_in,       &
                                ielem,          &
                                igaus)
end subroutine


subroutine compute_gauss_lhs_velocity_prediction(this,           &
                                                 gplhs,          &
                                                 elvar,          &
                                                 elevelset,      &
                                                 nnode_in,       &
                                                 ndofs_total_in, &
                                                 ndofs_in,       &
                                                 npast_in,       &
                                                 ielem,          &
                                                 igaus)
  class(VelocityPrediction), intent(in)  :: this
  real(8),                   intent(out) :: gplhs(ndofs_in, nnode_in, ndofs_in, nnode_in)
  real(8),                   intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                   intent(in)  :: elevelset(nnode_in)
  integer(4),                intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),                intent(in)  :: ielem, igaus
  real(8)                                :: elfracvelo(ndime,nnode_in,npast_in)
  real(8)                                :: elvelocity(ndime,nnode_in,npast_in)
  real(8)                                :: elpressure(nnode_in,npast_in)
  integer(4)                             :: inode, jnode, knode, idime, jdime, kdime

  ! Extract element nodal values of velocity & pressure
  elfracvelo = elvar(1:ndime,:,:)
  elpressure = elvar(ndime+1,:,:)
  elvelocity = elvar(ndime+2:2*ndime+1,:,:)

  ! Compute LHS contribution
  gplhs(:,:,:,:) = 0.0_dp
  do inode=1,nnode_elem
    do idime=1,ndime
      do jnode=1,nnode_elem
        !
        ! M -dt * theta * nu * L
        !
        gplhs(idime,inode,idime,jnode) =      &
            gp_shape_shape(igaus,inode,jnode) &
          - dt*theta                          &
          * gp_nu(ielem,igaus)                &
          * gp_dshape_dshape(ielem,igaus,inode,jnode)
        !
        ! +dt * theta * K * V_p^(n) 
        !
        do knode=1,nnode_elem
          do kdime=1,ndime
            gplhs(idime,inode,idime,jnode) =                           &
                gplhs(idime,inode,idime,jnode)                         &
              + dt*theta                                               &
              * gp_skew_symmetric(ielem,igaus,inode,jnode,knode,kdime) &
              * elfracvelo(kdime,knode,iter_k)
          end do
        end do
      end do
    end do
  end do
    
end subroutine


subroutine compute_gauss_lhs_pressure_solution(this,           &
                                               gplhs,          &
                                               elvar,          &
                                               elevelset,      &
                                               nnode_in,       &
                                               ndofs_total_in, &
                                               ndofs_in,       &
                                               npast_in,       &
                                               ielem,          &
                                               igaus)
  class(PressureSolution), intent(in)  :: this
  real(8),                 intent(out) :: gplhs(ndofs_in, nnode_in, ndofs_in, nnode_in)
  real(8),                 intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                 intent(in)  :: elevelset(nnode_in)
  integer(4),              intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),              intent(in)  :: ielem, igaus
  integer(4)                           :: inode, jnode, idof
  real(8)                              :: ks

  ks = 9333333.333333336_dp

  ! Compute LHS contribution
  gplhs = 0.0_dp
  do inode=1,nnode_elem
    do jnode=1,nnode_elem
      !
      ! 1/rho * L
      !
      gplhs(1,inode,1,jnode) =                      &
          gp_dshape_dshape(ielem,igaus,inode,jnode) &
        / gp_rho(ielem,igaus)
    end do
  end do
end subroutine


subroutine compute_gauss_lhs_velocity_correction(this,           & 
                                                 gplhs,          &
                                                 elvar,          &
                                                 elevelset,      &
                                                 nnode_in,       &
                                                 ndofs_total_in, &
                                                 ndofs_in,       &
                                                 npast_in,       &
                                                 ielem,          &
                                                 igaus)
  class(VelocityCorrection), intent(in)  :: this
  real(8),                   intent(out) :: gplhs(ndofs_in, nnode_in, ndofs_in, nnode_in)
  real(8),                   intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                   intent(in)  :: elevelset(nnode_in)
  integer(4),                intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),                intent(in)  :: ielem, igaus
  integer(4)                             :: inode, jnode, idime, jdime

  ! Compute LHS contribution
  gplhs = 0.0_dp
  do inode=1,nnode_elem
    do idime=1,ndime
      do jnode=1,nnode_elem
        !
        ! M 
        !
        gplhs(idime,inode,idime,jnode) = gp_shape_shape(igaus,inode,jnode)
      end do
    end do
  end do
end subroutine


subroutine compute_gauss_lhs_stabilization_solution(this,           &
                                                    gplhs,          &
                                                    elvar,          &
                                                    elevelset,      &
                                                    nnode_in,       &
                                                    ndofs_total_in, &
                                                    ndofs_in,       &
                                                    npast_in,       &
                                                    ielem,          &
                                                    igaus)
  class(StabilizationSolution), intent(in)  :: this
  real(8),                      intent(out) :: gplhs(ndofs_in, nnode_in, ndofs_in, nnode_in)
  real(8),                      intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                      intent(in)  :: elevelset(nnode_in)
  integer(4),                   intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),                   intent(in)  :: ielem, igaus
  integer(4)                                :: inode, jnode, idime, jdime

  ! Compute LHS contribution
  gplhs = 0.0_dp
  do inode=1,nnode_elem
    do idime=1,ndime
      do jnode=1,nnode_elem
        !
        ! M 
        !
        gplhs(idime,inode,idime,jnode) = gp_shape_shape(igaus,inode,jnode)
      end do
    end do
  end do
end subroutine


subroutine compute_gauss_rhs_velocity_prediction(this,           &
                                                 gprhs,          &
                                                 elvar,          &
                                                 elsource,       &
                                                 elevelset,      &
                                                 nnode_in,       &
                                                 ndofs_total_in, &
                                                 ndofs_in,       &
                                                 npast_in,       &
                                                 ielem,          &
                                                 igaus)
  class(VelocityPrediction), intent(in)  :: this
  real(8),                   intent(out) :: gprhs(ndofs_in, nnode_in)
  real(8),                   intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                   intent(in)  :: elsource(ndofs_total_in, nnode_in)
  real(8),                   intent(in)  :: elevelset(nnode_in)
  integer(4),                intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),                intent(in)  :: ielem, igaus
  real(8)                                :: elvelocity(ndime,nnode_in,npast_in)
  real(8)                                :: elfracvelo(ndime,nnode_in,npast_in)
  real(8)                                :: elpressure(nnode_in,npast_in)
  real(8)                                :: elmomentum_source(ndime,nnode_in)
  integer(4)                             :: inode, jnode, knode, idime, kdime

  ! Extract element nodal values of velocity & pressure
  elfracvelo = elvar(1:ndime,:,:)
  elpressure = elvar(ndime+1,:,:)
  elvelocity = elvar(ndime+2:2*ndime+1,:,:)
  ! Extract element nodal values of momentum source
  elmomentum_source = elsource(1:ndime,:)

  ! Compute RHS contribution
  gprhs = 0.0_dp
  do inode=1,nnode_elem
      do idime=1,ndime
          do jnode=1,nnode_elem
              !
              ! M * V^(n)
              !
              gprhs(idime,inode) = gprhs(idime,inode) &
                                 + gp_shape_shape(igaus,inode,jnode) &
                                 * elvelocity(idime,jnode,iter_k)
              !
              ! dt * (1-theta) * mu * L * V_p^(n)
              !
              gprhs(idime,inode) = gprhs(idime,inode) &
                                 + dt*(1.0_dp-theta) &
                                 * gp_nu(ielem,igaus) &
                                 * gp_dshape_dshape(ielem,igaus,inode,jnode) &
                                 * elfracvelo(idime,jnode,iter_k)
              !
              ! - dt * (1-theta) * K * V_p^(n)
              !
              do knode=1,nnode_elem
                  do kdime=1,ndime
                      gprhs(idime,inode) = gprhs(idime,inode) &
                                         - dt*(1.0_dp-theta) &
                                         * gp_skew_symmetric(ielem,igaus,inode,jnode,knode,kdime) &
                                         * elfracvelo(kdime,knode,iter_k) &
                                         * elfracvelo(idime,jnode,iter_k)
                  end do
              end do
              !
              ! dt * gamma / rho * G * P^(n)
              !
              gprhs(idime,inode) = gprhs(idime,inode) &
                                 - dt*gama &
                                 / gp_rho(ielem,igaus) &
                                 * gp_shape_dshape(ielem,igaus,inode,jnode,idime) &
                                 * elpressure(jnode,iter_k)
          end do
      end do
  end do
end subroutine


subroutine compute_gauss_rhs_pressure_solution(this,         &
                                             gprhs,          &
                                             elvar,          &
                                             elsource,       &
                                             elevelset,      &
                                             nnode_in,       &
                                             ndofs_total_in, &
                                             ndofs_in,       &
                                             npast_in,       &
                                             ielem,          &
                                             igaus)
  class(PressureSolution), intent(in)  :: this
  real(8),                 intent(out) :: gprhs(ndofs_in, nnode_in)
  real(8),                 intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                 intent(in)  :: elsource(ndofs_total_in, nnode_in)
  real(8),                 intent(in)  :: elevelset(nnode_in)
  integer(4),              intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in, ielem, igaus
  real(8)                              :: elfracvelo(ndime,nnode_in,npast_in)
  real(8)                              :: elvelocity(ndime,nnode_in,npast_in)
  real(8)                              :: elpressure(nnode_in,npast_in)
  real(8)                              :: elstabilization(ndime,nnode_in,npast_in)
  real(8)                              :: elpressure_source(nnode_in)
  integer(4)                           :: inode, jnode, idime, jdime
  real(8)                              :: ks

  ks = 9333333.333333336_dp

  ! Extract element nodal values of velocity & pressure
  elfracvelo      = elvar(1:ndime,:,:)
  elpressure      = elvar(ndime+1,:,:)
  elvelocity      = elvar(ndime+2:2*ndime+1,:,:)
  elstabilization = elvar(2*ndime+2:,:,:)
  ! Extract element nodal values of pressure source
  elpressure_source = elsource(ndime+1,:)

  ! Compute RHS contribution
  gprhs = 0.0_dp
  idime = 1
  do inode=1,nnode_elem
      do jnode=1,nnode_elem
          !
          ! 1/(dt+tau) * D * V_p^(n+1)  +  tau/(dt+tau) * D * S^(n)
          !
          do jdime=1,ndime
              gprhs(idime,inode) = gprhs(idime,inode) &
                                 + 1.0_dp / (dt + el_tau(ielem)) &
                                 * gp_shape_dshape(ielem,igaus,inode,jnode,jdime) &
                                 * (elfracvelo(jdime,jnode,iter_k) + el_tau(ielem)*elstabilization(jdime,jnode,iter_k))
          end do
          !
          ! dt*gama/(dt+tau) * 1/rho * L * P^(n)
          !
          gprhs(idime,inode) = gprhs(idime,inode) &
                             + dt * gama / (dt + el_tau(ielem)) &
                             / gp_rho(ielem,igaus) &
                             * gp_dshape_dshape(ielem,igaus,inode,jnode) &
                             * elpressure(jnode,iter_k)
          !
          ! dt / rho * M * Psource 
          !
          gprhs(idime,inode) = gprhs(idime,inode) &
                             + gp_shape_shape(igaus,inode,jnode) &
                             * elpressure_source(jnode) &
                             / gp_rho(ielem,igaus)
      end do
  end do
end subroutine


subroutine compute_gauss_rhs_velocity_correction(this,           &
                                                 gprhs,          &
                                                 elvar,          &
                                                 elsource,       &
                                                 elevelset,      &
                                                 nnode_in,       &
                                                 ndofs_total_in, &
                                                 ndofs_in,       &
                                                 npast_in,       &
                                                 ielem,          &
                                                 igaus)
  class(VelocityCorrection), intent(in)  :: this
  real(8),                   intent(out) :: gprhs(ndofs_in, nnode_in)
  real(8),                   intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                   intent(in)  :: elsource(ndofs_total_in, nnode_in)
  real(8),                   intent(in)  :: elevelset(nnode_in)
  integer(4),                intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in, ielem, igaus
  real(8)                                :: elfracvelo(ndime,nnode_in,npast_in)
  real(8)                                :: elvelocity(ndime,nnode_in,npast_in)
  real(8)                                :: elpressure(nnode_in,npast_in)
  integer(4)                             :: inode, jnode, knode, idime

  ! Extract element nodal values of velocity & pressure
  elfracvelo = elvar(1:ndime,:,:)
  elpressure = elvar(ndime+1,:,:)
  elvelocity = elvar(ndime+2:2*ndime+1,:,:)

  ! Compute RHS contribution
  gprhs = 0.0_dp
  do inode=1,nnode_elem
      do jnode=1,nnode_elem
          do idime=1,ndime
              !
              ! M * V_p^(n+1)
              !
              gprhs(idime,inode) = gprhs(idime,inode) &
                                 + gp_shape_shape(igaus,inode,jnode) &
                                 * elfracvelo(idime,jnode,iter_k)
              !
              ! -dt / rho * G * (P^(n) - gamma*P^(n-1)
              !
              gprhs(idime,inode) = gprhs(idime,inode) &
                                 - dt &
                                 / gp_rho(ielem,igaus) &
                                 * gp_dshape_shape(ielem,igaus,inode,jnode,idime) &
                                 * (      elpressure(jnode,iter_k) &
                                   - gama*elpressure(jnode,time_n) )
          end do
      end do
  end do
end subroutine


subroutine compute_gauss_rhs_stabilization_solution(this,           &
                                                    gprhs,          &
                                                    elvar,          &
                                                    elsource,       &
                                                    elevelset,      &
                                                    nnode_in,       &
                                                    ndofs_total_in, &
                                                    ndofs_in,       &
                                                    npast_in,       &
                                                    ielem,          &
                                                    igaus)
  class(StabilizationSolution), intent(in)  :: this
  real(8),                      intent(out) :: gprhs(ndofs_in, nnode_in)
  real(8),                      intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                      intent(in)  :: elsource(ndofs_total_in, nnode_in)
  real(8),                      intent(in)  :: elevelset(nnode_in)
  integer(4),                   intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in, ielem, igaus
  real(8)                                   :: elpressure(nnode_in,npast_in)
  integer(4)                                :: inode, jnode, knode, idime

  ! Extract element nodal values of velocity & pressure
  elpressure = elvar(ndime+1,:,:)

  ! Compute RHS contribution
  gprhs = 0.0_dp
  do inode=1,nnode_elem
    do jnode=1,nnode_elem
      do idime=1,ndime
        !
        ! dt / rho * G * P^(n)
        !
        gprhs(idime,inode) = gprhs(idime,inode) &
                           + 1.0_dp/gp_rho(ielem,igaus) &
                           * gp_dshape_shape(ielem,igaus,inode,jnode,idime) &
                           * elpressure(jnode,iter_k)
      end do
    end do
  end do
end subroutine


subroutine compute_gauss_force_velocity_prediction(this,           &
                                                   gpforce,        &
                                                   elvar,          &
                                                   elsource,       &
                                                   nnode_in,       &
                                                   ndofs_total_in, &
                                                   ndofs_in,       &
                                                   npast_in,       &
                                                   ielem,          &
                                                   igaus)
  class(VelocityPrediction), intent(in)  :: this
  real(8),                   intent(out) :: gpforce(ndofs_in, nnode_in)
  real(8),                   intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                   intent(in)  :: elsource(ndofs_total_in, nnode_in)
  integer(4),                intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),                intent(in)  :: ielem, igaus
  real(8)                                :: elvelocity(ndime,nnode_in,npast_in)
  real(8)                                :: elpressure(nnode_in,npast_in)
  integer(4)                             :: inode, jnode, idime

  ! Load variables
  elpressure = elvar(ndime+1,:,:)
  elvelocity = elvar(ndime+2:2*ndime+1,:,:)

  ! Compute gauss point contribution to force
  gpforce = 0.0_dp
  do inode = 1,nnode_elem
    do idime = 1,ndime
      do jnode = 1,nnode_elem
        gpforce(idime,inode) = gpforce(idime,inode) &
                             - gp_shape_dshape(ielem,igaus,inode,jnode,idime) & ! pressure contribution
                             * elpressure(jnode,iter_k) &
                             + gp_rho(ielem,igaus) &                            ! viscous contribution
                             * gp_nu(ielem,igaus) &
                             * gp_dshape_dshape(ielem,igaus,inode,jnode) &
                             * elvelocity(idime,jnode,iter_k)
      end do
    end do
  end do

end subroutine


subroutine compute_gauss_force_pressure_solution(this,           &
                                                 gpforce,        &
                                                 elvar,          &
                                                 elsource,       &
                                                 nnode_in,       &
                                                 ndofs_total_in, &
                                                 ndofs_in,       &
                                                 npast_in,       &
                                                 ielem,          &
                                                 igaus)
  class(PressureSolution), intent(in)  :: this
  real(8),                 intent(out) :: gpforce(ndofs_in, nnode_in)
  real(8),                 intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                 intent(in)  :: elsource(ndofs_total_in, nnode_in)
  integer(4),              intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in, ielem, igaus
  real(8)                              :: elvelocity(ndime,nnode_in,npast_in)
  real(8)                              :: elpressure(nnode_in,npast_in)
  real(8)                              :: elstabilization(ndime,nnode_in,npast_in)
  real(8)                              :: elpressure_source(nnode_in)
  integer(4)                           :: inode, jnode, idime, jdime

  ! Compute RHS contribution
  gpforce = 0.0_dp
  print*,'IMPLEMENT FORCE CALCULATION!'
end subroutine


subroutine compute_gauss_force_velocity_correction(this,           &
                                                   gpforce,        &
                                                   elvar,          &
                                                   elsource,       &
                                                   nnode_in,       &
                                                   ndofs_total_in, &
                                                   ndofs_in,       &
                                                   npast_in,       &
                                                   ielem,          &
                                                   igaus)
  class(VelocityCorrection), intent(in)  :: this
  real(8),                   intent(out) :: gpforce(ndofs_in, nnode_in)
  real(8),                   intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                   intent(in)  :: elsource(ndofs_total_in, nnode_in)
  integer(4),                intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in, ielem, igaus
  real(8)                                :: elvelocity(ndime,nnode_in,npast_in)
  real(8)                                :: elpressure(nnode_in,npast_in)
  integer(4)                             :: inode, jnode, knode, idime

  ! Compute RHS contribution
  gpforce = 0.0_dp
  print*,'IMPLEMENT FORCE CALCULATION!'
end subroutine


subroutine compute_gauss_force_stabilization_solution(this,           &
                                                      gpforce,        &
                                                      elvar,          &
                                                      elsource,       &
                                                      nnode_in,       &
                                                      ndofs_total_in, &
                                                      ndofs_in,       &
                                                      npast_in,       &
                                                      ielem,          &
                                                      igaus)
  class(StabilizationSolution), intent(in)  :: this
  real(8),                      intent(out) :: gpforce(ndofs_in, nnode_in)
  real(8),                      intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),                      intent(in)  :: elsource(ndofs_total_in, nnode_in)
  integer(4),                   intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in, ielem, igaus
  real(8)                                   :: elpressure(nnode_in,npast_in)
  integer(4)                                :: inode, jnode, knode, idime

  ! Compute RHS contribution
  gpforce = 0.0_dp
  print*,'IMPLEMENT FORCE CALCULATION!'
end subroutine


subroutine compute_natural_bcs_velocity_prediction(this,           &
                                                   bcnat,          &
                                                   source,         &
                                                   nnode_in,       &
                                                   ndofs_total_in, &
                                                   ndofs_in)
    class(VelocityPrediction), intent(in)  :: this
    real(8),                   intent(out) :: bcnat(ndofs_in, nnode_in)
    real(8),                   intent(in)  :: source(ndofs_total_in, nnode_in)
    integer(4),                intent(in)  :: nnode_in, ndofs_total_in, ndofs_in
    bcnat(:,:) = (dt/gp_rho(1,1)) * source(1:ndime,:)
end subroutine


subroutine compute_natural_bcs_pressure_solution(this,           &
                                                 bcnat,          &
                                                 source,         &
                                                 nnode_in,       &
                                                 ndofs_total_in, &
                                                 ndofs_in)
    class(PressureSolution), intent(in)  :: this
    real(8),                 intent(out) :: bcnat(ndofs_in, nnode_in)
    real(8),                 intent(in)  :: source(ndofs_total_in, nnode_in)
    integer(4),              intent(in)  :: nnode_in, ndofs_total_in, ndofs_in
    bcnat(:,:) = 0.0_dp
end subroutine


subroutine compute_natural_bcs_velocity_correction(this,           &
                                                   bcnat,          &
                                                   source,         &
                                                   nnode_in,       &
                                                   ndofs_total_in, &
                                                   ndofs_in)
    class(VelocityCorrection), intent(in)  :: this
    real(8),                   intent(out) :: bcnat(ndofs_in, nnode_in)
    real(8),                   intent(in)  :: source(ndofs_total_in, nnode_in)
    integer(4),                intent(in)  :: nnode_in, ndofs_total_in, ndofs_in
    bcnat(:,:) = 0.0_dp
end subroutine


subroutine compute_natural_bcs_stabilization_solution(this,           &
                                                      bcnat,          &
                                                      source,         &
                                                      nnode_in,       &
                                                      ndofs_total_in, &
                                                      ndofs_in)
    class(StabilizationSolution), intent(in)  :: this
    real(8),                      intent(out) :: bcnat(ndofs_in, nnode_in)
    real(8),                      intent(in)  :: source(ndofs_total_in, nnode_in)
    integer(4),                   intent(in)  :: nnode_in, ndofs_total_in, ndofs_in
    bcnat(:,:) = 0.0_dp
end subroutine


subroutine compute_skew_symmetric_matrix()
  integer(4) :: ielem, igaus, inode, jnode, knode, kdime, jdime
  gp_skew_symmetric = 0.0_dp
  do ielem=1,nelem
    do igaus=1,ngaus
      do inode=1,nnode_elem
        do jnode=1,nnode_elem
          do knode=1,nnode_elem
            do kdime=1,ndime
              !
              ! K_IJK^k = N_i * ( N_K * dN_J/dX^k + 0.5 * N_J * dN_K/dX^k )
              !
              gp_skew_symmetric(ielem,igaus,inode,jnode,knode,kdime) =                   &
                  gp_skew_symmetric(ielem,igaus,inode,jnode,knode,kdime)                 &
                + gp_shape(igaus,inode)                                                  &
                * (     gp_shape(igaus,knode) * gp_dshape_glo(ielem,igaus,jnode,kdime)   &
                  + 0.5_dp*gp_shape(igaus,jnode) * gp_dshape_glo(ielem,igaus,knode,kdime) )
            end do
          end do
        end do
      end do
    end do
  end do
end subroutine


subroutine modify_timestep(dt_in)
    real(8), intent(in) :: dt_in
    call change_timestep(dt_in)
end subroutine


end module
