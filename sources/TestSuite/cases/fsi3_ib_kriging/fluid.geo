// Fluid domain for Turek and Hron's elastic tail benchmark

// Fluid domain
L  = 1.5;
H  = 0.41;
xc = 0.2;
yc = 0.2;
r  = 0.05;

// Solid domain
l   = 0.35;
h   = 0.02;
drb = 0.04898979485566357;
xb  = xc+drb;
yb  = yc-h/2;

// Characteristic sizes
lout = 0.04;
lin  = 0.04;
lb   = 0.02;

// Points
Point(1) = {0.0,  0.0,    0, lin};
Point(2) = {L,    0.0,    0, lout};
Point(3) = {L,    H,      0, lout};
Point(4) = {0.0,  H,      0, lin};
Point(5) = {xc,   yc,     0, lb};
Point(6) = {xb,   yc+h/2, 0, lb};
Point(7) = {xc-r, yc,     0, lb};
Point(8) = {xb,   yc-h/2, 0, lb};

// Virtual points
Point(9)  = {xb+l, yc-h/2, 0, lb};
Point(10) = {xb+l, yc+h/2, 0, lb};

// Lines
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Circle(5) = {6, 5, 7};
Circle(6) = {7, 5, 8};
Line(7) = {8, 6};

// Virtual lines
Line(8) = {8, 9};
Line(9) = {9, 10};
Line(10) = {10, 6};

Field[1] = Attractor;
Field[1].NNodesByEdge = 100;
Field[1].EdgesList = {5, 6, 8, 9, 10};
Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = lb;
Field[2].LcMax = lout;
Field[2].DistMin = 0.04;
Field[2].DistMax = 0.06;
Field[3] = Attractor;
Field[3].NNodesByEdge = 100;
Field[3].EdgesList = {4};
Field[4] = Threshold;
Field[4].IField = 3;
Field[4].LcMin = lin;
Field[4].LcMax = lout;
Field[4].DistMin = 1.0;
Field[4].DistMax = 2.0;
Field[5] = Min;
Field[5].FieldsList = {2,4};
Background Field = 5;

Mesh.CharacteristicLengthExtendFromBoundary = 0;
Mesh.CharacteristicLengthFromPoints = 0;
Mesh.CharacteristicLengthFromCurvature = 0;

// Surfaces
Curve Loop(1) = {1, 2, 3, 4};
Curve Loop(2) = {5, 6, 7};
Plane Surface(1) = {1,2};

// Tags
Physical Point("sw")       = {1};
Physical Point("se")       = {2};
Physical Point("ne")       = {3};
Physical Point("nw")       = {4};
Physical Point("beam1")    = {6};
Physical Point("beam2")    = {7};
Physical Curve("s")        = {1};
Physical Curve("e")        = {2};
Physical Curve("n")        = {3};
Physical Curve("w")        = {4};
Physical Curve("circle")   = {5, 6, 7};
Physical Surface("domain") = {1};
