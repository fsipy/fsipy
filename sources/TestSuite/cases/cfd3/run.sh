#!/bin/bash
#SBATCH --job-name=cfd3short
#SBATCH --output=fsipy_%j.out
#SBATCH --error=fsipy_%j.err
#SBATCH -n 1
#SBATCH -t 24:00:00
###SBATCH --qos=debug
module load python/3-intel-2019.2
module load fabric/1.5.3
python3 /gpfs/projects/bsc21/WORK-DAVID/shared/fsipy/sources/Main.py cfd3 
