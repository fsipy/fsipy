$PhysicalParameters
    physics  fluid                  # problem physics
    material newtonian              # material model
    nu       0.001                  # kinematic viscosity
    rho      1000.0                 # density
    p0       0.0                    # reference pressure
    ndime    2                      # number of spatial dimensions 
$EndPhysicalParameters

$NumericalParameters
    time_coupling      fixed        # ['fixed' | 'from_critical']
    dt                 1e-4         # time step size
    #cfl                0.9         # CFL number (dt=CFL*dt_critical) 
    tinitial           0.0          # initial time
    tfinal             20.0         # max real time
    ntmax              11           # max number of timesteps
    gamma              0.0          # pressure fractional-step parameter [0|1]
    theta              1.0          # theta for time integration scheme
    npast              2            # number of saved trailing solution timesteps
    flag_lump_mass     0            # flag for mass-matrix lumping
    volume_quadrature  Gauss2       # quadrature for volume numerical integration
    surface_quadrature Gauss2       # quadrature for surface numerical integration
$EndNumericalParameters

$InitialConditions
    type uniform                    # type of IC [none|uniform|space_time_function|from_field]
    nparameters 5                   # number of parameters for initial condition
    parameters  0.0 0.0 0.0 0.0 0.0 # initial condition parameters 
$EndInitialConditions

$Sources
$EndSources

$Inputs
    $Subproblems
    $EndSubproblems
    $Sets
    $EndSets
    $Fields
    $EndFields
$EndInputs

$Outputs
    path_outs        reference      # path to outputs directory 
    output_datatype  pkl            # output datatype
    ndump            0              # number of timesteps between outputs
    $InVolume
        velocity
        pressure
    $EndInVolume
    $AtSets
    $EndAtSets
    $AtPhysicalGroups
    $EndAtPhysicalGroups
$EndOutputs
