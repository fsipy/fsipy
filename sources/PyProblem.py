"""
    FsiPy Python Problem Kernel
    ===========================
"""

# External libraries
import os
import sys
import time
import copy
import vtk 
import math
import decimal
import numpy                as np
import numpy.linalg         as la
import scipy                as sp
import scipy.sparse         as ssp
import scipy.sparse.linalg  as ssla
import time                 as ti
import pickle               as pkl
from   abc                  import ABC, abstractmethod
from   mpi4py               import MPI

# Internal libraries
import ddmpy
import aux                  as aux
import pprocess             as pp
import FortranFluid         as FortranFluid
import FortranSolid         as FortranSolid
import FortranSolidImplicit as FortranSolidImplicit
from   PyParameters         import *
from   PyMesh               import *
from   PyEquation           import *
from   PyTiming             import *

# Settings
np.seterr(divide='ignore', invalid='ignore')

# ============================================================================ #
#                             Problem Class                                    #
# ============================================================================ #
class Problem(Timing):
    """ Main Problem class. """
    
    # Initialization
    def __init__(self, d_params, d_mesh, d_bconds, d_parallel, rank):
        
        # Attributes
        self._rank          = rank
        self._d_params      = d_params
        self._o_mesh        = Mesh( d_mesh, d_params['is_parallel'] )
        self._d_bconds      = d_bconds
        self._d_parallel    = d_parallel
        
        # Initializes problem
        self.initialize()


    @Timing()
    def run_simulation(self):
        """ Main routine: runs simulation. """

        # Set initial conditions
        self.set_initial_conditions()
        # Post-process initial conditions
        self.post_process()
        # Time loop
        while (self._tcount < self._ntmax) and (self._time < self._tfinal):
            # Advance single timestep
            self.advance_timestep()
        # Deallocate Fortran module
        self.deallocate()


    @Timing()
    def advance_timestep(self):
        """ Advances single timestep. """

        # Begin timestep
        self.begin_timestep()
        # Loop over equations
        for iequation in range(self._nequations):
            self._niter = 0
            self._error = 1.0e13
            # Begin equation 
            self.begin_equation(iequation)
            # Iterative loop 
            while self.eval_iteration_condition():
                self._niter += 1
                # Begin iteration
                self.begin_iteration(iequation)
                # Do iteration 
                self.do_iteration(iequation)
                # End iteration
                self.end_iteration(iequation)
            # End equation 
            self.end_equation(iequation)
        # End timestep
        self.end_timestep()



    def eval_iteration_condition(self):
        """ Evaluates convergence condition for inner iteration. """

        return (self._niter < self._d_params['nitermax']) and \
               (self._error > self._d_params['tolerance'])



    def begin_timestep(self):
        """ Initialization before loop over equations. """

        self._flag_in_begstep = True
        # Loop over tasks
        for task in self._l_tasks_begin_timestep:
            # Do selected task
            self.do_task(task)
        self._flag_in_begstep = False



    def begin_equation(self, iequation):
        """ Initialization before iterative loop.

            Inputs
            ------
                iequation: int
                    equation index
        """
        self._flag_in_begeqn = True
        # Loop over tasks
        for task in self._l_tasks_begin_equation:
            # Do selected equation task
            self.do_task(task, iequation)
        self._flag_in_begeqn = False



    def begin_iteration(self, iequation):
        """ Initialization of single iteration.

            Inputs
            ------
                iequation: int
                    equation index
        """
        self._flag_in_begiter = True
        # Loop over tasks
        for task in self._l_tasks_begin_iteration:
            # Do selected equation task
            self.do_task(task, iequation)
        self._flag_in_begiter = False



    def do_iteration(self, iequation):
        """ Advances inner iteration for iterative schemes.

            Inputs
            ------
                iequation: int
                    equation index
        """
        self._flag_in_iter = True
        # Loop over tasks
        for task in self._l_tasks_do_iteration:
            # Do selected equation task
            self.do_task(task, iequation)
        self._flag_in_iter = False



    def end_iteration(self, iequation):
        """ Ending of single iteration.

            Inputs
            ------
                iequation: int
                    equation index
        """
        self._flag_in_enditer = True
        # Loop over tasks
        for task in self._l_tasks_end_iteration:
            # Do selected equation task
            self.do_task(task, iequation)
        self._flag_in_enditer = False



    def end_equation(self, iequation):
        """ Ending after iterative loop.

            Inputs
            ------
                iequation: int
                    equation index
        """
        self._flag_in_endeqn = True
        # Loop over tasks
        for task in self._l_tasks_end_equation:
            # Do selected equation task
            self.do_task(task, iequation)
        self._flag_in_endeqn = False



    def end_timestep(self):
        """ Ending after loop over equations. """

        self._flag_in_endstep = True
        # Loop over tasks
        for task in self._l_tasks_end_timestep:
            # Do selected task
            self.do_task(task)
        self._flag_in_endstep = False



    def do_task(self, task, iequation=None, icoupling=None):
        """ Task manager for tasks in each timestep.
            
            Inputs
            ------
                task : str
                    task name
                *iequation : int
                    equation ID
                *icoupling : int
                    coupling ID
        """

        if isinstance(task, str):

            ## Problem tasks
            if   task == 'update_time':
                self.update_time()

            elif   task == 'update_boundary_conditions':
                self.update_boundary_conditions()

            elif task == 'update_time_variables':
                self.update_time_variables()

            elif task == 'update_aux_time_variables':
                self.update_aux_time_variables()

            elif task == 'update_iterative_variables':
                self.update_iterative_variables()

            elif task == 'update_aux_iterative_variables':
                self.update_aux_iterative_variables()

            elif task == 'postprocess':
                self.post_process()

            ## Equation tasks
            elif   task == 'assemble_algebraic_system':
                self.assemble_algebraic_system(iequation)

            elif task == 'impose_boundary_conditions_on_algebraic_system':
                self.impose_boundary_conditions_on_algebraic_system(iequation)

            elif task == 'impose_zero_boundary_conditions_on_algebraic_system':
                self.impose_zero_boundary_conditions_on_algebraic_system(iequation)

            elif task == 'solve_algebraic_system':
                self.solve_algebraic_system(iequation)

            elif task == 'integrate_secondary_variables':
                self.integrate_secondary_variables(iequation)

            elif task == 'impose_boundary_conditions_on_variables':
                self.impose_boundary_conditions_on_variables(iequation)

            elif task == 'check_convergence':
                self.check_convergence(iequation)

            elif task == 'check_conservation':
                self.check_conservation(iequation)

            elif task == 'calculate_reaction':
                self.calculate_reaction(iequation)

            elif task == 'calculate_forces_from_stresses':
                self.calculate_forces_from_stresses(iequation=0)

            elif task == 'update_variables':
                self.update_variables()

            elif   task == 'compute_iteration_error':
                self.compute_iteration_error(iequation)

        ## Coupling tasks
        elif len(task) == 2:

            what_to_do = task[0]
            icoupling  = task[1]
            if  what_to_do == 'receive_from_coupling':
                self.receive_from_coupling( icoupling )

            elif what_to_do == 'send_to_coupling':
                self.send_to_coupling( icoupling )



    def initialize(self):
        """ Initializes Problem object. """

        self.initialize_physical_attributes()
        self.initialize_mesh_attributes()
        self.initialize_numerical_attributes()
        self.initialize_variables()
        self.initialize_equations()
        self.set_dof_fixities()
        self.initialize_material_properties()
        self.compute_timestep_size()
        self.initialize_fortran_module()
        self.initialize_flags()
        self.restart()
        self.print_parameters()
    


    def initialize_physical_attributes(self):
        """ Initializes physical attributes. """

        self._casename                 = self._d_params['casename']
        self._ndime                    = self._d_params['ndime']
        self._ndofs                    = self.get_number_of_dofs()
        self._nequations               = len(self._l_equation_names)
        self._nvars                    = len(self._l_variables_names)
        self._in_coupling              = False
        self._l_icouplings_as_source   = []
        self._l_icouplings_as_target   = []



    def initialize_mesh_attributes(self):
        """ Initializes mesh attributes. """

        # Save main mesh attributes as attributes of Problem
        self._nnode                    = self._o_mesh._nnode
        self._nelem                    = self._o_mesh._nelem
        self._nelem_surf               = self._o_mesh._nelem_surf

        # Get list of node coordinates
        l_node_tags,l_node_coords,_     = self._o_mesh.getNodes()
        l_node_tags                     = list(l_node_tags-1)
        l_node_coords                   = l_node_coords.reshape(self._nnode,-1)[:,0:self._ndime]
        l_node_coords[l_node_tags,:]    = l_node_coords[:,:].copy()
        self._o_mesh._l_node_coords     = l_node_coords
        self._o_mesh._l_node_coords_ale = l_node_coords

        # Load volume elements
        self.get_single_type_element_properties('volume')

        # Load surface elements
        if (self._nelem_surf > 0):
            self.get_single_type_element_properties('surface')

        # Get dimensions of each physical group
        self.get_physical_groups_data()

        # Assure that each boundary node has only 1 physical tag
        self.assign_unique_physical_tags()

        # Get elements for each node
        self._o_mesh.get_connectivities()

        # Find nodes neighboring each element up to given number of levels
        # self.find_node_neighbors_of_elements()



    def initialize_numerical_attributes(self):
        """ Initializes numerical attributes. """

        self._npast                    = self._d_params['npast']
        self._tcount                   = 0
        self._ipass                    = 0
        self._time                     = self._d_params['tinitial']
        self._tfinal                   = self._d_params['tfinal']
        self._ntmax                    = self._d_params['ntmax']
        self._flag_advected_nodes      = False
        self._is_iterative             = False
        self._d_params['is_iterative'] = False
        self._cancel_forces_dirichlet  = False
        self._d_params['ndofs']        = self._ndofs
        self._d_params['nequations']   = self._nequations
        self._d_params['nvars']        = self._nvars



    def initialize_variables(self):
        """ Initializes dict of variables and sources.

        Outputs
        -------
            d_variables : dict containing np-arrays with dims (ndofs_eqn,nnode,npast)or (nnode,npast) for scalars
                problem variables, keys of dictionary are variable names
            d_sources : dict containing np-arrays with dims (ndofs_eqn,nnode) or (nnode) for scalars
                sources associated to each variable
            d_residuals : dict containing np-arrays with dims (ndofs_eqn,nnode) or (nnode) for scalars
                residuals associated to each variable
        """

        # Initialize variable dicts
        self._d_variables = {}
        self._d_sources   = {}
        self._d_residuals = {}

        # Loop over variables
        for ivariable, name in enumerate(self._l_variables_names):

            # Classifies and allocates variables
            variable_dims = self._l_variables_dims[ivariable]

            # Get type of source input and load parameters
            if name in self._d_params['sources']:
                source_type = self._d_params['sources'][name]['type']
                values = self._d_params['sources'][name]['parameters']
            # If not defined set to 0
            else:
                source_type = 'uniform'
                values = 0.0

            # Build source arrays according to configuration
            if source_type == 'uniform':
                self._d_sources[name] = self.build_uniform_sources(variable_dims,name,values)
            elif source_type == 'space_time_function':
                self._d_sources[name] = self.build_sources_from_stf(variable_dims,name,values)
            elif source_type == 'from_field':
                self._d_sources[name] = self.load_sources_from_fields(variable_dims,name,values)

            # Initialize variables to 0 
            if   variable_dims == 'vector':
                self._d_variables[name] = np.zeros((self._ndime, self._nnode, self._npast), float)
            elif variable_dims == 'scalar':
                self._d_variables[name] = np.zeros((self._nnode, self._npast), float)
            else:
                print('Error! Variable type not recognzied.')

            # Initialize residuals to 0 
            if   variable_dims == 'vector':
                self._d_residuals[name] = np.zeros((self._ndime, self._nnode, self._npast), float)
            elif variable_dims == 'scalar':
                self._d_residuals[name] = np.zeros((self._nnode, self._npast), float)
            else:
                print('Error! Variable type not recognzied.')

        # Initialize all other global arrays
        self._force     = np.zeros((self._ndime,self._nnode),float)
        self._level_set = np.zeros(self._nnode, float)

        return self._d_variables, self._d_sources, self._d_residuals



    def initialize_equations(self):
        """ Initializes list of equation objects. """
    
        # Initialize partial update counter
        self.ipartial_update = 0
    
        # Set non-defined BC types to None
        l_bctypes = ['nodes','elements','zero']
        for bctype in l_bctypes:
            if bctype not in self._d_dofs_in_bconds:
                self._d_dofs_in_bconds[bctype] = None
    
        # Loop over equations
        self._l_oequations = []
        for iequation, equation_name in enumerate(self._l_equation_names):
            variable_name = self._l_variables_in_equations[iequation]
            ivariable     = self._l_variables_names.index(variable_name)
            variable_dims = self._l_variables_dims[ivariable]
    
            # Distinguishes between vector and scalar variables
            if   variable_dims == 'vector':
                ndofs = self._ndime
            elif variable_dims == 'scalar':
                ndofs = 1
            else:
                print('Error! Variable type not recognzied.')
    
            # Reads sources
            if len(self._d_params['l_fields']) == 0:
                source = np.zeros((ndofs,self._nnode),float)
            else:
                source = self._d_params['l_fields'][iequation]
    
            # Reads flags
            flag_dynamic_lhs = self._l_flags_dynamic_lhs[iequation]
            flag_for_lumping = self._l_flags_for_lumping[iequation]
    
            # Initializes Equation object
            oequation = Equation(equation_name,
                                 variable_name,
                                 variable_dims,
                                 ndofs,
                                 source,
                                 flag_dynamic_lhs,
                                 flag_for_lumping,
                                 self._d_params,
                                 self._o_mesh,
                                 self._d_dofs_in_bconds,
                                 self._d_parallel)
    
            # Appends object to list of Equations
            self._l_oequations.append(oequation)
    
        return self._l_oequations
    
    def set_dof_fixities(self):
        """ Returns (ndofs,nnode) boolean masks which indicate degrees of
            freedom which are prescribed and those that are free, where ndofs
            is ndime if the primary variable is vectorial and ndofs=1 if it's
            a scalar.
    
            Example: Prescribed DoFs can be extracted from a variable as
                
                prescribed_values = self._d_variables[name][l_dofs_dirichlet,iter_k]
    
            Outputs
            -------
                l_dofs_dirichlet: np-array of bool and dims (ndofs,nnode)
                    True for prescribed nodes, False for free nodes
                l_dofs_free: np-array of bool and dims (ndofs,nnode)
                    False for prescribed nodes, True for free nodes
        """
        self._d_dirichlet_dofs = {}
        self._d_coupling_dofs  = {}
        self._d_free_dofs      = {}
    
        # Loop over equations        
        for equation_name in self._d_bconds:
            # Variable name where we will impose the Dirichlet boundary values
            variable_name  = self._d_dofs_in_bconds[equation_name]
            ivariable      = self._l_variables_names.index(variable_name)
            variable_dims  = self._l_variables_dims[ivariable]
            
            if (variable_dims=='vector'):
                ndofs = self._ndime
            else:
                ndofs = 1
    
            # Initialize boolean arrays
            self._d_dirichlet_dofs[equation_name] = np.zeros( (ndofs,self._nnode), dtype=bool )
            self._d_coupling_dofs[equation_name]  = np.zeros( (ndofs,self._nnode), dtype=bool )
            self._d_free_dofs[equation_name]      = np.ones(  (ndofs,self._nnode), dtype=bool )
                                            
            for iphysical, boundary_condition in self._d_bconds[equation_name]['nodes'].items():
                # Get nodes at physical entity
                l_inodes = boundary_condition['l_inodes']
                for idof in range(ndofs):
                    if boundary_condition['types'][idof] == 1:
                        self._d_free_dofs[equation_name][idof, l_inodes] = False
                        if boundary_condition['function_type'] == 'from_coupling':
                            self._d_coupling_dofs[equation_name][idof, l_inodes]  = True
                        else:
                            self._d_dirichlet_dofs[equation_name][idof, l_inodes] = True
    
            # If nodes are advected then all DoFs except for
            # Dirichlet DoFs are coupling DoFs
    
            # OJO -  ATENCION: HE QUITADO ESTA CONDICION POR EL MOEMENTO 
            if self._flag_advected_nodes:
                self._d_free_dofs[equation_name][:,:]                                       = False
                self._d_coupling_dofs[equation_name][:,:]                                   = True
                self._d_coupling_dofs[equation_name][self._d_dirichlet_dofs[equation_name]] = False


    def initialize_flags(self):
        """ Initializes flags. """

        self._flag_in_begstep = False
        self._flag_in_begeqn  = False
        self._flag_in_begiter = False
        self._flag_in_iter    = False
        self._flag_in_enditer = False
        self._flag_in_endeqn  = False
        self._flag_in_endstep = False



    def restart(self):
        """ Restarts previously saved problem. """

        if self._d_params['flag_load_restart']: self.load_state()



    def get_number_of_dofs(self):
        """ Returns total number of degrees of freedom per node.

        Outputs
        -------
            ndofs : int
                number of degrees of freedom per node
        """

        nvectors = self._l_variables_dims.count('vector')
        nscalars = self._l_variables_dims.count('scalar')
        ndofs = nvectors * self._ndime + nscalars

        return ndofs



    def get_physical_groups_data(self):
        """ Load info of physical groups: dimensionality and tags per boundary. """

        ## Load physical groups
        phys_groups = list(zip(*self._o_mesh.getPhysicalGroups()))
        phys_dims = list(phys_groups[0])
        phys_tags = list(phys_groups[1])
        
        ## Get dimensions for each physical group
        # Loop over primary varibles for each equation        
        for equation_name in self._d_bconds:
            for btype in self._d_bconds[equation_name]:
                for iphysical, boundary_condition in self._d_bconds[equation_name]['nodes'].items():
                    # Check first if the physical tag from the input files is in
                    # the current partition if iphysical in phys_tags:
                    boundary_condition['dim'] = phys_dims[ phys_tags.index(iphysical) ]
        
        ## Get tags of physical groups at boundaries
        ndime_bound         = self._ndime-1
        self._l_phys_bounds = [x for i, x in enumerate(phys_tags) \
                               if phys_dims[i]==ndime_bound]
        self._nbounds       = len(self._l_phys_bounds)



    def assign_unique_physical_tags(self):
        """ Saves list of nodes & elements for each boundary by reading physical
            groups from Gmsh object. Assign a unique physical tag for each
            boundary node for a given equation.
        """

        # Loop over equation with boundary conditions
        for equation_name in self._d_bconds:
            # Initialize the list of all nodes with boundary conditions  
            l_inodes_all = []
            # Loop over type of condition: nodes or element
            for btype in self._d_bconds[equation_name]:
                for iphysical, boundary_condition in self._d_bconds[equation_name][btype].items():
                    # Loop over dimensions
                    for idime in range(self._ndime):                        
                        if boundary_condition['dim'] == idime:

                            # Save nodes
                            l_inodes,_ = self._o_mesh.getNodesForPhysicalGroup(dim=boundary_condition['dim'],
                                                                               tag=iphysical)
                            l_inodes = l_inodes.astype(int) - 1
                            l_inodes = list(set(l_inodes) - set(l_inodes_all))
                            boundary_condition['l_inodes'] = l_inodes
                            l_inodes_all.extend( l_inodes )

                            # Save elements
                            #l_tags = self._o_mesh.getEntitiesForPhysicalGroup(dim=boundary_condition['dim'],
                            #                                                  tag=iphysical)                        
                            #l_ielems_gmsh = []
                            #for tag in l_tags:
                            #    _,l_ielems_in_tag,_ = self._o_mesh.getElements(dim=boundary_condition['dim'],
                            #                                                   tag=tag)
                            #    l_ielems_in_tag = [i - 1 for i in l_ielems_in_tag]
                            #    l_ielems_gmsh.extend(l_ielems_in_tag[0])
                            #boundary_condition['l_ielems_gmsh'] = l_ielems_gmsh
                    
        # Save list of boundary nodes to array
        self._o_mesh._l_inodes_boundary = l_inodes_all



    def get_single_type_element_properties(self, elem_dimensionality):
        """ Loads mesh & quadrature data for elements of a single type (either
            volume or surface elements).

            Inputs
            ------
                elem_dimensionality: str
                    dimensionality of element [volume|surface]
            Outputs
            -------
                Note: If surface element, a "_surf" suffix is added to each attribute name)

                self._o_mesh: Mesh object
                    dumps outputs in self._o_mesh
                self._l_elems: np-array with dims (nelem,nnode_elem)
                    node indices for each element
                self._l_ielems_gmsh: np-array with dims (nelem)
                    element indices for each element according to Gmsh numbering
                    (starts at 1)
                self._nnode_elem: int
                    number of nodes per element.
                self._gp_coords_local: np-array with dims (nelem,ngaus,3)
                    coordinates of gauss points of isoparametric element.
                self._gauss_weights: np-array with dims (ngauss)
                    quadrature integration weights.
                self._ngaus: int
                    number of gauss points per element.
                self._gp_jacobians: np-array with dims (nelem,ngaus,ndime,ndime)
                    jacobian matrices of all elements evaluated at gauss points.
                self._gp_inv_jacobians: np-array with dims (nelem,ngaus,ndime,ndime)
                    inverse of jacobian matrices of all elements evaluated at
                    gauss points.
                self._gp_det_jacobians: np-array with dims (nelem,ngaus)
                    determinants of jac matrices of all elements evaluated
                    at gauss points.
                self._gp_coords: np-array with dims (nelem,ngaus,3)
                    coordinates of gauss points for all elements.
                self._nbasis: int
                    number of shape functions per element.
                self._gp_shape: np-array with dims (ngaus,nbasis)
                    shape functions evaluated at gauss points for isoparametric
                    element.
                self._gp_dshape: np-array with dims (ngaus,nbasis,3)
                    derivatives of shape functions evaluated at gauss points
                    for isoparametric element.
                self._gp_volume: np-array with dims (nelem,ngaus)
                    gauss_weight * gp_det_jacobian, this is the integration volume
                    used to assemble gauss point data to global vectors.
                self._l_elem_lengths: np-array with dims (nelem)
                    characteristic element lengths.
        """

        # Load Gmsh object
        omesh = self._o_mesh._gmsh.mesh
        nnode = self._o_mesh._nnode

        # Select element dimensionality:
        if elem_dimensionality == 'volume':
            # Volume:  ndime_element = ndime_space
            elem_type = self._o_mesh._elem_type
            nelem = self._nelem
            quadrature = self._d_params['volume_quadrature']
            extension = ''
        elif elem_dimensionality == 'surface':
            # Surface: ndime_element = ndime_space - 1
            elem_type = self._o_mesh._elem_type_surf
            nelem = self._nelem_surf
            quadrature = self._d_params['surface_quadrature']
            extension = '_surf'
        else:
            print('Error! Element dimensionality not recognized.')

        # Load node coordinates
        l_node_coords = self._o_mesh._l_node_coords

        # Get number of nodes per element
        _, _, _, nnode_elem, _, _ = self._o_mesh.getElementProperties(elem_type)

        # Get coords of gauss points and gauss weights of isoparametric element
        gp_coords_local, \
        gauss_weights = self._o_mesh.getIntegrationPoints(elem_type, quadrature)

        # Calculate number of gauss points per element
        ngaus = len(gauss_weights)

        # Get jacobians and det of jacobians evaluated at gauss points,
        # and coordinates of gauss points for all volume elements
        gp_jacobians, \
        gp_det_jacobians, \
        gp_coords = self._o_mesh.getJacobians(elem_type, gp_coords_local)
        # Reshape arrays to clearer form
        gp_jacobians = gp_jacobians.reshape(nelem, ngaus, 3, 3)[:, :, 0:self._ndime, 0:self._ndime]
        gp_det_jacobians = gp_det_jacobians.reshape(nelem, ngaus)
        gp_coords = gp_coords.reshape(nelem, ngaus, 3)[:, :, 0:self._ndime]

        # Get shape functions and its derivatives evaluated at gauss points
        _, gp_shape, _ = self._o_mesh.getBasisFunctions(elem_type, gp_coords_local, 'Lagrange')
        _, gp_dshape, _ = self._o_mesh.getBasisFunctions(elem_type, gp_coords_local, 'GradLagrange')
        # Reshape arrays to clearer form
        gp_shape = gp_shape.reshape(ngaus, -1)
        nbasis = gp_shape.shape[1]
        gp_dshape = gp_dshape.reshape(ngaus, nbasis, 3)[:, :, 0:self._ndime]

        # Compute quantities by element:
        #   * gp_volume: "volume" of gauss point (ie: vol = |J| * w )
        #   * gp_inv_jacobians: inverse jacobian matrices of element evaluated at gauss point
        #   * l_elem_lengths: characteristic length of element
        l_ielems_gmsh, l_elems = self.get_elements(elem_dimensionality)
        l_elem_lengths = np.zeros(nelem, float)
        gp_volume = np.zeros(gp_det_jacobians.shape, float)
        gp_inv_jacobians = np.zeros(gp_jacobians.shape, float)
        l_ielems4nodes = [[] for i in range(nnode)]

        # Loop over elements
        for ielem in range(nelem):

            # Compute characteristic element length
            l_inodes = l_elems[ielem, :]
            coords = l_node_coords[l_inodes, :]
            l_elem_lengths[ielem] = self.compute_element_characteristic_length(coords)
            # Loop over gauss points
            for igaus in range(ngaus):
                # Compute gauss point volume
                gp_volume[ielem, igaus] = gp_det_jacobians[ielem, igaus] * gauss_weights[igaus]
                # Compute inverse jacobians at gauss points
                gp_inv_jacobians[ielem, igaus, :, :] = la.inv(gp_jacobians[ielem, igaus, :, :])

            # Build list of elements for each node
            for inode in l_inodes:
                l_ielems4nodes[inode].append(ielem)

        # Reshape gauss point coords to (ngauss,ndime)
        gp_coords_local = gp_coords_local.reshape(ngaus, 3)[:, 0:self._ndime]

        # Return connectivities and element data and save as attributes
        exec('self._o_mesh._l_elems' + extension + ' = l_elems')
        exec('self._o_mesh._l_ielems_gmsh' + extension + ' = l_ielems_gmsh')
        exec('self._o_mesh._l_ielems4nodes' + extension + ' = l_ielems4nodes')
        exec('self._o_mesh._nnode_elem' + extension + ' = nnode_elem')
        exec('self._o_mesh._gp_coords_local' + extension + ' = gp_coords_local')
        exec('self._o_mesh._gauss_weights' + extension + ' = gauss_weights')
        exec('self._o_mesh._ngaus' + extension + ' = ngaus')
        exec('self._o_mesh._gp_jacobians' + extension + ' = gp_jacobians')
        exec('self._o_mesh._gp_inv_jacobians' + extension + ' = gp_inv_jacobians')
        exec('self._o_mesh._gp_det_jacobians' + extension + ' = gp_det_jacobians')
        exec('self._o_mesh._gp_coords' + extension + ' = gp_coords')
        exec('self._o_mesh._nbasis' + extension + ' = nbasis')
        exec('self._o_mesh._gp_shape' + extension + ' = gp_shape')
        exec('self._o_mesh._gp_dshape' + extension + ' = gp_dshape')
        exec('self._o_mesh._gp_volume' + extension + ' = gp_volume')
        exec('self._o_mesh._l_elem_lengths' + extension + ' = l_elem_lengths')



    def get_elements(self, elem_dimensionality):
        """ Returns element-node connectivity of mesh for elements of
            given dimensionality (ie: volume or surface elements).

            Inputs
            ------
                elem_dimensionality: str
                    dimensionality of element ['volume'|'surface'].
            Outputs
            -------
                l_elems: np-array with dims (nelem, nnode_elem)
                    global node IDs for each element.
        """

        # Select element dimensionality:
        if elem_dimensionality == 'volume':
            # Volume:  ndime_element = ndime_space
            elem_type = self._o_mesh._elem_type
            nelem = self._nelem
        elif elem_dimensionality == 'surface':
            # Surface: ndime_element = ndime_space - 1
            elem_type = self._o_mesh._elem_type_surf
            nelem = self._nelem_surf
        else:
            print('Error! Element dimensionality not recognized.')

        # Get element connectivity list
        l_ielems, l_elems = self._o_mesh.getElementsByType(elem_type)

        # Convert indexing to Python format (starting at i=0,1,2,...,nnode)
        l_elems = l_elems - 1

        # Return reshaped list with dims: (nelem,nnode_elem)
        return l_ielems, l_elems.reshape((nelem, -1))



    def get_elements_for_physical_group(self,
                                        dim,
                                        iphysical,
                                        return_global_element_tags=False):
        """ Returns list of elements for a given physical group.

            Inputs
            ------
                dim : int
                    dimension of physical group
                iphysical : int 
                    tag of physical group
                return_global_element_tags : bool
                    if False: returns list of elements with FsiPy indexing ony
                    if True:  returns lists of elements with both FsiPy and
                              Gmsh indexing
            Outputs
            -------
                l_ielems : list of int
                    list of elements in physical group using FsiPy element
                    indexing (i=0,...,nnode-1 or i=0,...,nnode_surf-1)
                l_ielems_gmsh : (optional) list of int
                    list of elements in physical group using Gmsh element
                    indexing (i=1,...,nnode+nnode_surf)
        """

        # Get tags of entities for physical group
        l_tags = self._o_mesh.getEntitiesForPhysicalGroup(dim=dim, tag=iphysical)

        # Get list of elements using Gmsh element indexing
        l_ielems_gmsh = []
        for tag in l_tags:
            _,l_ielems_gmsh_tmp,_ = self._o_mesh.getElements(dim=dim, tag=tag)
            l_ielems_gmsh.extend(list(l_ielems_gmsh_tmp[0]))

        # Transform to FsiPy element indexing
        l_ielems = l_ielems_gmsh - min(self._o_mesh._l_ielems_gmsh_surf)

        # Returns both FsiPy and Gmsh element indices
        if return_global_element_tags:
            return l_ielems, l_ielems_gmsh
        # Returns only FsiPy element indices
        else:
            return l_ielems



    def calculate_normal_to_element(self, ndime, coords):
        """ Calculates normal vector to element face.

        Inputs
        ------
            ndime : int
                number of dimensions of problem
            coords : np-array with dims (nnode_elem,ndime)
                coordinates of element edges
        Outputs
        -------
            normal : np-array with dims (ndime)
                vector normal to element
        """

        if ndime == 0:
            normal = 1.0
        elif ndime == 1:
            normal = np.array([coords[1,1] - coords[0,1],
                               coords[0,0] - coords[1,0]])
            normal /= la.norm(normal)
        elif ndime == 2:
            print('Implement calculate_normal_to_element() for ndime=3')

        return normal



    def compute_element_characteristic_length(self, coords):
        """ Estimates characteristic length of element.

        Inputs
        ------
            coords : np-array with dims (ndime)
                coordinates of element nodes
        Outputs
        -------
            length : float
                characteristic length of element
        """

        # Computes element volume
        volume = self.compute_element_volume(coords)

        # Estimates length from representative cube
        if   self._ndime == 1:
            length = volume
        elif self._ndime == 2:
            length = np.sqrt(volume)
        elif self._ndime == 3:
            length = np.cbrt(volume)

        # Estimates length from representative sphere
        if   self._ndime == 1:
            length = volume
        elif self._ndime == 2:
            length = 2.0 * np.sqrt(volume/np.pi)
        elif self._ndime == 3:
            length = 2.0 * np.cbrt(0.75/np.pi*volume)

        return length



    def compute_element_volume(self, coords):
        """ Computes element volume.

        Inputs
        ------
            coords : np-array with dims (nnode_elem,ndime)
                coordinates of element nodes
        Outputs
        -------
            volume : float
                element volume
        """

        if self._ndime == 1:
            volume = self.compute_element_volume_1d(coords)
        if self._ndime == 2:
            volume = self.compute_element_volume_2d(coords)
        elif self._ndime == 3:
            volume = self.compute_element_volume_3d(coords)

        return volume



    def compute_element_volume_1d(self, coords):
        """ Computes 1D element length.

        Inputs
        ------
            coords : np-array with dims (nnode_elem,ndime)
                coordinates of element nodes
        Outputs
        -------
            length : float
                1D element length
        """

        xmin =  1.0e13
        xmax = -1.0e13
        for inode in range(self._nnode_elem):
            xmin = min(xmin, coords[inode,0])
            xmax = max(xmax, coords[inode,0])
        length = xmax-xmin

        return length



    def compute_element_volume_2d(self, coords):
        """ Computes 2D element area.

        Inputs
        ------
            coords : np-array with dims (nnode_elem,ndime)
                coordinates of element nodes
        Outputs
        -------
            area : float
                2D element area
        """

        x, y = coords[:,0], coords[:,1]
        area = 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))

        return area



    def compute_element_volume_3d(self, coords):
        """ Computes 3D element volume.

        Inputs
        ------
            coords : np-array with dims (nnode_elem,ndime)
                coordinates of element nodes
        Outputs
        -------
            area : float
                2D element area

            See R. Nurberg, ICL:  https://wwwf.imperial.ac.uk/~rn/centroid.pdf
        """
        print('Error! Still need to implement volume computation in 3D.')
        exit()
        return None



    def build_uniform_sources(self, variable_dims, name, values):
        """ Build uniform sources. """

        # If vectorial field:
        if   variable_dims == 'vector':
            values = np.ones(self._ndime) * values
            source = np.ones((self._ndime, self._nnode), float)
            for idime in range(self._ndime): source[idime,:] *= values[idime]

        # If scalar field:
        elif variable_dims == 'scalar':
            source = np.ones((self._nnode), float) * values

        else:
            print('Error! Variable type not recognzied.')

        return source



    def build_sources_from_stf(self, variable_dims, name, values=None):
        print('To-do! Implement build_sources_from_stf.')



    def build_sources_from_fields(self, variable_dims, name, values=None):
        print('To-do! Implement build_sources_from_fields.')



    def update_time(self, flag_external_call=False):
        """ Updates timestep counter and real time. """

        # Compute timestep size for dynamical time-stepping
        # any other timestep related values such as the
        # N-S stabilization intrinsic time
        self.compute_timestep_size()

        if self._in_coupling and not flag_external_call: return

        # Update timestep
        self._tcount += 1
        self._time   += self._d_params['dt']

        # Define max timestep and printing cadency
        if self._tcount == 1:
            self._ntmax = int( np.floor( min( [self._ntmax,
                          self._d_params['tfinal']/self._d_params['dt']] ) ) )
            self._d_params['ntmax'] = self._ntmax
            self._nprint = int(self._ntmax/100)
        # Print timestep
        if (not self._in_coupling) and \
           ((self._ntmax <= 101) or \
           (np.mod(self._tcount, self._nprint) == 0)):
            if MPI.COMM_WORLD.Get_rank() == 0:
                print("{:<18} {:<10} {:<8}".format('Beginning timestep',
                                                   str(self._tcount),
                                                   '('+str(round(self._tcount/(self._ntmax)*100,1))+'%)'))


    #@aux.timeit
    def assemble_algebraic_system(self, iequation):
        """ Assembles LHS and RHS for the iequation. """

        a_variables, a_sources = self.variables_dict2array()
        oequation              = self._l_oequations[iequation]

        oequation.assemble_algebraic_system(a_variables,
                                            a_sources,
                                            self._level_set,
                                            self._tcount,
                                            self._is_iterative)



    # @aux.timeit
    def solve_algebraic_system(self, iequation):
        """ Solves the algebraic iequation. """

        oequation = self._l_oequations[iequation]
        variable_name = oequation._variable_name
        variable_dims = oequation._variable_dims

        if variable_dims == 'vector':
            variable = self._d_variables[variable_name][:, :, iter_k]
        else:
            variable = self._d_variables[variable_name][:, iter_k]

        if self._is_iterative:
            variable = oequation.iterate(variable)
        else:
            variable = oequation.solve(self._d_params['is_parallel'])

        if variable_dims == 'vector':
            self._d_variables[variable_name][:, :, iter_k] = variable
        else:
            self._d_variables[variable_name][:, iter_k] = variable


    #@aux.timeit
    def update_boundary_conditions(self):
        """ Updates boundary conditions in time. """

        for equation_name in self._d_bconds:    
            for btype in self._d_bconds[equation_name]:
                for iphysical, boundary_condition in self._d_bconds[equation_name][btype].items():
                    # Constant BC
                    if  boundary_condition['function_type'] == 'constant':
                        boundary_condition['values'] = boundary_condition['factors']

                    # Time function BC
                    elif boundary_condition['function_type'] == 'time_function':
                        ndofs_fcn                    = len(boundary_condition['function'])
                        function                     = np.array([boundary_condition['function'][idof](self._time) for idof in range(ndofs_fcn)])
                        boundary_condition['values'] = boundary_condition['factors'] * function

                    # Space-time function BC
                    elif boundary_condition['function_type'] == 'space-time_function':
                        l_inodes         = boundary_condition['l_inodes']
                        coords           = self._o_mesh._l_node_coords[l_inodes,:]
                        nnode            = len(l_inodes)
                        coords           = coords.reshape((nnode,-1))
                        coords           = np.concatenate((coords, np.zeros((nnode,3-self._ndime),float)),axis=1)
                        ndofs_fcn        = len(boundary_condition['function'])
                        ndofs            = len(boundary_condition['types'])
                        function         = np.zeros(ndofs_fcn,float)
                        boundary_condition['values']  = np.zeros((nnode,ndofs),float)
                        for inode_loc in range(nnode):
                            for idof in range(ndofs_fcn):
                                function[idof] = boundary_condition['function'][idof](self._time,
                                                                                      coords[inode_loc,0],
                                                                                      coords[inode_loc,1],
                                                                                      coords[inode_loc,2])
                            boundary_condition['values'][inode_loc,:] = boundary_condition['factors'] * function
                    elif boundary_condition['function_type'] == 'from_coupling':
                        continue

                    else:
                        print('Error! Could not recognize BC function type.')



    def impose_zero_boundary_conditions_on_algebraic_system(self, iequation):

        oequation = self._l_oequations[iequation]
        oequation.impose_zero_boundary_conditions_on_algebraic_system(self._d_bconds,
                                                                      self._o_mesh,
                                                                      self._d_dofs_in_bconds,
                                                                      self._d_variables,
                                                                      self._l_variables_names,
                                                                      self._l_variables_types,
                                                                      self._is_iterative,
                                                                      self._d_params['is_parallel'],
                                                                      self._d_parallel)



    #@aux.timeit
    def impose_boundary_conditions_on_algebraic_system(self, iequation):
        """ Imposes boundary conditions in LHS and RHS of iequation. """

        oequation = self._l_oequations[iequation]
        oequation.impose_boundary_conditions_on_algebraic_system(self._d_bconds,
                                                                 self._o_mesh,
                                                                 self._d_dofs_in_bconds,
                                                                 self._d_variables,
                                                                 self._l_variables_names,
                                                                 self._l_variables_types,
                                                                 self._is_iterative,
                                                                 self._d_params['is_parallel'],
                                                                 self._d_parallel)



    def impose_boundary_conditions_on_variables(self, iequation):
        """ Imposes boundary conditions directly on variables of the secondary
            type.
        """
        oequation = self._l_oequations[iequation]
        self._d_variables, \
        self._d_sources    = oequation.impose_boundary_conditions_on_variables(\
                                 self._d_bconds,
                                 self._o_mesh,
                                 self._d_variables,
                                 self._d_sources,
                                 self._d_dofs_in_bconds,
                                 self._is_iterative)



    def impose_variable_externally(self, variable_name, l_idofs, values, iequation=None):
        #if isinstance( iequation, int ):
        self._d_variables[variable_name][l_idofs,iter_k] = values[l_idofs]



    def impose_boundary_condition_from_coupling(self, values, variable_name):

        for equation_name in self._d_bconds:    
            for iphysical, boundary_condition in self._d_bconds[equation_name]['nodes'].items():
                if boundary_condition['function_type'] == 'from_coupling':
                    # Get nodes at physical entity
                    l_inodes                     = boundary_condition['l_inodes']
                    nnode                        = len(l_inodes)
                    ndofs                        = len(boundary_condition['types'])
                    boundary_condition['values'] = np.zeros((nnode,ndofs),float)
                    for idof in range(ndofs):
                        if boundary_condition['types'][idof] == 1:
                            boundary_condition['values'][:,idof] = values[idof,l_inodes]
                            boundary_condition['variable_name']  = variable_name



    def variables_dict2list(self):
        """ Convert dict of variables to list. """

        return [self._d_variables[k] for k in self._l_variables_names]



    def variables_dict2array(self):
        """ Convert dict of variables and sources to numpy array. """

        a_variables = np.zeros((self._ndofs, self._nnode, self._npast), float)
        a_sources   = np.zeros((self._ndofs, self._nnode), float)
        idof = 0
        for vname,vtype in zip(self._l_variables_names, self._l_variables_dims):
            if   (vtype == 'vector'): ndofs = self._ndime
            elif (vtype == 'scalar'): ndofs = 1
            a_variables[idof:idof+ndofs,:,:] = self._d_variables[vname]
            a_sources[idof:idof+ndofs,:]     = self._d_sources[vname]
            idof += ndofs

        return a_variables, a_sources



    #@aux.timeit
    def post_process(self, flag_external_call=False, first_output=False):
        """ Post-processes data. """

        # Avoid 1st data dump if loading from restart
        flag_break = (first_output      and self._d_params['flag_load_restart']) \
                  or (self._in_coupling and not flag_external_call)
        if flag_break: return

        if self.eval_dump_condition():

            dtype = self._d_params['output_datatype']

            # Save data to vtk format
            if   dtype == 'vtk':
                pp.save2vtk(self._d_params['path_outs'],
                            self._d_params['casename'],
                            self._o_mesh._l_node_coords,
                            self._o_mesh._l_elems,
                            self._o_mesh._elem_type,
                            self.build_dict_of_fields_for_pprocess(),
                            self._d_params['l_variables_in_volume'],
                            self._tcount,
                            self._time)

            # Save data to pickle format 
            elif dtype == 'pkl':
                self.save2pkl()

            # Save quantities stored as single values for each timestep
            self.save2txt()

        # Save current state for restart
        self.save_state()



    def eval_dump_condition(self):
        """ Evaluates condition for dumping output data. """

        ndump     = self._d_params['ndump']
        flag_save = ( ndump > 0 ) and ( np.mod(self._tcount, ndump) == 0 )

        return flag_save



    def build_dict_of_fields_for_pprocess(self):
        """ Builds dict of fields to be post-processed, that is,
            variables of problem and fields which may be saved
            directly as attributes of Problem. """

        # Build dict of fields
        d_vars = self._d_variables.copy()
        var_names = self._d_params['l_variables_in_volume']
        for var_name in var_names:

            # if field is variable of Problem
            if var_name in self._d_variables:
                field = self._d_variables[var_name]
                if np.ndim(field) == 2:
                    field = field[:,time_n]
                else:
                    field = field[:,:,time_n]
                d_vars[var_name] = field

            # if field is attribute of Problem
            if hasattr(self,'_'+var_name):
                field = eval('self._'+var_name)
                if len(field) > self._nnode:
                    field = field.reshape((self._ndime,-1),order='F')
                shap = field.shape
                if len(shap) > 1:
                    if shap[0] > shap[1]: field = field.T
                d_vars[var_name] = field

            # if field is attribute of Problem
            if len(var_name) > 8:
                sou_name = var_name[0:-7]
                if sou_name in self._d_sources:
                    field = self._d_sources[sou_name]
                    if np.ndim(field) == 1:
                        field = field[:]
                    else:
                        field = field[:,:]
                    d_vars[var_name] = field

        return d_vars



    def save2pkl(self):
        """ Post-processes data to pickle format. """

        comm = MPI.COMM_WORLD

        if self._d_params['nvariables_in_volume'] == 0: return

        if comm.Get_size() > 1:
            filename = os.path.join(self._d_params['path_outs'], \
                                    self._d_params['casename']+'_'+str(self._rank)+'.pkl')
        else:
            filename = os.path.join(self._d_params['path_outs'], \
                                    self._d_params['casename']+'.pkl')

        with open(filename, 'wb') as f:
            pkl.dump(self._d_variables, f)



    def save2txt(self):
        """ Saves data to text file. """
        
        self.save_fields_at_physicals2txt()
        self.save_fields_at_sets2txt()
        self.save_fields_at_witness_points2txt()



    def save_fields_at_witness_points2txt(self):

        if (self._d_params['nwitness_points'] == 0) \
        or (self._d_params['nfields_witness_points'] == 0): return
        # Loop over fields
        for field_name in self._d_params['l_fields_witness_points']:
            # Load field
            if hasattr(self, '_'+field_name):
                field = eval('self._'+field_name)
            elif field_name in self._d_variables:
                field = self._d_variables[field_name]
                ivariable     = self._l_variables_names.index(field_name)
                variable_dims = self._l_variables_dims[ivariable]
                if variable_dims == 'vector': field = field[:,:,time_n]
                if variable_dims == 'scalar': field = field[:,time_n]
            else: continue
            # Loop over witness points
            for iwitpoint, coords in enumerate(self._d_params['l_witness_points'], 1):
                # Interpolate field to witness point
                value = self._o_mesh.interpolateFieldToPoint(field, coords)
                # Save to txt file
                if self._tcount > 0: mode='a'
                else:                mode='wb'
                filename = os.path.join(self._d_params['path_outs'], \
                                        self._d_params['casename']   \
                                      + '.wp' + '%02d' % iwitpoint   \
                                      + '.' + field_name + '.out')
                with open(filename, mode) as f:
                    np.savetxt(f,
                               [np.append([self._time], value)],
                               fmt='%1.7e', delimiter=' ')


    def save_fields_at_sets2txt(self):

        if (self._d_params['nvariables_at_sets'] == 0): return

        for field_name in self._d_params['l_variables_at_sets']:
            print('To-do! Implement post-processing at sets.')



    def save_fields_at_physicals2txt(self):

        if (self._d_params['nvariables_at_physicals'] == 0): return
        # Loop over fields
        for field_name in self._d_params['l_variables_at_physicals']:
            # If field is attribute of Problem => sum over boundaries 
            if hasattr(self, '._'+field_name):
                field = eval('self._'+field_name)
                if np.ndim(field) == 3: field = field[:,:,time_n]

                # Loop over all physical entities
                for dim, iphysical in self._o_mesh.getPhysicalGroups():
                    # Only get physical groups of codimension > 0
                    if dim == self._ndime: continue
                    # Get nodes at physical entity
                    l_inodes,_ = self._o_mesh.getNodesForPhysicalGroup(dim=dim,
                                                                       tag=iphysical)
                    l_inodes = l_inodes.astype(int) - 1
                    # Sum values at variables
                    field_at_phys = np.sum(field[:,l_inodes],axis=1)
                    # Save to txt file
                    if self._tcount > 0: mode='a'
                    else:                mode='wb'
                    filename = os.path.join(self._d_params['path_outs'], \
                                            self._d_params['casename'] + \
                                            '.physical' + '%02d'%iphysical + \
                                            '.out')
                    with open(filename, mode) as f:
                        np.savetxt(f,
                                   [np.append([self._time],field_at_phys)],
                                   fmt='%1.7e', delimiter=' ')
            # If field is stress => Integrate over boundaries
            elif field_name in ['stress']:
                stress = self.compute_stresses_at_boundaries()
                for iboun, iphysical in enumerate(self._l_phys_bounds):
                    field_at_phys = stress[iboun,:]
                    # Save to txt file
                    if self._tcount > 0: mode='a'
                    else:                mode='wb'
                    filename = os.path.join(self._d_params['path_outs'], \
                                            self._d_params['casename'] + \
                                            '.' + field_name + \
                                            '.physical' + '%02d'%iphysical + \
                                            '.out')
                    with open(filename, mode) as f:
                        np.savetxt(f,
                                   [np.append([self._time],field_at_phys)],
                                   fmt='%1.7e', delimiter=' ')
                


    def par_array_exchange(self,array):
        """ Parallel subdomain array exchange. """

        comm = MPI.COMM_WORLD
        if comm.Get_size() > 1:
           # For 1-dimensional arrays
           if np.ndim(array) == 1:
               d_idofs_interface_I = self._d_parallel['nodal_interface_exchange']
               for jsubdomain, l_inodes in d_idofs_interface_I.items():
                   sendbuff = array[l_inodes]
                   recvbuff = np.zeros(sendbuff.shape, float)
                   comm.Send([sendbuff, MPI.DOUBLE], dest=jsubdomain, tag=13)
                   comm.Recv([recvbuff, MPI.DOUBLE], source=jsubdomain, tag=13)
                   array[l_inodes] += recvbuff
           # For 2-dimensional arrays
           elif np.ndim(array) == 2:
               for idime in range(self._ndime):
                   array[idime,:] = self.par_array_exchange(array[idime,:])

        return array



    def copy_variables(self, idx_copy, idx_paste, iequation=-1):
        """ Copies value of variables in index idx_copy to index idx_paste:

            v[:,:,idx_paste] = v[:,:,idx_copy]

        """
        # Build list of variables to be modified
        if iequation > 0:
            equation_name = self._l_equation_names[iequation]
            l_variables_names = [self._l_variables_in_equations[iequation]]
        else:
            l_variables_names = self._l_variables_names

        # Loop over list of variables
        for name in l_variables_names:
            ivariable     = self._l_variables_names.index(name)
            variable_dims = self._l_variables_dims[ivariable]

            # Copy variables at idx_copy to idx_paste
            if   variable_dims == 'vector':
                self._d_variables[name][:,:,idx_paste] = self._d_variables[name][:,:,idx_copy]
            elif variable_dims == 'scalar':
                self._d_variables[name][:,idx_paste]   = self._d_variables[name][:,idx_copy]
            else:
                print('Error! Variable type not recognzied.')



    def update_iterative_variables(self, iequation=-1):
        """ Sets iterative variable to be the last timestep's value 
            at the beggining of an iterative scheme.

            v[:,:,iter_k] = v[:,:,time_n]
        """
        self.copy_variables(time_n, iter_k, iequation)



    def update_aux_iterative_variables(self, iequation=-1):
        """ Sets auxiliary iterative variable to be the last value
            of the iterative variable.

            v[:,:,iter_aux] = v[:,:,iter_k]
        """
        self.copy_variables(iter_k, iter_aux, iequation)



    def update_time_variables(self, iequation=-1):
        """ Sets current variable to be the last value of the iterative
            variable at the end of an iterative scheme.

            v[:,:,time_n] = v[:,:,iter_k]
        """
        self.copy_variables(iter_k, time_n, iequation)



    def update_aux_time_variables(self, iequation=-1):
        """ Sets auxiliary time variable to be the last value
            of the time variable.

            v[:,:,time_aux] = v[:,:,time_n]
        """
        self.copy_variables(time_n, time_aux, iequation)



    def find_wet_elements(self, l_points):
        """ Finds list of element IDs for a given list of points in space.

            Inputs:
            -------
                l_points : np-array with dims (npoin_in, ndime)
                    given list of points in space which define wet elements.
            Outputs:
            --------
                l_wet_elems: list
                    list with all elements containing l_points.
        """

        npoin       = l_points.shape[0]
        coords      = np.zeros(3,float)
        l_wet_elems = []
        # loop over points in list
        for ipoin in range(npoin):
            # extracts point coordinate
            coords[0:self._ndime] = l_points[ipoin,:]
            # returns list of element(s) containing point
            ielem = self._o_mesh.getElementByCoordinates(coords[0],
                                                         coords[1],
                                                         coords[2],
                                                         dim=self._ndime)

            # If cannot find element => contine loop
            if ielem == None: continue

            ielem = ielem - 1
            # appends element(s) to global list
            l_wet_elems.append([ielem])
        # returns non-repeated list of wet elements
        return list( set( l_wet_elems ) )


    
    def calculate_reaction(self,iequation=-1):
        """ Computes algebraic reaction force for all Equations.
        """

        # If only 1 equation in Problem => save reaction as attr of Problem
        if (self._nequations == 1):
            oequation = self._l_oequations[0]
            self._reaction = oequation.calculate_reaction(self._d_variables,
                                                          self._is_iterative)

        # If >1 equation in Problem => calculate reaction for iequation and save
        #                              as attribute of Equation 
        else:
            oequation = self._l_oequations[iequation]
            oequation.calculate_reaction(self._d_variables,
                                         self._is_iterative)



    def calculate_forces_from_stresses(self, iequation=None):
        """ Computes body force from stress tensor for all Equations.
        """

        a_variables, a_sources = self.variables_dict2array()

        # If only 1 equation in Problem => save reaction as attr of Problem
        if (self._nequations == 1): iequation = 0

        if isinstance( iequation, int ):
            oequation   = self._l_oequations[iequation]
            self._force = oequation.calculate_force_from_stress(a_variables,
                                                                a_sources,
                                                                self._is_iterative)
            # Remove force contribution from Dirichlet degrees of freedom
            if self._cancel_forces_dirichlet:
                 self._force[ self._d_dirichlet_dofs[oequation._name] ] = 0.0

        # If >1 equation in Problem => save reaction as attr of each Equation
        else:
            for oequation in self._l_oequations:
                oequation.calculate_force_from_stress(a_variables,
                                                      a_sources,
                                                      self._is_iterative)



    def activate_iterative(self):
        self._is_iterative = True
        self._d_params['is_iterative'] = True



    def activate_in_coupling(self, l_dcouplings):
        # Activate flag
        self._in_coupling = True
        # Save couplings dictionary to use as legend
        self._l_dcouplings = l_dcouplings
        # Initialize coupling variables
        self._d_coupling_variables = {}



    def modify_timestep(self, dt):
        # Modify timestep in Python
        self._d_params['dt'] = dt
        # Modify timestep in Fortran kernel
        module_name = self._d_params['module_name']
        exec(module_name+'.'+module_name.lower()+'.modify_timestep(dt)')



    def compute_trapezoidal_rule(self, l_dofs):
        """ Computes trapezoidal rule increment for Quasi-Newton implicit
            scheme as:
            
            g(k) = X(k) - X(n) + dt/2*( U(k) + U(n) )

        """
        # Load displacements and velocities
        dt = self._d_params['dt']
        displ_iter = self._d_variables['displacement'][l_dofs,iter_k]
        displ_time = self._d_variables['displacement'][l_dofs,time_n]
        veloc_iter = self._d_variables['velocity'][l_dofs,iter_k]
        veloc_time = self._d_variables['velocity'][l_dofs,time_n]

        # Compute trapezoidal rule
        g = np.zeros((self._ndime,self._nnode),float)
        g[l_dofs] = displ_iter - displ_time - dt/2.*(veloc_iter + veloc_time)

        # Return single column vector
        return g.reshape((-1),order='F')



    def compute_iteration_error(self, iequation):
        """ Compute error for iteration as:
            
            Error = ||u(n+1,k+1) - u(n+1,k+1)|| / ||u(n+1,k)||

        """
        variable_name = self._l_variables_in_equations[iequation]
        ivariable     = self._l_variables_names.index(variable_name)
        variable_dims = self._l_variables_dims[ivariable]
        variable = self._d_variables[variable_name]
        if   variable_dims == 'vector':
            norm_iterk = la.norm(variable[:,:,iter_k])
            if norm_iterk < 1e-13:
                self._error = la.norm(variable[:,:,iter_k] - variable[:,:,iter_aux])
            else:
                self._error = la.norm(variable[:,:,iter_k] - variable[:,:,iter_aux]) \
                            / norm_iterk
        elif variable_dims == 'scalar':
            norm_iterk = la.norm(variable[:,iter_k])
            if norm_iterk < 1e-13:
                self._error = la.norm(variable[:,iter_k] - variable[:,iter_aux])
            else:
                self._error = la.norm(variable[:,iter_k] - variable[:,iter_aux]) \
                            / norm_iterk 



    def setup_source(self, dcoupling):
        """ Initialize myself as a source in a given coupling. """

        # Setup stages in which Problem sends to Coupling
        icoupling = dcoupling['icoupling']
        stage     = dcoupling['send'][0]
        order     = dcoupling['send'][1]
        if   order == 'before': iplace = 0
        elif order == 'after':  iplace = 1000

        task = ('send_to_coupling', icoupling)

        # Add tasks in defined moments (iplace) of Problem execution
        if stage == 'begin_timestep':
            self._l_tasks_begin_timestep.insert(iplace, task)
        if stage == 'end_timestep':
            self._l_tasks_end_timestep.insert(iplace, task)
        if stage == 'begin_equation':
            self._l_tasks_begin_iteration.insert(iplace, task)
        if stage == 'end_equation':
            self._l_tasks_end_iteration.insert(iplace, task)
        if stage == 'begin_iteration':
            self._l_tasks_begin_iteration.insert(iplace, task)
        if stage == 'end_iteration':
            self._l_tasks_end_iteration.insert(iplace, task)

        # Tell me in which couplings I'm a source
        self._l_icouplings_as_source.append( dcoupling['icoupling'] ) 

        # Initialize send variable
        self._d_coupling_variables[(icoupling,'send')] = np.zeros((self._ndime,self._nnode),'float') 



    def setup_target(self, dcoupling):
        """ Initialize myself as a target in a given coupling. """

        # Setup stages in which Problem receives from Coupling
        icoupling = dcoupling['icoupling']
        stage   = dcoupling['recv'][0]
        order   = dcoupling['recv'][1]
        if   order == 'before': iplace = 0
        elif order == 'after':  iplace = 1e3

        task = ('receive_from_coupling', icoupling)

        if stage == 'begin_timestep':
            self._l_tasks_begin_timestep.insert(iplace, task)
        if stage == 'end_timestep':
            self._l_tasks_end_timestep.insert(iplace, task)
        if stage == 'begin_equation':
            self._l_tasks_begin_iteration.insert(iplace, task)
        if stage == 'end_equation':
            self._l_tasks_end_iteration.insert(iplace, task)
        if stage == 'begin_iteration':
            self._l_tasks_begin_iteration.insert(iplace, task)
        if stage == 'end_iteration':
            self._l_tasks_end_iteration.insert(iplace, task)

        # Tell me in which couplings I'm a target 
        self._l_icouplings_as_target.append( icoupling )

        # Initialize receive variable
        self._d_coupling_variables[(icoupling,'recv')] = np.zeros((self._ndime,self._nnode),'float')

        # Initialize boundary conditions if necessary
        self.receive_from_coupling( icoupling )




    def receive_from_coupling(self, icoupling):
        """ Receives data from coupling buffer. """

        dcoupling     = self._l_dcouplings[icoupling]
        action        = dcoupling['action']
        variable_name = dcoupling['variable']
        equation_name = dcoupling['equation']
        where         = dcoupling['where']

        if   action == 'interpolate':
            self.interpolate_variable(variable_name, icoupling, 'recv', where)

        elif action == 'spread':
            self.spread_variable(variable_name, icoupling, 'recv', equation_name)




    def send_to_coupling(self, icoupling):
        """ Sends data to coupling buffer. """

        dcoupling     = self._l_dcouplings[icoupling]
        action        = dcoupling['action']
        variable_name = dcoupling['variable']
        equation_name = dcoupling['equation']

        if   action == 'interpolate':
            self.interpolate_variable(variable_name, icoupling, 'send')

            
        elif action == 'spread':
            self.spread_variable(variable_name, icoupling, 'send', equation_name)



    def interpolate_variable(self, variable_name, icoupling, what_to_do, where=None):
        """ Interpolates variable, whether if I'm sending or receiving. """

        #if isinstance(where, tuple):
        #    code  = where[1] # Boundary code for selective interpolation
        #    where = where[0] # Indicator if coupling is on 'whole_mesh' or on 'code'

        # Get type of coupling (ie: dirichlet, neumann, body-force, etc..)
        dcoupling = self._l_dcouplings[icoupling]
        type_coupling = dcoupling['type']
        equation_name = dcoupling['equation']
        variable_name = dcoupling['variable']


        # Send variable to list of coupling variables
        if (what_to_do == 'send'):

            # Send value of variable
            if type_coupling == 'dirichlet':
                self._d_coupling_variables[(icoupling, what_to_do)] = \
                        self._d_variables[variable_name][:,:,iter_k]

            # Send tractions
            elif type_coupling == 'neumann':
                self._d_coupling_variables[(icoupling, what_to_do)] = \
                        self._force.reshape((self._ndime,-1),order='F')


        # Receive variable from interpolation
        elif (what_to_do == 'recv'):

            # Impose on coupled degrees of freedom
            if type_coupling == 'dirichlet':
                # Set on BCs
                self.impose_boundary_condition_from_coupling( \
                        self._d_coupling_variables[(icoupling, what_to_do)], variable_name)
                # Set on variables directly (TODO: Eliminate this line)
                self.impose_variable_externally(variable_name, self._d_coupling_dofs[equation_name],
                        self._d_coupling_variables[(icoupling, what_to_do)])

            # Impose on tractions of coupled degrees of freedom 
            elif type_coupling == 'neumann':
                # Set on BCs
                # TODO: change 'displacement' from keyword coming from eqn_name or var_name
                self._d_sources['acceleration'] = \
                    self._d_coupling_variables[(icoupling, what_to_do)]
                # print('Solid receiving <F> =',np.mean(self._d_sources['acceleration'],axis=1)) 



    def spread_variable(self, variable_name, icoupling, what_to_do, equation_name=None):
        """ Spreads variable from patch to background, whether if I'm sending or receiving. """

        if (what_to_do == 'send'):
            self._d_coupling_variables[(icoupling, what_to_do)] = \
                self._force.reshape((self._ndime,-1),order='F')

        elif (what_to_do == 'recv'):
            self._d_sources[equation_name] = \
                self._d_coupling_variables[(icoupling, what_to_do)]



    def assemble_laplacian_matrix(self):
        """ Computes global laplacian matrix (used for calculating level-set).

        Outputs
        -------
            self._lapla : CSR sparse matrix with dims (nnode,nnode)
                Laplacian matrix
            self._idlapla : np-array with dims (nentries,2)
                sparse matrix entry IDs
         """

        # Calculate laplacian matrix Fortran Module
        module_name = self._d_params['module_name']
        self._lapla_tmp   = []
        self._idlapla_tmp = []
        str_expr = 'self._lapla_tmp, self._idlapla_tmp = ' \
            + module_name + '.' + module_name.lower() \
            + '.get_global_laplacian_matrix(self._nelem, self._o_mesh._nnode_elem)'
        exec(str_expr)

        # Save laplacian to SSP sparse format
        self._lapla   = self._lapla_tmp.copy()
        self._idlapla = self._idlapla_tmp.copy()
        if np.isnan(self._lapla_tmp).any(): print('Error! Found NaNs in lapla_tmp')
        self._lapla, self._idlapla = self.make_sparse(self._lapla, self._idlapla, 1)

        # Delete temporary arrays
        del self._lapla_tmp
        del self._idlapla_tmp

        # Impose Dirichlet boundary conditions on whole boundary
        l_irows_fixed = []
        l_irows_nonzero = []
        l_icols_nonzero = []
        for irow_fixed in self._o_mesh._l_inodes_boundary:
            l_irows_fixed.append(irow_fixed)
            l_icols_csr = self._lapla.nonzero()[0] == irow_fixed
            l_irows_csr = self._lapla.nonzero()[1] == irow_fixed
            l_icols_nonzero.append(self._lapla.nonzero()[1][l_icols_csr])
            l_irows_nonzero.append(self._lapla.nonzero()[0][l_irows_csr])

        # Set Dirichlet rows to be 1 at diagonal and 0 otherwise
        irow_loc = -1
        for irow_fixed in l_irows_fixed:
            irow_loc += 1
            self._lapla[irow_fixed, l_icols_nonzero[irow_loc]] = 0.0
            # Divide by the number of subdomains that shares the node
            self._lapla[irow_fixed, irow_fixed] = 1.0  # /self.nshare

        # Eliminate zeros from sparse matrix
        self._lapla.eliminate_zeros()



    def make_sparse(self, matrix, idmat, ndofs):
        """ Make matrix sparse. """

        if np.isnan(matrix).any():
            print('Error! Found NaNs in matrix.')
            exit()
        nrows  = ndofs * self._nnode
        matrix = ssp.csr_matrix((matrix, (idmat[:,0],idmat[:,1])), shape=(nrows,nrows))
        idmat  = np.unique(idmat, axis=0)

        return matrix, idmat



    def calculate_levelset_source(self, interp_matrix, rows, cols):
        """ Computes source for level-set Poisson equation.

        Inputs
        ------
            interp_matrix : CSR sparse matrix with dims (nnode_source,nnode_target)
                interpolation matrix between 2 meshes
            rows (cols) : np-array with dims (nentries)
                row (column) IDs for sparse matrix in interpolation matrix
        Outputs
        -------
            global_source : np-array with dims (nnodes_source*ndime)
                source used for RHS in Poisson equation
        """

        ndime = self._ndime
        nnodes_back = interp_matrix.shape[1]
        global_source = np.zeros(nnodes_back*ndime, float)
        l_inodes_back = map(int, list( set(cols) ) )
        l_ielems_surf = self._o_mesh._l_ielems4nodes_surf

        for inode_back in l_inodes_back:
            l_irows_back = [inode_back*ndime + idime for idime in range(ndime)]
            interp_slice   = interp_matrix[:,inode_back]
            l_inodes_patch = interp_slice.nonzero()[0]

            if len(l_inodes_patch) == 0: continue

            l_ielems_patch = list(set([ielem for inode in l_inodes_patch for ielem in l_ielems_surf[inode]]))

            # Loop over solid boundary elements
            for ielem in l_ielems_patch:
                global_source[l_irows_back] += self.calculate_levelset_elem_source(ielem, interp_slice)

        return global_source



    def calculate_levelset_elem_source(self, ielem, interp_slice):
        """ Calculates elemental source for level-set Poisson equation. """

        # Load node indices and coordinates
        l_inodes_glo = self._o_mesh._l_elems_surf[ielem, :]
        coords       = self._o_mesh._l_node_coords
        nnode_elem   = len(l_inodes_glo)
        ngaus_surf   = self._o_mesh._ngaus_surf

        # Calculate vector normal to boundary element
        inode1 = l_inodes_glo[0]
        inode2 = l_inodes_glo[1]
        normal_vector = np.array([coords[inode2,1] - coords[inode1,1],
                                  coords[inode1,0] - coords[inode2,0]])
        normal_vector /= np.linalg.norm(normal_vector)

        # Compute element contribution to source term
        source = -normal_vector[0:self._ndime]                             \
            * ( ( self._o_mesh._gp_volume_surf[ielem, 0:ngaus_surf]        \
            @     self._o_mesh._gp_shape_surf[:ngaus_surf, 0:nnode_elem] ) \
            @     np.array(interp_slice[l_inodes_glo].toarray())         )

        return source



    def calculate_levelset_rhs(self, global_source, interp_method):
        """ Calculates RHS for level-set Poisson equation

            TODO: Offload to Fortran kernel to speed up process.
        """

#        if interp_method[0:5].upper() == 'KRIGI':
#            # Invert mass matrix
#            self._inv_mass_matrix = ssla.inv(self._mass_matrix)
#            # Multiply global source vector by inverse Eulerian mass matrix for
#            # coordinate transform
#            for idime in range(self._ndime):
#                global_source[idime::self._ndime] = self._inv_mass_matrix.dot(global_source[idime::self._ndime])

        # Compute divergence of source vector
        module_name = self._d_params['module_name']
        self._global_vector_tmp = np.zeros(self._nnode, float)
        str_expr = 'self._global_vector_tmp = ' \
                 + module_name+'.'+module_name.lower()  \
                 + '.get_levelset_rhs('                 \
                 + 'global_source, '                    \
                 + 'self._o_mesh._nnode, '              \
                 + 'self._ndime)'
        exec(str_expr)
        global_vector = self._global_vector_tmp.copy()
        del self._global_vector_tmp

        return global_vector



    def add_to_RHS(self, ielem, global_vector, local_vector):
        """ Elemental assembly of vector to global vector. """

        # Degrees of freedom of the equation
        nnode = self._o_mesh._nnode_elem
        ndofs_rhs = int( len(local_vector) / nnode )

        # Loop over nodes
        irow_loc = -1
        for inode_loc in range(nnode):

            # Loop over degrees of freedom
            for idof in range(ndofs_rhs):
                irow_loc += 1
                irow_glo = int(self._o_mesh._l_elems[ielem,inode_loc]) * ndofs_rhs + idof

                # Assemble to element to global RHS vector
                global_vector[irow_glo] += local_vector[irow_loc]

        return global_vector



    def get_mass_matrix(self):

        # Get mass matrix from Fortran Module
        module_name = self._d_params['module_name']
        self._mass_matrix = []
        self._idmass = []
        str_expr = 'self._mass_matrix, self._idmass = ' \
                   + module_name + '.' + module_name.lower() \
                   + '.get_mass_matrix(' \
                   + 'self._nelem, ' \
                   + 'self._o_mesh._nnode_elem)'
        exec(str_expr)

        # Convert to CSR sparse format
        self._mass_matrix = \
            ssp.csr_matrix((self._mass_matrix,
                           (self._idmass[:, 0], self._idmass[:, 1])),
                           shape=(self._nnode, self._nnode))
        self._idmass = np.unique(self._idmass, axis=0)

        # Compute lumped mass matrix
        self._mass_matrix_lumped = np.zeros(self._nnode, float)
        for irow in range(self._nnode):
            self._mass_matrix_lumped[irow] = np.sum(self._mass_matrix[irow, :])

        # Parallel lumped mass matrix exchange
        self._mass_matrix_lumped = self.par_array_exchange(self._mass_matrix_lumped)

    def invert_mass_matrix(self, flag_lump=False):

        # Compute inverse of mass matrix
        if flag_lump:
            self._inv_mass_matrix_lumped = np.ones(self._nnode, float) \
                                           / self._mass_matrix_lumped
        else:
            self._inv_mass_matrix = ssla.inv(self._mass_matrix)



    def calculate_levelset_elem_rhs(self, ielem, source):
        """ Calculates elemental RHS for Poisson equation. """

        nnode = self._o_mesh._nnode_elem
        divergence_matrix = np.zeros((nnode, nnode*self._ndime), float)

        for igaus in range(self._o_mesh._ngaus):
            gp_dshape_glo   = self.compute_global_shape_function_derivatives(ielem, igaus)
            gp_shape_dshape = self.compute_divergence_matrix(ielem, igaus, gp_dshape_glo)
            for inode in range(nnode):
                for jnode in range(nnode):
                    for jdime in range(self._ndime):
                        jrow = jnode*self._ndime + jdime
                        divergence_matrix[inode,jrow] += gp_shape_dshape[inode,jnode,jdime] * self._o_mesh._gp_volume[ielem,igaus]

        return divergence_matrix.dot(source)



    def compute_global_shape_function_derivatives(self, ielem, igaus):
        """ Calculates global shape function derivatives. """

        nnode_elem = self._o_mesh._nnode_elem
        gp_dshape_glo = np.zeros((nnode_elem, self._ndime), float)
        for inode in range(nnode_elem):
            for idime in range(self._ndime):
                for jdime in range(self._ndime):
                    gp_dshape_glo[inode,idime] += \
                        self._o_mesh._gp_inv_jacobians[ielem,igaus,idime,jdime] \
                      * self._o_mesh._gp_dshape[igaus,inode,jdime]

        return gp_dshape_glo




    def compute_laplacian_matrix(self, ielem, igaus, gp_dshape_glo):
        """ Calculates element "Laplacian" matrix (without element volumes). """

        nnode_elem = self._o_mesh._nnode_elem
        gp_dshape_dshape = np.zeros((nnode_elem, nnode_elem), float)
        for inode in range(nnode_elem):
            for jnode in range(nnode_elem):
                for idime in range(self._ndime):
                    gp_dshape_dshape[inode,jnode] += \
                      - gp_dshape_glo[inode,idime] \
                      * gp_dshape_glo[jnode,idime]

        return gp_dshape_dshape



    def compute_divergence_matrix(self, ielem, igaus, gp_dshape_glo):
        """ Calculates element divergence matrix. """

        nnode_elem = self._o_mesh._nnode_elem
        gp_shape_dshape = np.zeros((nnode_elem, nnode_elem, self._ndime), float)
        for inode in range(nnode_elem):
            for jnode in range(nnode_elem):
                for idime in range(self._ndime):
                    gp_shape_dshape[inode,jnode,idime] = self._o_mesh._gp_shape[igaus,inode] * gp_dshape_glo[jnode,idime]

        return gp_shape_dshape



    def compute_elem_mass_matrix(self, ielem):
        """ Calculates element divergence matrix. """

        nnode_elem = self._o_mesh._nnode_elem
        mass_matrix = np.zeros((nnode_elem, nnode_elem), float)
        for igaus in range(self._o_mesh._ngaus):
            for inode in range(nnode_elem):
                for jnode in range(nnode_elem):
                    mass_matrix[inode,jnode] += \
                        self._o_mesh._gp_shape[igaus,inode] \
                      * self._o_mesh._gp_shape[igaus,jnode] \
                      * self._o_mesh._gp_volume[ielem,igaus]

        return mass_matrix



    def load_state(self):
        """ Loads state from pickle file for restart. """

        # Get MPI rank
        comm = MPI.COMM_WORLD

        # Get filename
        if comm.Get_size() > 1:
            filename = os.path.join(self._d_params['path_inputs'], \
                                    self._d_params['path_outs'], \
                                    self._d_params['casename']+'_'+str(self._rank)+'.rst')
        else:
            filename = os.path.join(self._d_params['path_inputs'], \
                                    self._d_params['path_outs'], \
                                    self._d_params['casename']+'.rst')

        # Load pickle
        f = open(filename, 'rb')
        tmp_dict = pkl.load(f)
        f.close()
        tmp_dict.pop('_ntmax',None)
        tmp_dict['_d_params'] = self._d_params.copy()
        self.__dict__.update(tmp_dict)



    def save_state(self):
        """ Saves current state to pickle file for later restart. """


        # Check if I must save state
        tcount    = self._tcount
        ndump     = self._d_params['ndump_restart']
        flag_save = (ndump > 0) and (np.mod(tcount,ndump) == 0) and (tcount > 0)
        if not flag_save: return

        # Get MPI rank
        comm = MPI.COMM_WORLD

        # Get filename
        if comm.Get_size() > 1:
            filename = os.path.join(self._d_params['path_outs'], \
                                    self._d_params['casename']+'_'+str(self._rank)+'.rst')
        else:
            filename = os.path.join(self._d_params['path_outs'], \
                                    self._d_params['casename']+'.rst')

        # Load pickle
        f = open(filename, 'wb')

        # Remove unnecessary attributes from saved problem 
        d_out = self.__dict__.copy()
        d_out.pop('_d_bconds',None)

        # Dump problem
        pkl.dump(d_out, f, 2)
        f.close()



    def deallocate(self):
        """ Deallocates associated Fortran module. """
        module_name = self._d_params['module_name']
        if module_name.replace(" ","") != "":
            str_expr = module_name+'.'+module_name.lower() + '.dealloc()'
            exec(str_expr)



    def set_number_of_neighbors(self, nlevels_interp):
        """ Set number of levels for neighboring nodes search. """

        self._d_params['nlevels_neighbors'] = nlevels_interp



    def find_node_neighbors_of_elements(self):
        """ Find NN nodes for each element. """

        l_neighbors = self._nelem * [[]]
        for ielem in range(self._nelem):
            l_inodes = self._o_mesh._l_elems[ielem, :]
            l_inodes_tmp = []
            for ilevel in range(self._d_params['nlevels_neighbors']):
                for i in l_inodes:
                    l_inodes_tmp.extend( self._o_mesh._l_inodes4nodes[i] )
                l_inodes = l_inodes_tmp.copy()
            l_inodes = list(set( l_inodes ))
            l_neighbors[ielem] = l_inodes

        self._l_neighbors = l_neighbors

        return l_neighbors



    def set_initial_conditions(self):
        """ TODO : implement setting of initial conditions. """
        pass



# ============================================================================ #
#             Abstract methods to be specified in each physics.                #
# ============================================================================ #

    @abstractmethod
    def initialize_material_properties(self):
        """ Initialize material properties. """
        pass

    @abstractmethod
    def compute_timestep_size(self):
        """ Computes timestep size. """
        pass

    @abstractmethod
    def print_parameters(self):
        """ Prints problem parameters. To be defined in each module. """
        pass

    @abstractmethod
    def initialize_fortran_module(self):
        """ Initializes corresponding Fortran Module. """
        pass

    @abstractmethod
    def integrate_secondary_variables(self, iequation):
        """ Integrates secondary variables from primary ones. """
        pass

    @abstractmethod
    def check_convergence(self, iequation):
        """ Checks convergence at each timestep. """
        pass

    @abstractmethod
    def check_conservation(self, iequation):
        """ Checks conservation of invariants at each timestep. """
        pass
# ============================================================================ #
