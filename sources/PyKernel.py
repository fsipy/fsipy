"""
    FsiPy Python Kernel
    ===================
"""

## Internal libraries
from PyParameters import *
from PyProblem    import *
from PyMesh       import *
from PyEquation   import *
from PyCoupling   import *
